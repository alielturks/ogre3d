#include "stdafx.h"
#include "BulletManager.h"


BulletManager::BulletManager()
{
}


BulletManager::~BulletManager()
{
	for (int i = 0; i < rigidBodies.size(); i++) {
		dynamicsWorld->removeRigidBody(rigidBodies.at(i).object);
	}
	for (int i = 0; i < staticObjects.size(); i++) {
		dynamicsWorld->removeRigidBody(staticObjects.at(i).object);
	}
}

void BulletManager::init(Ogre::SceneManager* mgr, OgreBites::SdkTrayManager*	mTray)
{
	mSceneMgr = mgr;
	mTrayMgr = mTray;
	//Bullet physics
	broadphase = new btDbvtBroadphase();

	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);

	solver = new btSequentialImpulseConstraintSolver;

	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	_defaultGravity = btVector3(0, -10, 0);
	dynamicsWorld->setGravity(_defaultGravity);

	debugDrawer = new OgreDebugDrawer(mSceneMgr);
	debugDrawer->setDebugMode(btIDebugDraw::DBG_DisableBulletLCP);
	dynamicsWorld->setDebugDrawer(debugDrawer);

	//createGround();
}

Ogre::SceneNode* BulletManager::addCube(const std::string name, const btVector3& size, const btVector3& position, const float mass, const float restitution, const float linearDamping, const float angularDamping, const float friction)
{
	btCollisionShape* fallShape = new btBoxShape(size);

	fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), position));
	btScalar btScalarMass = mass;
	btVector3 fallInertia(0, 0, 0);
	fallShape->calculateLocalInertia(btScalarMass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(btScalarMass, fallMotionState, fallShape, fallInertia);

	btRigidBody* body = new btRigidBody(fallRigidBodyCI);
	body->setLinearVelocity(btVector3(0, 0, 0));
	body->setRestitution(restitution);
	body->setFriction(friction);
	body->setDamping(linearDamping, angularDamping);
	body->activate(false);
	dynamicsWorld->addRigidBody(body);

	RigidBodyObject objectDescription;
	objectDescription.name = name;
	objectDescription.object = body;
	rigidBodies.push_back(objectDescription);

	//Create Ogre entity
	Ogre::Entity* cube = mSceneMgr->createEntity(name, "5cube.mesh");
	cube->setCastShadows(true);
	cube->setMaterialName("My/Cube");
	Ogre::SceneNode* mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
	mNode->setPosition(0, 0, 0);
	mNode->attachObject(cube);
	mNode->setScale(Ogre::Vector3(size.x(), size.y(), size.z()));

	return mNode;
}

void BulletManager::addSphere(const std::string name, const float size, const btVector3& position, const float restitution, const float linearDamping, const float angularDamping, const float friction)
{
	btSphereShape* fallShape = new btSphereShape(size);
	fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), position));
	btScalar mass = 0.1;
	btVector3 fallInertia(0, 0, 0);
	fallShape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);

	btRigidBody* body = new btRigidBody(fallRigidBodyCI);
	body->setLinearVelocity(btVector3(0, 0, 0));
	body->setRestitution(restitution);
	body->setFriction(friction);
	body->setDamping(linearDamping, angularDamping);
	dynamicsWorld->addRigidBody(body);

	RigidBodyObject objectDescription;
	objectDescription.name = name;
	objectDescription.object = body;
	rigidBodies.push_back(objectDescription);

	//Create ogre entity
	Ogre::Entity* sphere = mSceneMgr->createEntity(name, "sphere.mesh");
	sphere->setCastShadows(true);
	Ogre::SceneNode* sphereNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
	sphereNode->setPosition(0, 0, 0);
	sphereNode->attachObject(sphere);
	sphereNode->setScale(Ogre::Vector3(size/100.0, size/100.0, size/100.0));
}

btRigidBody* BulletManager::addCustomObject(const std::string name, const btVector3& position, const std::vector<btVector3>* points, const float restitution, const float friction, const float mass)
{
	ground = new btConvexHullShape();
	for (auto it = points->begin(); it != points->end(); ++it) {
		ground->addPoint(btVector3(it->getX(), it->getY(), it->getZ()), true);
	}

	btCompoundShape* compound = new btCompoundShape();
	btTransform localTrans;
	localTrans.setIdentity();
	localTrans.setOrigin(btVector3(0, 0, 0));
	compound->addChildShape(localTrans, ground);
	compound->recalculateLocalAabb();

	groundMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), position));
	btVector3 fallInertia(0, 0, 0);
	btScalar objectMass = mass;
	compound->calculateLocalInertia(objectMass, fallInertia);

	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(objectMass, groundMotionState, compound, fallInertia);

	btRigidBody* body = new btRigidBody(groundRigidBodyCI);
	body->setLinearVelocity(btVector3(0, 0, 0));
	body->setFriction(friction);
	body->setRestitution(restitution);
	body->setDamping(0, 0);
	dynamicsWorld->addRigidBody(body);

	RigidBodyObject objectDescription;
	objectDescription.name = name;
	objectDescription.object = body;

	if (mass == 0) {
		staticObjects.push_back(objectDescription);
	}
	else {
		rigidBodies.push_back(objectDescription);
	}

	return body;
}

Vehicle* BulletManager::createCar(const int ControllerId, const std::string name, const btVector3& position, const std::vector<btVector3>* points, const float restitution, const float friction, const float mass, ServerRequests::VehicleInfo info, std::vector<btRigidBody*> wheels)
{
	Vehicle* v = new Vehicle;
	v->setWorld(dynamicsWorld);
	v->setSceneManager(mSceneMgr);
	v->setTrayManager(mTrayMgr);
	v->setIndex(ControllerId);
	v->setVehicleInfo(info);
	v->setStartPosition(position.x(), position.y(), position.z());

	btRigidBody* chasis = addCustomObject(name, position, points, restitution, friction, mass);
	v->setChasis(chasis);
	v->init();
	v->setWheels(wheels);
	return v;
}

void BulletManager::deleteStaticObject(const std::string name)
{
	for (auto it = staticObjects.begin(); it != staticObjects.end(); ++it) {
		if ((*it).name == name) {
			dynamicsWorld->removeRigidBody((*it).object);
			it = staticObjects.erase(it);
			--it;
		}
	}
}

std::vector<RigidBodyObject> BulletManager::getBodies()
{
	return rigidBodies;
}

std::vector<RigidBodyObject>* BulletManager::getStaticBodies()
{
	return &staticObjects;
}

std::vector<RigidBodyObject>::iterator BulletManager::deleteObject(const std::vector<RigidBodyObject>::iterator it)
{
	dynamicsWorld->removeRigidBody((*it).object);
	mSceneMgr->getRootSceneNode()->removeChild((*it).name);
	mSceneMgr->destroyEntity((*it).name);
	return rigidBodies.erase(it);
}

btDiscreteDynamicsWorld* BulletManager::getWorld()
{
	return dynamicsWorld;
}

bool BulletManager::frameStarted(const Ogre::FrameEvent& evt)
{
	if (debugDrawer) {
		debugDrawer->frameStarted(evt);
	}
	if (_gravityTime > 0) {
		_gravityTime -= evt.timeSinceLastFrame;
		if (_gravityTime <= 0) {
			dynamicsWorld->setGravity(_defaultGravity);
			_gravityEffectEnabled = false;
		}
	}
	return true;
}
bool BulletManager::frameEnded(const Ogre::FrameEvent& evt)
{
	if (debugDrawer) {
		debugDrawer->frameEnded(evt);
	}
	return true;
}

bool BulletManager::keyPressed(const OIS::KeyEvent &arg)
{
	return true;
}

bool BulletManager::keyReleased(const OIS::KeyEvent &arg)
{
	return true;
}

bool BulletManager::axisMoved(const OIS::JoyStickEvent &e, int axis)
{
	return true;
}
bool BulletManager::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
	return true;
};
bool BulletManager::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
	return true;
};
bool BulletManager::povMoved(const OIS::JoyStickEvent &arg, int pov)
{
	/*
	Ogre::LogManager::getSingletonPtr()->logMessage("pov moved " + std::to_string(pov));
	*/
	return true;
};

void BulletManager::createGround(ServerRequests::TrackInfo* t)
{
	const float width = 2;
	const float height = 3;
	Ogre::Vector3 vec(width / 2, 0, 0);
	Ogre::ManualObject obj1("GrassObject");

	Ogre::Quaternion quat;
	quat.FromAngleAxis(Ogre::Degree(60), Ogre::Vector3::UNIT_Y);

	obj1.begin("Examples/GrassBlades", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	for (int i = 0; i < 3; ++i)
	{
		obj1.position(-vec.x, height, -vec.z);
		obj1.textureCoord(0, 0);
		obj1.position(vec.x, height, vec.z);
		obj1.textureCoord(1, 0);
		obj1.position(-vec.x, 0, -vec.z);
		obj1.textureCoord(0, 1);
		obj1.position(vec.x, 0, vec.z);
		obj1.textureCoord(1, 1);
		int offset = 4 * i;
		obj1.triangle(offset + 0, offset + 3, offset + 1);
		obj1.triangle(offset + 0, offset + 2, offset + 3);
		vec = quat * vec;
	}
	obj1.end();
	obj1.convertToMesh("GrassBladesMesh");
	Ogre::MeshPtr ptr = obj1.convertToMesh("GrassBladesMesh");
	Ogre::MeshSerializer ser;
	ser.exportMesh(ptr.getPointer(), "user_grass.mesh");


	Ogre::Entity* grass = mSceneMgr->createEntity("GrassBladesMesh");
	Ogre::StaticGeometry* sg = mSceneMgr->createStaticGeometry("GrassArea");
	const int size = 375;
	const int amount = 20;
	sg->setRegionDimensions(Ogre::Vector3(size, size, size));
	sg->setOrigin(Ogre::Vector3(-size / 2, 0, -size / 2));

	Ogre::ManualObject* obj = mSceneMgr->createManualObject("manual");
	obj->begin("My/Track", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	
	//Ogre::MaterialPtr materialPtr = Ogre::MaterialManager::getSingleton().getByName("My/Track");
	//Ogre::TextureUnitState* ptus = materialPtr->getTechnique(0)->getPass(1)->getTextureUnitState(0); //2nd pass, first texture unit
	//ptus->setAlphaOperation(Ogre::LBX_MODULATE, Ogre::LBS_MANUAL, Ogre::LBS_TEXTURE, 0.8f);

	const int wx = t->width;
	const int wy = t->height;
	const float scaleX = t->scale;
	const float scaleY = t->scale;
	const float scaleZ = t->scale;
	const float halfX = (float)wx / 2.0f;
	const float halfY = (float)wy / 2.0f;

	//This array is needed for the bullet
	float* points = new float[wx * wy];

	float somethingX = 0.01f;
	float somethingY = 0.01f;

	/*for (int i = 0; i < wy; i++) {
		for (int j = 0; j < wx; j++) {
		//	if (i == 0 || j == 0 || i == wy - 1 || j == wx - 1) {
			//	points[i * wx + j] = 2;
			//}
			//else {
				float x = (float)j;
				float y = (float)i;
				time(NULL);
				somethingX += 0.015;// (float)rand() / (float)RAND_MAX;
				time(NULL);
				somethingY += (float)rand() / (float)RAND_MAX;
				points[i * wx + j] = t->mapPoints.at(i * wx + j);// (sinf(somethingX) * cosf(somethingY)) / 10.0f;
				for (int innerX = 0; innerX < 1; innerX++) {
					for (int innerY = 0; innerY < 1; innerY++) {
						Ogre::Quaternion quat;
						quat.FromAngleAxis(
							Ogre::Degree(Ogre::Math::RangeRandom(0, 359)),
							Ogre::Vector3::UNIT_Y);
						Ogre::Vector3 scale(1, Ogre::Math::RangeRandom(0.9, 1.1), 1);
						sg->addEntity(grass, Ogre::Vector3((x + (innerX/10.0f)) * scaleX - halfX * scaleX, (sinf(somethingX) * cosf(somethingY)) / 5.0f * scaleY, (y + (innerY/10.0f)) * scaleZ - halfY * scaleZ), quat, scale);
					}
				}
			//}
		}
	}*/
	sg->build();

	for (int i = 0; i < wy; i++) {
		for (int j = 0; j < wx; j++) {
			points[i * wx + j] = t->mapPoints[i * wx + j];
			if (j + 1 < wx && i + 1 < wy && i != wy - 1 && j != wx - 1) {
				//First triangle
				//float pointValue1 = t->mapPoints[i * wx + j];//points[i * wx + j];
				//float pointValue2 = t->mapPoints[(i + 1) * wx + j];//points[(i + 1) * wx + j];
				//float pointValue3 = t->mapPoints[i * wx + (j + 1)];//points[i * wx + (j + 1)];

				float pointValue1 = t->mapPoints.at(i * wx + j);
				float pointValue2 = t->mapPoints.at((i + 1) * wx + j);
				float pointValue3 = t->mapPoints.at(i * wx + (j + 1));
				Ogre::Vector3 vec1((float)j - halfX, pointValue1, (float)i - halfY);
				Ogre::Vector3 vec2((float)j - halfX, pointValue2, (float)i - halfY + 1.0f);
				Ogre::Vector3 vec3((float)j - halfX + 1.0f, pointValue3, (float)i - halfY);

				Ogre::Vector3 edge1 = vec1 - vec2;
				Ogre::Vector3 edge2 = vec2 - vec3;

				Ogre::Vector3 normal1 = edge1.crossProduct(edge2);
				normal1.normalise();
				if (normal1.x <= 0) {
					//Normals should always point up for the ground
					normal1.x = -normal1.x;
				}

				float x0 = (float)j / (float)wx;
				float x1 = (float)j / (float)wx + 1.0f / (float)wx;
				float y0 = (float)i / (float)wy;
				float y1 = (float)i / (float)wy + 1.0f / (float)wy;

				{
					obj->position(vec1);
					obj->textureCoord(Ogre::Vector2(x0, y0));
					obj->normal(normal1);
				}
				{
					obj->position(vec2);
					obj->textureCoord(Ogre::Vector2(x0, y1));
					obj->normal(normal1);
				}
				{
					obj->position(vec3);
					obj->textureCoord(Ogre::Vector2(x1, y0));
					obj->normal(normal1);
				}

				//Second triangle
				float pointValue4 = t->mapPoints.at(i * wx + (j + 1));
				float pointValue5 = t->mapPoints.at((i + 1) * wx + j);
				float pointValue6 = t->mapPoints.at((i + 1) * wx + (j + 1));
				Ogre::Vector3 vec4((float)j - halfX + 1.0f, pointValue4, (float)i - halfY);
				Ogre::Vector3 vec5((float)j - halfX, pointValue5, (float)i - halfY + 1.0f);
				Ogre::Vector3 vec6((float)j - halfX + 1.0f, pointValue6, (float)i - halfY + 1.0f);

				Ogre::Vector3 edge3 = vec4 - vec5;
				Ogre::Vector3 edge4 = vec5 - vec6;

				Ogre::Vector3 normal2 = edge3.crossProduct(edge4);
				normal2.normalise();
				if (normal2.x <= 0) {
					//Normals should always point up for the ground
					normal2.x = -normal2.x;
				}

				{
					obj->position(vec4);
					obj->textureCoord(Ogre::Vector2(x1, y0));
					obj->normal(normal2);
				}
				{
					obj->position(vec5);
					obj->textureCoord(Ogre::Vector2(x0, y1));
					obj->normal(normal2);
				}
				{
					obj->position(vec6);
					obj->textureCoord(Ogre::Vector2(x1, y1));
					obj->normal(normal2);
				}
			}
		}
	}
	obj->end();

	Ogre::SceneNode* ground = mSceneMgr->getRootSceneNode()->createChildSceneNode("ground");
	ground->attachObject(obj);

	ground->setPosition(scaleX / 2.0f, -1, scaleZ/2.0f);
	
	ground->scale(Ogre::Vector3(scaleX, scaleY, scaleZ));

	btHeightfieldTerrainShape* terrain = new btHeightfieldTerrainShape(wx, wy, points, 1.0, -100, 100, 1, PHY_ScalarType::PHY_FLOAT, false);
	//delete[] points;
	terrain->setLocalScaling(btVector3(scaleX, scaleY, scaleZ));

	btRigidBody* groundBody = new btRigidBody(0, new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0))), terrain);
	groundBody->setCollisionFlags(groundBody->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);

	dynamicsWorld->addRigidBody(groundBody);
}

void BulletManager::changeGravity(float g, float t)
{
	btVector3 currentGravity = dynamicsWorld->getGravity();
	currentGravity.setY(currentGravity.y() + g);
	dynamicsWorld->setGravity(currentGravity);
	_gravityTime = t;
	_gravityEffectEnabled = true;
}

float BulletManager::getEffectTime()
{
	return _gravityTime;
}

bool BulletManager::getGravityEffect()
{
	return _gravityEffectEnabled;
}