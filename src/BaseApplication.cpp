/*
-----------------------------------------------------------------------------
Filename:    BaseApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "BaseApplication.h" 

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#include <macUtils.h>
#endif

//---------------------------------------------------------------------------
BaseApplication::BaseApplication(void)
	: mRoot(0),
	mSceneMgr(0),
	mWindow(0),
	mResourcesCfg(Ogre::StringUtil::BLANK),
	mPluginsCfg(Ogre::StringUtil::BLANK),
	mTrayMgr(0),
	mDetailsPanel(0),
	mCursorWasVisible(false),
	mShutDown(false),
	mInputManager(0),
	mMouse(0),
	mKeyboard(0),
	mOverlaySystem(0),
	last_x(0),
	last_y(0),
	mMiniscreen(0),
	mAnimationState(0)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
    m_ResourcePath = Ogre::macBundlePath() + "/Contents/Resources/";
#else
    m_ResourcePath = "";
#endif
}

//---------------------------------------------------------------------------
BaseApplication::~BaseApplication(void)
{
    if (mTrayMgr) delete mTrayMgr;
    if (mOverlaySystem) delete mOverlaySystem;

    // Remove ourself as a Window listener
    Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
    windowClosed(mWindow);
    delete mRoot;
}

//---------------------------------------------------------------------------
bool BaseApplication::configure(void)
{
    // Show the configuration dialog and initialise the system.
    // You can skip this and use root.restoreConfig() to load configuration
    // settings if you were sure there are valid ones saved in ogre.cfg.
    if(mRoot->showConfigDialog())
	//if (mRoot->restoreConfig())
    {
        // If returned true, user clicked OK so initialise.
        // Here we choose to let the system create a default rendering window by passing 'true'.
		mWindow = mRoot->initialise(false, "Cars");

		HWND hWnd = CreateWindow(NULL, "Cheeks Up Game", WS_EX_TOPMOST | WS_POPUP | WS_MAXIMIZE, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), HWND_DESKTOP, NULL, NULL, NULL);
		
		int width = 0;
		int height = 0;
		bool fullscreen = false;
		for (auto it = mRoot->getRenderSystem()->getConfigOptions().begin(); it != mRoot->getRenderSystem()->getConfigOptions().end(); ++it) {
			if (it->first == "Video Mode") {
				sscanf(it->second.currentValue.c_str(), "%i x %i", &width, &height);
			}
			if (it->first == "Full Screen" && it->second.currentValue == "Yes") {
				fullscreen = true;
			}
		}

		std::clog << "READING CONFIG " << width << "x" << height << std::endl;
		std::clog << "FULLSCREEN " << fullscreen << std::endl;

		Ogre::NameValuePairList misc;
		misc["externalWindowHandle"] = Ogre::StringConverter::toString((int)hWnd);
		misc["border"] = "none";
		mWindow = mRoot->createRenderWindow("Car&Gar", width, height, fullscreen, &misc);

        return true;
    }
    else
    {
        return false;
    }
}
//---------------------------------------------------------------------------
void BaseApplication::chooseSceneManager(void)
{
    // Get the SceneManager, in this case a generic one
    mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);

    // Initialize the OverlaySystem (changed for Ogre 1.9)
    mOverlaySystem = new Ogre::OverlaySystem();
    mSceneMgr->addRenderQueueListener(mOverlaySystem);
}
//---------------------------------------------------------------------------
void BaseApplication::createCamera(void)
{
	/*
	if (_joysticks.empty()) {
		Ogre::Camera* camera = mSceneMgr->createCamera("Camera_Default");
		camera->setPosition(0, 50, 200);
		camera->setNearClipDistance(0.1);

		OgreBites::SdkCameraMan* cameraMan = new OgreBites::SdkCameraMan(camera);
		cameraMan->setTopSpeed(10);
		cameraMan->setStyle(OgreBites::CameraStyle::CS_FREELOOK);

		_playerCameraMans.push_back(cameraMan);
		_playerCameras.push_back(camera);
	}
	else {
		for (auto it = _joysticks.begin(); it != _joysticks.end(); ++it) {
			Ogre::Camera* camera = mSceneMgr->createCamera("Camera_" + std::to_string((*it)->getID()));
			camera->setPosition(0, 50, 200);
			camera->setNearClipDistance(0.1);

			OgreBites::SdkCameraMan* cameraMan = new OgreBites::SdkCameraMan(camera);
			cameraMan->setTopSpeed(10);
			cameraMan->setStyle(OgreBites::CameraStyle::CS_FREELOOK);

			_playerCameraMans.push_back(cameraMan);
			_playerCameras.push_back(camera);
		}
	}*/

	{

		Ogre::Camera* camera = mSceneMgr->createCamera("First_Camera");
		camera->setPosition(0, 0, -100);
		camera->setNearClipDistance(0.1);

		OgreBites::SdkCameraMan* cameraMan = new OgreBites::SdkCameraMan(camera);
		cameraMan->setTopSpeed(10);
		cameraMan->setStyle(OgreBites::CameraStyle::CS_FREELOOK);

		_playerCameraMans.push_back(cameraMan);
		_playerCameras.push_back(camera);
	}
	{

		Ogre::Camera* camera = mSceneMgr->createCamera("Second_Camera");
		camera->setPosition(0, 50, 100);
		camera->setNearClipDistance(0.1);

		OgreBites::SdkCameraMan* cameraMan = new OgreBites::SdkCameraMan(camera);
		cameraMan->setTopSpeed(10);
		cameraMan->setStyle(OgreBites::CameraStyle::CS_FREELOOK);

		_playerCameraMans.push_back(cameraMan);
		_playerCameras.push_back(camera);
	}
}
//---------------------------------------------------------------------------
void BaseApplication::createFrameListener(void)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    mInputManager = OIS::InputManager::createInputSystem(pl);

    mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));
    mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));
	try {
		while (OIS::JoyStick* joystick = static_cast<OIS::JoyStick*>(mInputManager->createInputObject(OIS::OISJoyStick, true))) {
			joystick->setEventCallback(this);
			_joysticks.push_back(joystick);
		}
	}
	catch (std::exception &e) {
	}

    mMouse->setEventCallback(this);
    mKeyboard->setEventCallback(this);

    // Set initial mouse clipping size
    windowResized(mWindow);

    // Register as a Window listener
    Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

    mInputContext.mKeyboard = mKeyboard;
    mInputContext.mMouse = mMouse;
    mTrayMgr = new OgreBites::SdkTrayManager("InterfaceName", mWindow, mInputContext);
    mTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);
	//mTrayMgr->createLabel(OgreBites::TL_TOPLEFT, "version", GAME_VERSION, 300);
    //mTrayMgr->showLogo(OgreBites::TL_BOTTOMRIGHT);
    //mTrayMgr->hideCursor();

    // Create a params panel for displaying sample details
    Ogre::StringVector items;
    items.push_back("cam.pX");
    items.push_back("cam.pY");
    items.push_back("cam.pZ");
    items.push_back("");
    items.push_back("cam.oW");
    items.push_back("cam.oX");
    items.push_back("cam.oY");
    items.push_back("cam.oZ");
    items.push_back("");
    items.push_back("Filtering");
    items.push_back("Poly Mode");
	items.push_back("X");
	items.push_back("Y");

    mDetailsPanel = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "DetailsPanel", 200, items);
    mDetailsPanel->setParamValue(9, "Bilinear");
    mDetailsPanel->setParamValue(10, "Solid");
    mDetailsPanel->hide();

    mRoot->addFrameListener(this);

	createCamera();
	//createViewports();
	{
		Ogre::Viewport* vp = mWindow->addViewport(_playerCameras.at(0), 0, 0, 0, 1.0, 1.0);
		vp->setClearEveryFrame(true);
		vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
		_playerCameras.at(0)->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));

		_playerViewports.push_back(vp);
	}
}
//---------------------------------------------------------------------------
void BaseApplication::destroyScene(void)
{
}
//---------------------------------------------------------------------------
void BaseApplication::createViewports(void)
{
	/*int maxScreensX = 1;
	int maxScreensY = 1;

	switch (_playerCameras.size()) {
	case 1:
		maxScreensX = 1;
		maxScreensY = 1;
		break;
	case 2:
		maxScreensX = 1;
		maxScreensY = 2;
		break;
	case 3:
		maxScreensX = 2;
		maxScreensY = 2;
		break;
	case 4:
		maxScreensX = 2;
		maxScreensY = 2;
		break;
	case 5:
		maxScreensX = 3;
		maxScreensY = 2;
		break;
	case 6:
		maxScreensX = 3;
		maxScreensY = 2;
		break;
	case 7:
		maxScreensX = 4;
		maxScreensY = 2;
		break;
	case 8:
		maxScreensX = 4;
		maxScreensY = 2;
		break;
	}
	int count = 0;
	int countX = 0;
	int countY = 0;
	for (auto it = _playerCameras.begin(); it != _playerCameras.end(); ++it) {
		Ogre::Viewport* vp = mWindow->addViewport((*it), count, (1.0 / (float)maxScreensX) * countX, (1.0 / (float)maxScreensY) * countY, (1.0 / (float)maxScreensX), (1.0 / (float)maxScreensY));
		vp->setClearEveryFrame(true);
		vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
		vp->getTarget()->addListener(this);
		(*it)->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
		countX++;
		if (countX == maxScreensX) {
			countY++;
			countX = 0;
		}
		_playerViewports.push_back(vp);
		count++;
	}*/

	mWindow->removeAllViewports();
	_playerViewports.clear();
	{
		Ogre::Viewport* vp = mWindow->addViewport(_playerCameras.at(0), 0, 0, 0, 0.5, 1.0);
		vp->setClearEveryFrame(true);
		vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
		_playerCameras.at(0)->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
		_playerViewports.push_back(vp);
	}
	{
		Ogre::Viewport* vp = mWindow->addViewport(_playerCameras.at(1), 1, 0.5, 0, 0.5, 1.0);
		vp->setClearEveryFrame(true);
		vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
		_playerCameras.at(1)->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));

		_playerViewports.push_back(vp);
	}	
	mWindow->addListener(this);
}
//---------------------------------------------------------------------------
void BaseApplication::setupResources(void)
{
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
            // OS X does not set the working directory relative to the app.
            // In order to make things portable on OS X we need to provide
            // the loading with it's own bundle path location.
            if (!Ogre::StringUtil::startsWith(archName, "/", false)) // only adjust relative directories
                archName = Ogre::String(Ogre::macBundlePath() + "/" + archName);
#endif

            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
                archName, typeName, secName);
        }
    }
}
//---------------------------------------------------------------------------
void BaseApplication::createResourceListener(void)
{
}
//---------------------------------------------------------------------------
void BaseApplication::loadResources(void)
{
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
//---------------------------------------------------------------------------
void BaseApplication::go(void)
{
#ifdef _DEBUG
#ifndef OGRE_STATIC_LIB
    mResourcesCfg = m_ResourcePath + "resources_d.cfg";
    mPluginsCfg = m_ResourcePath + "plugins_d.cfg";
#else
    mResourcesCfg = "resources_d.cfg";
    mPluginsCfg = "plugins_d.cfg";
#endif
#else
#ifndef OGRE_STATIC_LIB
    mResourcesCfg = m_ResourcePath + "resources.cfg";
    mPluginsCfg = m_ResourcePath + "plugins.cfg";
#else
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#endif
#endif

    if (!setup())
        return;

    mRoot->startRendering();
    // Clean up
    destroyScene();
}
//---------------------------------------------------------------------------
bool BaseApplication::setup(void)
{
    mRoot = new Ogre::Root(mPluginsCfg);

    setupResources();

    bool carryOn = configure();
    if (!carryOn) return false;

    chooseSceneManager();

    // Set default mipmap level (NB some APIs ignore this)
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

    // Create any resource listeners (for loading screens)
    createResourceListener();
    // Load resources
    loadResources();

    // Create the scene
    createScene();

    createFrameListener();

    return true;
};
//---------------------------------------------------------------------------
bool BaseApplication::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
    if(mWindow->isClosed())
        return false;

    if(mShutDown)
        return false;

    // Need to capture/update each device
    mKeyboard->capture();
    mMouse->capture();

	//Capture all the joysticks
	for (auto it = _joysticks.begin(); it != _joysticks.end(); ++it) {
		(*it)->capture();
	}

    mTrayMgr->frameRenderingQueued(evt);

    return true;
}
//---------------------------------------------------------------------------
bool BaseApplication::keyPressed( const OIS::KeyEvent &arg )
{
	if (arg.key == OIS::KC_M)   // toggle visibility of advanced frame stats
	{
		//mTrayMgr->;
		//mWindow->setFullscreen(true, 800, 600);
		Ogre::Entity* ogreEntity = mSceneMgr->createEntity("cube.mesh");
		Ogre::SceneNode* ogreNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		ogreNode->attachObject(ogreEntity);
		ogreNode->setPosition(last_x * 100, last_y * 100, 100);
		last_x++;
		if (last_x > 5) {
			last_x = 0;
			last_y++;
		}

	}

    if (arg.key == OIS::KC_F)   // toggle visibility of advanced frame stats
    {
        mTrayMgr->toggleAdvancedFrameStats();
    }
    else if (arg.key == OIS::KC_G)   // toggle visibility of even rarer debugging details
    {
        if (mDetailsPanel->getTrayLocation() == OgreBites::TL_NONE)
        {
            mTrayMgr->moveWidgetToTray(mDetailsPanel, OgreBites::TL_TOPRIGHT, 0);
            mDetailsPanel->show();
        }
        else
        {
            mTrayMgr->removeWidgetFromTray(mDetailsPanel);
            mDetailsPanel->hide();
        }
    }
    else if (arg.key == OIS::KC_T)   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::TextureFilterOptions tfo;
        unsigned int aniso;

        switch (mDetailsPanel->getParamValue(9).asUTF8()[0])
        {
        case 'B':
            newVal = "Trilinear";
            tfo = Ogre::TFO_TRILINEAR;
            aniso = 1;
            break;
        case 'T':
            newVal = "Anisotropic";
            tfo = Ogre::TFO_ANISOTROPIC;
            aniso = 8;
            break;
        case 'A':
            newVal = "None";
            tfo = Ogre::TFO_NONE;
            aniso = 1;
            break;
        default:
            newVal = "Bilinear";
            tfo = Ogre::TFO_BILINEAR;
            aniso = 1;
        }

        Ogre::MaterialManager::getSingleton().setDefaultTextureFiltering(tfo);
        Ogre::MaterialManager::getSingleton().setDefaultAnisotropy(aniso);
        mDetailsPanel->setParamValue(9, newVal);
    }
    else if (arg.key == OIS::KC_R)   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::PolygonMode pm;

		switch (_playerCameras.at(0)->getPolygonMode())
        {
        case Ogre::PM_SOLID:
            newVal = "Wireframe";
            pm = Ogre::PM_WIREFRAME;
            break;
        case Ogre::PM_WIREFRAME:
            newVal = "Points";
            pm = Ogre::PM_POINTS;
            break;
        default:
            newVal = "Solid";
            pm = Ogre::PM_SOLID;
        }

		_playerCameras.at(0)->setPolygonMode(pm);
        mDetailsPanel->setParamValue(10, newVal);
    }
    else if(arg.key == OIS::KC_F5)   // refresh all textures
    {
        Ogre::TextureManager::getSingleton().reloadAll();
    }
    else if (arg.key == OIS::KC_SYSRQ)   // take a screenshot
    {
        mWindow->writeContentsToTimestampedFile("screenshot", ".jpg");
    }

	for (auto it = _playerCameraMans.begin(); it != _playerCameraMans.end(); ++it) {
		//(*it)->injectKeyDown(arg);
	}
    return true;
}
//---------------------------------------------------------------------------
bool BaseApplication::keyReleased(const OIS::KeyEvent &arg)
{
	for (auto it = _playerCameraMans.begin(); it != _playerCameraMans.end(); ++it) {
		//(*it)->injectKeyUp(arg);
	}
    return true;
}
//---------------------------------------------------------------------------
bool BaseApplication::mouseMoved(const OIS::MouseEvent &arg)
{
    return true;
}

bool BaseApplication::frameStarted(const Ogre::FrameEvent& evt)
{
	return true;
}
bool BaseApplication::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

//---------------------------------------------------------------------------
bool BaseApplication::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
    if (mTrayMgr->injectMouseDown(arg, id)) return true;
	for (auto it = _playerCameraMans.begin(); it != _playerCameraMans.end(); ++it) {
		(*it)->injectMouseDown(arg, id);
	}
    //mCameraMan->injectMouseDown(arg, id);
	//mCameraMan2->injectMouseDown(arg, id);

    return true;
}
//---------------------------------------------------------------------------
bool BaseApplication::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
    if (mTrayMgr->injectMouseUp(arg, id)) return true;

	for (auto it = _playerCameraMans.begin(); it != _playerCameraMans.end(); ++it) {
		(*it)->injectMouseUp(arg, id);
	}
    //mCameraMan->injectMouseUp(arg, id);
	//mCameraMan2->injectMouseUp(arg, id);
    return true;
}
//---------------------------------------------------------------------------
// Adjust mouse clipping area
void BaseApplication::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width = width;
    ms.height = height;
}
//---------------------------------------------------------------------------
// Unattach OIS before window shutdown (very important under Linux)
void BaseApplication::windowClosed(Ogre::RenderWindow* rw)
{
    // Only close for window that created OIS (the main window in these demos)
    if(rw == mWindow)
    {
        if(mInputManager)
        {
            mInputManager->destroyInputObject(mMouse);
            mInputManager->destroyInputObject(mKeyboard);

			for (auto it = _joysticks.begin(); it != _joysticks.end(); ++it) {
				mInputManager->destroyInputObject((*it));
			}
			_joysticks.clear();

            OIS::InputManager::destroyInputSystem(mInputManager);
            mInputManager = 0;
        }
    }
}
//---------------------------------------------------------------------------

bool BaseApplication::axisMoved(const OIS::JoyStickEvent &e, int axis)
{
	int x = e.state.mAxes[axis].abs;
	Ogre::LogManager::getSingletonPtr()->logMessage("axis moved " + std::to_string(x) + "   " + std::to_string(axis));
	return true;
}
bool BaseApplication::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("button pressed " + std::to_string(button));
	return true;
};
bool BaseApplication::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("button released " + std::to_string(button));
	return true;
};
bool BaseApplication::povMoved(const OIS::JoyStickEvent &arg, int pov)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("pov moved " + std::to_string(pov));
	return true;
};

void BaseApplication::preRenderTargetUpdate(const Ogre::RenderTargetEvent& rte)
{
}
void BaseApplication::postRenderTargetUpdate(const Ogre::RenderTargetEvent& rte)
{
}

void BaseApplication::preViewportUpdate(const Ogre::RenderTargetViewportEvent& evt)
{
}

void BaseApplication::eventTriggered(GameEvent::EventTypes evt)
{
	switch (evt) {
	case GameEvent::EventTypes::PLAYERS_JOINING:
		createViewports();
		break;
	}
}