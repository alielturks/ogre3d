#include "stdafx.h"
#include "Drop.h"


Drop::Drop(int i, Drop::DropType t):
_id(i),
_type(t),
_isActive(true),
_lifetime(100)
{
}


Drop::~Drop()
{
}

void Drop::init(Ogre::SceneManager* mgr, Ogre::SceneNode* node)
{
	//_entity = mgr->createEntity("Drop" + std:to_string(_id), "cube.mesh");
	//Entity name == node name, we defined that in the BulletManager class
	_entity = mgr->getEntity(node->getName());
	//_sceneNode = mgr->getRootSceneNode()->createChildSceneNode("DropSceneNode" + std::to_string(_id));
	_sceneNode = node;
	_particleSystem = mgr->createParticleSystem("Particle" + std::to_string(_id), "My/Particle");
	_sceneNode->attachObject(_particleSystem);
	//_sceneNode->attachObject(_entity);
	//_sceneNode->setScale(Ogre::Vector3(4.0f, 4.0f, 4.0f));
	_sceneManager = mgr;
	setTexture();
}

Drop::DropType Drop::getType()
{
	return _type;
}

void Drop::setPosition(Ogre::Vector3 pos)
{
	_sceneNode->setPosition(pos);
}

void Drop::update(const Ogre::FrameEvent& evt)
{
	if (!_isActive) return;
	//_sceneNode->yaw(Ogre::Radian(evt.timeSinceLastFrame));

}

bool Drop::checkCollision(Ogre::Vector3 pos)
{
	if (!_isActive) return false;
	const Ogre::Vector3 v = _sceneNode->getPosition();
	const float x = (pos.x - v.x);
	const float y = (pos.y - v.y);
	const float z = (pos.z - v.z);
	if ((x * x + y * y + z * z) < 200) {
		//destroy();
		_isActive = false;
		_sceneManager->destroyParticleSystem(_particleSystem->getName());
		return true;
	}

	return false;
}

void Drop::destroy()
{
	_isActive = false;
	_sceneManager->destroyParticleSystem(_particleSystem->getName());
	_sceneManager->destroySceneNode(_sceneNode->getName());
	_sceneManager->destroyEntity(_entity);
	_particleSystem = 0;
	_sceneNode = 0;
	_entity = 0;
}

void Drop::setValue(float v)
{
	_value = v;
}

float Drop::getValue()
{
	return _value;
}

int Drop::getID()
{
	return _id;
}

float Drop::getValueTime()
{
	return _valueTime;
}

void Drop::setValueTime(float t)
{
	_valueTime = t;
}

void Drop::setTexture()
{
	switch (_type) {
	case DropType::CAR_UPGRADE:
		_entity->setMaterialName("Drop/Speed");
		break;
	case DropType::CASH:
		break;
	case DropType::GRAVITY:
		_entity->setMaterialName("Drop/Gravity");
		break;
	case DropType::POINTS:
		break;
	case DropType::WEAPON:
		break;
	case DropType::CAR_SPIN:
		_entity->setMaterialName("Drop/Jump");
		break;
	case DropType::CAR_ICE:
		_entity->setMaterialName("Drop/Ice");
		break;
	}
}

void Drop::show()
{
	_sceneNode->showBoundingBox(true);
}

void Drop::hide()
{
	_sceneNode->showBoundingBox(false);
}