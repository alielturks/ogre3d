/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
___                 __    __ _ _    _
/___\__ _ _ __ ___  / / /\ \ (_) | _(_)
//  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
|___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "stdafx.h"
#include "Game.h"

//---------------------------------------------------------------------------
Game::Game(void)
{
	_eventDispatcher.addListener(this);
}
//---------------------------------------------------------------------------
Game::~Game(void)
{
	for (int i = 0; i < _players.size(); i++) {
		delete _players.at(i);
	}
}

//---------------------------------------------------------------------------
void Game::createScene(void)
{

}

bool Game::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
	if (mTrayMgr->injectMouseDown(arg, id)) return true;
	for (auto it = _playerCameraMans.begin(); it != _playerCameraMans.end(); ++it) {
		(*it)->injectMouseDown(arg, id);
	}
	return true;
}

bool Game::frameRenderingQueued(const Ogre::FrameEvent& evt)
{

	if (mWindow->isClosed())
		return false;

	if (mShutDown)
		return false;

	// Need to capture/update each device
	mKeyboard->capture();
	mMouse->capture();

	for (auto it = _joysticks.begin(); it != _joysticks.end(); ++it) {
		(*it)->capture();
	}
	
	mTrayMgr->frameRenderingQueued(evt);


	if (!mTrayMgr->isDialogVisible())
	{
		for (auto it = _playerCameraMans.begin(); it != _playerCameraMans.end(); ++it) {
			(*it)->frameRenderingQueued(evt);
		}
		
	}

	return true;

}

bool Game::frameStarted(const FrameEvent& evt)
{ 
	//Update the screen
	if (_screen) {
		_screen->update(evt);
		if (_screen->getScreenEvent() == Screens::ScreenEvents::GO_TO_MENU_SCREEN) {
			changeScreen(Screens::ScreenTypes::MENU);
		}
		else if (_screen->getScreenEvent() == Screens::ScreenEvents::GO_TO_PLAY_SCREEN) {
			changeScreen(Screens::ScreenTypes::PLAY);
		}
		else if (_screen->getScreenEvent() == Screens::ScreenEvents::EXIT_GAME) {
			mShutDown = true;
		}
	}

	bulletManager.getWorld()->stepSimulation(evt.timeSinceLastFrame, 0);

 	//lensFlare->update();
	std::vector<RigidBodyObject> objects = bulletManager.getBodies();
	for (auto it = objects.begin(); it != objects.end(); ++it) {
		btTransform trans;
		(*it).object->getMotionState()->getWorldTransform(trans);
		Ogre::SceneNode* ogreNode = mSceneMgr->getSceneNode((*it).name);
		if (ogreNode) {
			ogreNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY() - 1, trans.getOrigin().getZ()));
			ogreNode->setOrientation(trans.getRotation().getW(), trans.getRotation().getX(), trans.getRotation().getY(), trans.getRotation().getZ());
		}
	}

	bulletManager.getWorld()->debugDrawWorld();
	bulletManager.frameStarted(evt);

	for (auto it = _players.begin(); it != _players.end(); ++it) {
		(*it)->frameStarted(evt);
	}

	for (auto it = _drops.begin(); it != _drops.end(); ++it) {
		(*it).update(evt);
		for (auto p = _players.begin(); p != _players.end(); ++p) {
			if ((*it).checkCollision((*p)->getPlayerPos())) {
				//player have collected this drop
				(*p)->collectDrop(&(*it));
				if ((*it).getType() == Drop::DropType::GRAVITY) {
					bulletManager.changeGravity((*it).getValue(), (*it).getValueTime());
				}
			}
		}
	}

	if (bulletManager.getGravityEffect()) {
		if (!_gravityLabel->isVisible()) {
			_gravityLabel->show();
		}
		_gravityLabel->setCaption("Gravity Off " + std::to_string((int)bulletManager.getEffectTime()));
	}
	else {
		if (_gravityLabel->isVisible()) {
			_gravityLabel->hide();
		}
	}
	return true;
}

bool Game::frameEnded(const FrameEvent& evt)
{
	bulletManager.frameEnded(evt);
	return true;
}

void Game::createMesh(std::string name, std::vector<btVector3>* v)
{
	/// Create the mesh via the MeshManager
	Ogre::MeshPtr msh = MeshManager::getSingleton().createManual(name, "General");

	/// Create one submesh
	SubMesh* sub = msh->createSubMesh();

	/// Define the vertices (8 vertices, each have 3 floats for position and 3 for normal)
	const size_t nVertices = 6;
	const size_t vbufCount = 3 * 2 * nVertices;
	Ogre::Vector3 dir0 = Ogre::Vector3(v->at(2).getX(), v->at(2).getY() - 1, v->at(2).getZ()) - Ogre::Vector3(v->at(0).getX(), v->at(0).getY() - 1, v->at(0).getZ());
	Ogre::Vector3 dir1 = Ogre::Vector3(v->at(0).getX(), v->at(0).getY() - 1, v->at(0).getZ()) - Ogre::Vector3(v->at(1).getX(), v->at(1).getY() - 1, v->at(1).getZ());
	Ogre::Vector3 normal = dir0.crossProduct(dir1).normalisedCopy();
	Ogre::Vector3 normal2 = dir1.crossProduct(dir0).normalisedCopy();
	float vertices[vbufCount] = {
		v->at(0).getX(), v->at(0).getY() - 1, v->at(0).getZ(),        //0 position
		normal.x, normal.y, normal.z,
		v->at(1).getX(), v->at(1).getY() - 1, v->at(1).getZ(),
		normal.x, normal.y, normal.z,
		v->at(2).getX(), v->at(2).getY() - 1, v->at(2).getZ(),
		normal.x, normal.y, normal.z,

		v->at(0).getX(), v->at(0).getY() - 1, v->at(0).getZ(),        //0 position
		normal2.x, normal2.y, normal2.z,
		v->at(1).getX(), v->at(1).getY() - 1, v->at(1).getZ(),
		normal2.x, normal2.y, normal2.z,
		v->at(2).getX(), v->at(2).getY() - 1, v->at(2).getZ(),
		normal2.x, normal2.y, normal2.z,
	};

	RenderSystem* rs = Root::getSingleton().getRenderSystem();
	RGBA colours[nVertices];
	RGBA *pColour = colours;
	// Use render system to convert colour value since colour packing varies
	rs->convertColourValue(ColourValue(1.0, 0.0, 0.0), pColour++); //0 colour
	rs->convertColourValue(ColourValue(0.0, 1.0, 0.0), pColour++); //1 colour
	rs->convertColourValue(ColourValue(0.0, 0.0, 1.0), pColour++); //2 colour
	rs->convertColourValue(ColourValue(1.0, 0.0, 0.0), pColour++); //0 colour
	rs->convertColourValue(ColourValue(0.0, 1.0, 0.0), pColour++); //1 colour
	rs->convertColourValue(ColourValue(0.0, 0.0, 1.0), pColour++); //2 colour

	/// Define 12 triangles (two triangles per cube face)
	/// The values in this table refer to vertices in the above table
	const size_t ibufCount = 6;
	unsigned short faces[ibufCount] = {
		0, 1, 2,
		5, 4, 3
	};

	/// Create vertex data structure for 8 vertices shared between submeshes
	msh->sharedVertexData = new VertexData();
	msh->sharedVertexData->vertexCount = nVertices;

	/// Create declaration (memory format) of vertex data
	VertexDeclaration* decl = msh->sharedVertexData->vertexDeclaration;
	size_t offset = 0;
	// 1st buffer
	decl->addElement(0, offset, VET_FLOAT3, VES_POSITION);
	offset += VertexElement::getTypeSize(VET_FLOAT3);
	decl->addElement(0, offset, VET_FLOAT3, VES_NORMAL);
	offset += VertexElement::getTypeSize(VET_FLOAT3);
	/// Allocate vertex buffer of the requested number of vertices (vertexCount) 
	/// and bytes per vertex (offset)
	HardwareVertexBufferSharedPtr vbuf =
		HardwareBufferManager::getSingleton().createVertexBuffer(
		offset, msh->sharedVertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY);
	/// Upload the vertex data to the card
	vbuf->writeData(0, vbuf->getSizeInBytes(), vertices, true);

	/// Set vertex buffer binding so buffer 0 is bound to our vertex buffer
	VertexBufferBinding* bind = msh->sharedVertexData->vertexBufferBinding;
	bind->setBinding(0, vbuf);

	// 2nd buffer
	offset = 0;
	decl->addElement(1, offset, VET_COLOUR, VES_DIFFUSE);
	offset += VertexElement::getTypeSize(VET_COLOUR);
	/// Allocate vertex buffer of the requested number of vertices (vertexCount) 
	/// and bytes per vertex (offset)
	vbuf = HardwareBufferManager::getSingleton().createVertexBuffer(
		offset, msh->sharedVertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY);
	/// Upload the vertex data to the card
	vbuf->writeData(0, vbuf->getSizeInBytes(), colours, true);

	/// Set vertex buffer binding so buffer 1 is bound to our colour buffer
	bind->setBinding(1, vbuf);

	/// Allocate index buffer of the requested number of vertices (ibufCount) 
	HardwareIndexBufferSharedPtr ibuf = HardwareBufferManager::getSingleton().
		createIndexBuffer(
		HardwareIndexBuffer::IT_16BIT,
		ibufCount,
		HardwareBuffer::HBU_STATIC_WRITE_ONLY);

	/// Upload the index data to the card
	ibuf->writeData(0, ibuf->getSizeInBytes(), faces, true);

	/// Set parameters of the submesh
	sub->useSharedVertices = true;
	sub->indexData->indexBuffer = ibuf;
	sub->indexData->indexCount = ibufCount;
	sub->indexData->indexStart = 0;

	/// Set bounding information (for culling)
	msh->_setBounds(AxisAlignedBox(-100, -100, -100, 100, 100, 100));
	msh->_setBoundingSphereRadius(Math::Sqrt(3 * 100 * 100));

	/// Notify -Mesh object that it has been loaded
	msh->load();
}
//---------------------------------------------------------------------------

void Game::buttonHit(OgreBites::Button *b)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("Button hit " + b->getName());
}

bool Game::mouseMoved(const OIS::MouseEvent &arg)
{
	if (mTrayMgr->injectMouseMove(arg)) return true;

	//for (auto it = _playerCameraMans.begin(); it != _playerCameraMans.end(); ++it) {
	//	(*it)->injectMouseMove(arg);
	//}

	return true;
}

void Game::createFrameListener(void)
{
	BaseApplication::createFrameListener();
	//Add buttons
	
	bulletManager.init(mSceneMgr, mTrayMgr);
	changeScreen(Screens::ScreenTypes::LOADING);

	//bulletManager.init(mSceneMgr, mTrayMgr);
	// Create your scene here :)
	mSceneMgr->setAmbientLight(Ogre::ColourValue(0.1, 0.1, 0.1));
	_gravityLabel = mTrayMgr->createLabel(OgreBites::TL_NONE, "gravityLabel", "", 200);
	_gravityLabel->_setLeft(0.05);
	_gravityLabel->_setTop(0.25);
	_gravityLabel->hide();

	//mSceneMgr->setSkyDome(true, "My/CloudySky", 1, 4, 45000, false);
	//mSceneMgr->setSkyBox(true, "Examples/TrippySkyBox", 50000, true);

	//lensFlare = new LensFlare(Ogre::Vector3(0, 3000, 0), (*_playerCameras.begin()), mSceneMgr);

	/*Ogre::MaterialPtr mat =
		Ogre::MaterialManager::getSingleton().create(
		"PlaneMat", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	Ogre::MovablePlane* mPlane = new Ogre::MovablePlane("Plane");
	mPlane->d = 0;
	mPlane->normal = Ogre::Vector3::UNIT_Y;
	mPlane->setCastShadows(false);
	mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	Ogre::MeshManager::getSingleton().createPlane(
		"PlaneMesh",
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		*mPlane,
		2000, 2000, 100, 100,
		true,
		10, 10, 10,
		Ogre::Vector3::UNIT_Z);
	Ogre::Entity* mPlaneEntity = mSceneMgr->createEntity("PlaneMesh");
	mPlaneEntity->setMaterialName("Examples/Rocky");
	Ogre::SceneNode* mPlaneNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("MainPlane");
	mPlaneNode->attachObject(mPlaneEntity);
	mPlaneNode->setPosition(Ogre::Vector3(0, 0, 0));
	std::vector<btVector3> planePoints;
	for (int i = 0; i < 100; i++) {
		planePoints.push_back(btVector3(-i * 10, 1, -i * 10));
		planePoints.push_back(btVector3(-i * 10, 1, i * 10));
		planePoints.push_back(btVector3(i * 10, 1, i * 10));
		planePoints.push_back(btVector3(i * 10, 1, -i * 10));
	}

	bulletManager.addCustomObject("MainPlane", btVector3(0, 0, 0), &planePoints, 1.0, 0.2, 0);*/

	/*std::vector<btVector3> planePoints2;
	planePoints2.push_back(btVector3(0, 1, 0));
	planePoints2.push_back(btVector3(100, 100, 100));
	planePoints2.push_back(btVector3(100, 100, -100));

	bulletManager.addGround("Cone1", btVector3(0, 0, 0), &planePoints2, 1.0, 0.2);
	createMesh("Mesh1", &planePoints2);
	Ogre::Entity* mesh1 = mSceneMgr->createEntity("MyMesh1", "Mesh1");
	mesh1->setCastShadows(true);
	MaterialPtr material = MaterialManager::getSingleton().create(
	"Test/ColourTest", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	mesh1->setMaterialName("Test/ColourTest");
	material->getTechnique(0)->getPass(0)->setVertexColourTracking(TVC_AMBIENT);
	Ogre::SceneNode* mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("MyMesh1");
	mNode->setPosition(0, 0, 0);
	mNode->attachObject(mesh1);

	std::vector<btVector3> planePoints3;
	planePoints3.push_back(btVector3(0, 1, 0));
	planePoints3.push_back(btVector3(100, 100, 100));
	planePoints3.push_back(btVector3(-100, 100, 100));

	bulletManager.addGround("Cone1", btVector3(0, 1, 0), &planePoints3, 1.0, 0.2);
	createMesh("Mesh2", &planePoints3);
	Ogre::Entity* mesh2 = mSceneMgr->createEntity("MyMesh2", "Mesh2");
	mesh2->setCastShadows(true);
	mesh2->setMaterialName("Test/ColourTest");
	Ogre::SceneNode* mNode2 = mSceneMgr->getRootSceneNode()->createChildSceneNode("MyMesh2");
	mNode2->setPosition(0, 0, 0);
	mNode2->attachObject(mesh2);

	std::vector<btVector3> planePoints4;
	planePoints4.push_back(btVector3(0, 1, 0));
	planePoints4.push_back(btVector3(-100, 100, 100));
	planePoints4.push_back(btVector3(-100, 100, -100));

	bulletManager.addGround("Cone1", btVector3(0, 0, 0), &planePoints4, 1.0, 0.2);
	createMesh("Mesh3", &planePoints4);
	Ogre::Entity* mesh3 = mSceneMgr->createEntity("MyMesh3", "Mesh3");
	mesh3->setCastShadows(true);
	mesh3->setMaterialName("Test/ColourTest");
	Ogre::SceneNode* mNode3 = mSceneMgr->getRootSceneNode()->createChildSceneNode("MyMesh3");
	mNode3->setPosition(0, 0, 0);
	mNode3->attachObject(mesh3);

	std::vector<btVector3> planePoints5;
	planePoints5.push_back(btVector3(0, 1, 0));
	planePoints5.push_back(btVector3(-100, 100, -100));
	planePoints5.push_back(btVector3(100, 100, -100));

	bulletManager.addGround("Cone1", btVector3(0, 0, 0), &planePoints5, 1.0, 0.2);
	createMesh("Mesh4", &planePoints5);
	Ogre::Entity* mesh4 = mSceneMgr->createEntity("MyMesh4", "Mesh4");
	mesh4->setCastShadows(true);
	mesh4->setMaterialName("Test/ColourTest");
	Ogre::SceneNode* mNode4 = mSceneMgr->getRootSceneNode()->createChildSceneNode("MyMesh4");
	mNode4->setPosition(0, 0, 0);
	mNode4->attachObject(mesh4);*/
}

bool Game::keyPressed(const OIS::KeyEvent &arg)
{
	BaseApplication::keyPressed(arg);
	if (arg.key == OIS::KC_ESCAPE)
	{
		//changeScreen(Screens::ScreenTypes::MENU);
	}
	if (arg.key == OIS::KC_L) {
		//_eventDispatcher.dispatchEvent(GameEvent::EventTypes::RACE_STARTED);
	}
	if (arg.key == OIS::KC_D) {
		_eventDispatcher.dispatchEvent(GameEvent::EventTypes::CREATE_DROP);
	}

	for (auto it = _players.begin(); it != _players.end(); ++it) {
		(*it)->keyPressed(arg);
	}
	return true;
}

bool Game::keyReleased(const OIS::KeyEvent &arg)
{
	BaseApplication::keyReleased(arg);
	for (auto it = _players.begin(); it != _players.end(); ++it) {
		(*it)->keyReleased(arg);
	}
	return true;
}

void Game::createCar()
{
	int vehicleCount = 2;
	int ID = -1;
	if (!_joysticks.empty()) {
		vehicleCount = _joysticks.size();
	}
	/*for (int i = 0; i < 10; i++) {
		Drop d(i, Drop::DropType::CAR_UPGRADE);
		d.setValue((i + 1));
		d.init(mSceneMgr, bulletManager.addCube(std::to_string(i) + "_drop", btVector3(4, 4, 4), btVector3(i * 10, 10, i * 10), 0.5, 0.0, 0.0, 0.2));
		d.setPosition(Ogre::Vector3(10, i * 30, 10));

		_drops.push_back(d);
	}*/

	for (int i = 0; i < vehicleCount; i++) {

		ID++;
		std::vector<btRigidBody*> wheels;
		for (int j = 0; j < 4; j++) {
			if (!_joysticks.empty()) {
				ID = _joysticks.at(i)->getID();
			}
			std::string name = "tire_" + std::to_string(ID) + "_" + std::to_string(j);
			Ogre::Entity* potatoEntity = mSceneMgr->createEntity(name, "cube.mesh");

			potatoEntity->setMaterialName("My/Black");
			potatoEntity->setCastShadows(true);

			size_t vertex_count, index_count;
			Vector3* vertices;
			unsigned* indices;

			MeshUtilities::getMeshInformation(potatoEntity->getMesh(), vertex_count, vertices, index_count, indices);
			std::vector<btVector3> wheelPoints;
			for (int k = 0; k < index_count; k++) {
				wheelPoints.push_back(btVector3(vertices[indices[k]].x, vertices[indices[k]].y, vertices[indices[k]].z));
			}

			Ogre::SceneNode* wheelNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
			wheelNode->attachObject(potatoEntity);
			//wheelNode->setScale(Ogre::Vector3(2, 2, 2));
			//wheelNode->showBoundingBox(true);
		}

		btRigidBody* chasis;
		{
			std::string name = "car_" + std::to_string(ID);
			Ogre::Entity* carEntity = mSceneMgr->createEntity(name, "car.mesh");
			if (i == 0) {
				carEntity->setMaterialName("My/Green");
			}
			else if (i == 1) {
				carEntity->setMaterialName("My/Pink");
			}
			else if (i == 2) {
				carEntity->setMaterialName("My/Orange");
			}
			else if (i == 3) {
				carEntity->setMaterialName("My/Red");
			}
			carEntity->setCastShadows(true);
			size_t vertex_count, index_count;
			Vector3* vertices;
			unsigned* indices;
			
			MeshUtilities::getMeshInformation(carEntity->getMesh(), vertex_count, vertices, index_count, indices);
			std::vector<btVector3> chasisPoints;
			for (int i = 0; i < index_count; i++) {
				chasisPoints.push_back(btVector3(vertices[indices[i]].x, vertices[indices[i]].y, vertices[indices[i]].z));
			}

			Ogre::SceneNode* carNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
			carNode->attachObject(carEntity);
			
			float mass = _requestThreadController->getRequestResults()->getVehicles().begin()->mass;
			float speed = _requestThreadController->getRequestResults()->getVehicles().begin()->speed;
			Player* p = new Player;
			btVector3 start((i + 1) * 20, (_requestThreadController->getRequestResults()->getTrackInfo()->mapPoints[1 * _requestThreadController->getRequestResults()->getTrackInfo()->width + 1] + 0.1) * 100, (i + 1) * 20);
			p->setVehicle(bulletManager.createCar(ID, name, start, &chasisPoints, 0.3, 0.02, mass, (*_requestThreadController->getRequestResults()->getVehicles().begin()), wheels), carNode);
			p->setCamera(_playerCameras.at(i));
			p->init(mTrayMgr);
			p->setSceneManager(mSceneMgr);

			if (i == 0) {
				p->setControls(OIS::KC_U, OIS::KC_J, OIS::KC_SPACE, OIS::KC_H, OIS::KC_K, OIS::KC_I, true);
			}
			else {
				p->setControls(OIS::KC_UP, OIS::KC_DOWN, OIS::KC_RCONTROL, OIS::KC_LEFT, OIS::KC_RIGHT, OIS::KC_RSHIFT, true);
			}

			_eventDispatcher.addListener(p);
			_players.push_back(p);
		}
	}


	/*for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			std::string name = "cone_" + std::to_string(i) + "_" + std::to_string(j);
			Ogre::Entity* potatoEntity = mSceneMgr->createEntity(name, "konuss.mesh");

			potatoEntity->setMaterialName("My/Red");

			potatoEntity->setCastShadows(true);
			size_t vertex_count, index_count;
			Vector3* vertices;
			unsigned* indices;

			MeshUtilities::getMeshInformation(potatoEntity->getMesh(), vertex_count, vertices, index_count, indices);
			std::vector<btVector3> wheelPoints;
			for (int k = 0; k < index_count; k++) {
				wheelPoints.push_back(btVector3(vertices[indices[k]].x, vertices[indices[k]].y, vertices[indices[k]].z));
			}

			Ogre::SceneNode* wheelNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
			wheelNode->attachObject(potatoEntity);

			bulletManager.addCustomObject(name, btVector3(10 - i * 50, 3, 10 - j * 50), &wheelPoints, 0.3, 10, 1);
		}
	}*/

	for (int j = 0; j < 10; j++) {
		std::string name = "ramp_" + std::to_string(j);
		Ogre::Entity* rampEntity = mSceneMgr->createEntity(name, "rampa.mesh");

		rampEntity->setMaterialName("My/Blue");

		rampEntity->setCastShadows(true);
		size_t vertex_count, index_count;
		Vector3* vertices;
		unsigned* indices;

		MeshUtilities::getMeshInformation(rampEntity->getMesh(), vertex_count, vertices, index_count, indices);
		std::vector<btVector3> rampPoints;
		for (int k = 0; k < index_count; k+=3) {
			rampPoints.push_back(btVector3(vertices[indices[k]].x, vertices[indices[k]].y, vertices[indices[k]].z));
			rampPoints.push_back(btVector3(vertices[indices[k + 1]].x, vertices[indices[k + 1]].y, vertices[indices[k + 1]].z));
			rampPoints.push_back(btVector3(vertices[indices[k + 2]].x, vertices[indices[k + 2]].y, vertices[indices[k + 2]].z));
		}
		Ogre::SceneNode* rampNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
		rampNode->attachObject(rampEntity);

		bulletManager.addCustomObject(name, btVector3(j * 40, 3, j * 40), &rampPoints, 0.1, 10, 1000);
	}

}

bool Game::axisMoved(const OIS::JoyStickEvent &e, int axis)
{
	int x = e.state.mAxes[axis].abs;
	bulletManager.axisMoved(e, axis);
	return true;
}
bool Game::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
	bulletManager.buttonPressed(arg, button);
	return true;
};
bool Game::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
	bulletManager.buttonReleased(arg, button);
	return true;
};
bool Game::povMoved(const OIS::JoyStickEvent &arg, int pov)
{
	bulletManager.povMoved(arg, pov);
	return true;
};

void Game::changeScreen(Screens::ScreenTypes type)
{
	if (_screen) {
		if (_screen->getScreenType() == type) {
			return;
		}
		// We need different screen, so we can just destroy the current one
		_screen->destroy();
	}
	
	if (type == Screens::ScreenTypes::LOADING) {
		Screens::LoadingScreen* loadingScreen = new Screens::LoadingScreen();
		loadingScreen->setRequestThreadController(_requestThreadController);
		_screen = loadingScreen;
	}
	else if (type == Screens::ScreenTypes::MENU) {
		Screens::MenuScreen* menuScreen = new Screens::MenuScreen();
		for (auto it = _joysticks.begin(); it != _joysticks.end(); ++it) {
			(*it)->setEventCallback(menuScreen);
		}
		_screen = menuScreen;
	} 
	else if (type == Screens::ScreenTypes::PLAY) {
		_screen = new Screens::PlayScreen();
		bulletManager.createGround(_requestThreadController->getRequestResults()->getTrackInfo());
		createCar();
		for (int i = 0; i < 50; i++) {
			createDrop();
		}
		for (auto it = _joysticks.begin(); it != _joysticks.end(); ++it) {
			(*it)->setEventCallback(this);
		}
	}
	if (_screen) {
		_screen->setSceneManager(mSceneMgr);
		_screen->setTrayManager(mTrayMgr);
		_screen->setEventDispatcher(&_eventDispatcher);
		_screen->setBulletManager(&bulletManager);

		//create all the stuff needed for this screen
		_screen->create();
	}
	
}

void Game::preViewportUpdate(const Ogre::RenderTargetViewportEvent& evt)
{
	for (int i = 0; i < _playerViewports.size(); i++) {
		if (evt.source == _playerViewports.at(i)) {
			//Show only the player's GUI which is currently rendering
			_players.at(i)->showGui();
		}
		else {
			//Hide other player's GUI elements
			_players.at(i)->hideGui();
		}
		_players.at(i)->updateEffects();
	}
}


void Game::setRequestHandler(ServerRequests::RequestThreadController* a)
{
	_requestThreadController = a;
}

void Game::eventTriggered(GameEvent::EventTypes evt)
{
	BaseApplication::eventTriggered(evt);
	switch (evt) {
	case GameEvent::EventTypes::CREATE_DROP:
		createDrop();
		break;
	}
}

void Game::createDrop()
{
	float result = Ogre::Math::RangeRandom(0, 4);
	Drop::DropType t;
	int value = 0;
	int valueTime = 0;
	if (result <= 1) {
		t = Drop::DropType::GRAVITY;
		value = 10;
		valueTime = 5;
	}
	else if (result <= 2) {
		t = Drop::DropType::CAR_UPGRADE;
		value = 200;
	}
	else if (result <= 3) {
		t = Drop::DropType::CAR_SPIN;
	}
	else if (result <= 4) {
		t = Drop::DropType::CAR_ICE;
		valueTime = 5;
	}

	Drop d(_drops.size() , t);
	d.setValue(value);
	d.setValueTime(valueTime);
	float posX = (float)_requestThreadController->getRequestResults()->getTrackInfo()->width / 2.0f * (float)_requestThreadController->getRequestResults()->getTrackInfo()->scale - 10.0f;
	float posY = (float)_requestThreadController->getRequestResults()->getTrackInfo()->height / 2.0f * (float)_requestThreadController->getRequestResults()->getTrackInfo()->scale - 10.0f;
	Ogre::SceneNode* cube = bulletManager.addCube(std::to_string(d.getID()) + "_drop", btVector3(4, 4, 4), btVector3(Ogre::Math::RangeRandom(-posX, posX), 100, Ogre::Math::RangeRandom(-posY, posY)), 10, 1.0, 0.0, 0.0, 0.9);
	d.init(mSceneMgr, cube);
	_drops.push_back(d);
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

/**
 * This is the thread function which will handle outgoing requests
 */
void requestThreadFunction(void* pointer)
{
	std::clog << "thread started!" << std::endl;
	ServerRequests::RequestThreadController* requestThreadController = reinterpret_cast<ServerRequests::RequestThreadController*>(pointer);
	while (requestThreadController->go()) {
		//Do this till we get a false back from the go() method
	}
	std::clog << "thread ending" << std::endl;
}

/**
* Mutex to avoid reading and writing at the same time to the _taskList
*/
//std::mutex _taskListMutex;

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		std::ofstream coutFile;
		coutFile.open("cout.log");
		std::ofstream cerrFile;
		cerrFile.open("cerr.log");
		std::ofstream clogFile;
		clogFile.open("clog.log");

		//Store current buffers so we can reasign them when program ends it's execution
		std::streambuf* backupCout = std::cout.rdbuf();
		std::streambuf* backupCerr = std::cerr.rdbuf();
		std::streambuf* backupClog = std::clog.rdbuf();

		//Replace cout,cerr,clog buffers with our custom stream buffers
		std::cout.rdbuf(coutFile.rdbuf());
		std::cerr.rdbuf(cerrFile.rdbuf());
		std::clog.rdbuf(clogFile.rdbuf());
		std::clog << "Creating thread" << std::endl;
		
		ServerRequests::RequestThreadController requestThreadController;

		// Create application object
		Game app;
		std::thread requestThread(requestThreadFunction, &requestThreadController);
		app.setRequestHandler(&requestThreadController);

		requestThreadController.addTask(ServerRequests::TaskList::GET_VEHICLES);
		requestThreadController.addTask(ServerRequests::TaskList::GET_TRACK);

		try {
			app.go();
		}
		catch (Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		requestThreadController.shutdown();
		requestThread.join();

		std::cout.rdbuf(backupCout);
		std::cerr.rdbuf(backupCerr);
		std::clog.rdbuf(backupClog);

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
