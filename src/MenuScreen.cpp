#include "stdafx.h"
#include "MenuScreen.h"
using namespace Screens;

MenuScreen::MenuScreen() :
BasicScreen(ScreenTypes::MENU),
_currentView(ViewTypes::MAIN),
_selectedPlayer(0),
_currentButtonListener(ControlOptions::NONE)
{
	_logger = Ogre::LogManager::getSingletonPtr()->createLog("MenuScreen.log", false, false);
	for (int i = 0; i < 8; i++) {
		_controls[i].left = 0;
		_controls[i].right = 0;
		_controls[i].accelerate = 0;
		_controls[i].brake = 0;
		_controls[i].reset = 0;
	}
}


MenuScreen::~MenuScreen()
{
}

void MenuScreen::create()
{
	_logger->logMessage("creating screen");

	//This should be called before buttons are created!!!
	getTrayManager()->setListener(this);
	getTrayManager()->showCursor();
	createMainView();

	_backgroundCar = getSceneManager()->getRootSceneNode()->createChildSceneNode("menu_node");
	Ogre::Entity* entity = getSceneManager()->createEntity("car.mesh");

	Ogre::Light* light = getSceneManager()->createLight("SecondLight");
	light->setType(Ogre::Light::LT_POINT);
	light->setPosition(0, 100, 0);
	light->setCastShadows(true);
	//light->setAttenuation(3250, 1.0, 0.0014, 0.000007);
	light->setDiffuseColour(1.0, 1.0, 1.0);
	light->setSpecularColour(1.0, 1.0, 1.0);
	_backgroundCar->attachObject(entity);
	Ogre::Camera* camera = getSceneManager()->getCamera("First_Camera");
	camera->setPosition(_backgroundCar->getPosition().x + 10, _backgroundCar->getPosition().y + 10, _backgroundCar->getPosition().z + 10);
	camera->lookAt(_backgroundCar->getPosition());
}

void MenuScreen::destroy()
{
	_logger->logMessage("destroying screen");
	getTrayManager()->setListener(0);
	getSceneManager()->destroyAllEntities();
	getSceneManager()->destroySceneNode("menu_node");
	getSceneManager()->destroyAllLights();
}

void MenuScreen::update(const Ogre::FrameEvent evt)
{
	//_backgroundCar->yaw(Ogre::Angle(10.0f * evt.timeSinceLastFrame));
}

void MenuScreen::buttonHit(OgreBites::Button* button)
{
	_logger->logMessage("Button hit " + button->getName());
	switch (_currentView) {
	case ViewTypes::MAIN:
		if (button->getName() == "play") {
			createView(ViewTypes::PLAY);
		}
		else if (button->getName() == "options") {
			createView(ViewTypes::OPTIONS);
		}
		else if (button->getName() == "exit") {
			setScreenEvent(ScreenEvents::EXIT_GAME);
		}
		break;
	case ViewTypes::OPTIONS:
		if (button->getName() == "video") {
			createView(ViewTypes::OPTIONS_VIDEO);
		}
		else if (button->getName() == "controls") {
			createView(ViewTypes::OPTIONS_CONTROLS);
		}
		else if (button->getName() == "back") {
			createView(ViewTypes::MAIN);
		}
		break;
	case ViewTypes::OPTIONS_CONTROLS:
		if (button->getName() == "player") {
			_selectedPlayer++;
			if (_selectedPlayer > 7) {
				_selectedPlayer = 0;
			}
			button->setCaption("Player " + std::to_string(_selectedPlayer + 1));
			updatecontrolsView();
		}
		else if (button->getName() == "left") {
			_controllerValue = &_controls[_selectedPlayer];
			_currentButtonListener = ControlOptions::LEFT;
		}
		else if (button->getName() == "right") {
			_controllerValue = &_controls[_selectedPlayer];
			_currentButtonListener = ControlOptions::RIGHT;
		}
		else if (button->getName() == "accelerate") {
			_controllerValue = &_controls[_selectedPlayer];
			_currentButtonListener = ControlOptions::ACCELERATE;
		}
		else if (button->getName() == "brake") {
			_controllerValue = &_controls[_selectedPlayer];
			_currentButtonListener = ControlOptions::BRAKE;
		}
		else if (button->getName() == "reset") {
			_controllerValue = &_controls[_selectedPlayer];
			_currentButtonListener = ControlOptions::RESET;
		}
		if (button->getName() == "back") {
			createView(ViewTypes::OPTIONS);
		}
		break;
	case ViewTypes::OPTIONS_VIDEO:
		if (button->getName() == "back") {
			createView(ViewTypes::OPTIONS);
		}
		break;
	case ViewTypes::PLAY:
		if (button->getName() == "player_count"){
			destroyPlayView();
			setScreenEvent(ScreenEvents::GO_TO_PLAY_SCREEN);
			getEventDispatcher()->dispatchEvent(GameEvent::EventTypes::PLAYERS_JOINING);
		}
		else if (button->getName() == "back") {
			createView(ViewTypes::MAIN);
		}
		break;
	}
}

void MenuScreen::createView(ViewTypes type)
{
	//Destroy previous view first
	switch (_currentView) {
	case MenuScreen::ViewTypes::MAIN:
		destroyMainView();
		break;
	case MenuScreen::ViewTypes::OPTIONS:
		destroyOptionsView();
		break;
	case MenuScreen::ViewTypes::OPTIONS_CONTROLS:
		destroyControlsView();
		break;
	case MenuScreen::ViewTypes::OPTIONS_VIDEO:
		destroyVideoView();
		break;
	case MenuScreen::ViewTypes::PLAY:
		destroyPlayView();
		break;
	}

	//Create new view
	_currentView = type;
	switch (_currentView) {
	case MenuScreen::ViewTypes::MAIN:
		createMainView();
		break;
	case MenuScreen::ViewTypes::OPTIONS:
		createOptionsView();
		break;
	case MenuScreen::ViewTypes::OPTIONS_CONTROLS:
		createControlsView();
		break;
	case MenuScreen::ViewTypes::OPTIONS_VIDEO:
		createVideoView();
		break;
	case MenuScreen::ViewTypes::PLAY:
		createPlayView();
		break;
	}
}

bool MenuScreen::axisMoved(const OIS::JoyStickEvent &e, int axis)
{
	if (_currentButtonListener != ControlOptions::NONE) {
		switch (_currentButtonListener) {
		case ControlOptions::LEFT:
			_controllerValue->setLeft(axis, true);
			break;
		case ControlOptions::RIGHT:
			_controllerValue->setRight(axis, true);
			break;
		case ControlOptions::ACCELERATE:
			_controllerValue->setAccelerate(axis, true);
			break;
		case ControlOptions::BRAKE:
			_controllerValue->setBrake(axis, true);
			break;
		case ControlOptions::RESET:
			_controllerValue->setReset(axis, true);
			break;
		}
		updatecontrolsView();
		_currentButtonListener = ControlOptions::NONE;
	}

	return true;
}

bool MenuScreen::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
	if (_currentButtonListener != ControlOptions::NONE) {
		switch (_currentButtonListener) {
		case ControlOptions::LEFT:
			_controllerValue->setLeft(button, false);
			break;
		case ControlOptions::RIGHT:
			_controllerValue->setRight(button, false);
			break;
		case ControlOptions::ACCELERATE:
			_controllerValue->setAccelerate(button, false);
			break;
		case ControlOptions::BRAKE:
			_controllerValue->setBrake(button, false);
			break;
		case ControlOptions::RESET:
			_controllerValue->setReset(button, false);
			break;
		}
		updatecontrolsView();
		_currentButtonListener = ControlOptions::NONE;
	}
	return true;
}

bool MenuScreen::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
	return true;
}

bool MenuScreen::povMoved(const OIS::JoyStickEvent &arg, int pov)
{
	if (_currentButtonListener != ControlOptions::NONE) {
		switch (_currentButtonListener) {
		case ControlOptions::LEFT:
			_controllerValue->setLeft(pov, false);
			break;
		case ControlOptions::RIGHT:
			_controllerValue->setRight(pov, false);
			break;
		case ControlOptions::ACCELERATE:
			_controllerValue->setAccelerate(pov, false);
			break;
		case ControlOptions::BRAKE:
			_controllerValue->setBrake(pov, false);
			break;
		case ControlOptions::RESET:
			_controllerValue->setReset(pov, false);
			break;
		}
		updatecontrolsView();
		_currentButtonListener = ControlOptions::NONE;
	}
	return true;
}

void MenuScreen::createMainView()
{
	getTrayManager()->createButton(OgreBites::TL_CENTER, "play", "Play", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "options", "Options", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "exit", "Exit", 200);
}

void MenuScreen::destroyMainView()
{
	getTrayManager()->destroyWidget("play");
	getTrayManager()->destroyWidget("options");
	getTrayManager()->destroyWidget("exit");
}

void MenuScreen::createOptionsView()
{
	getTrayManager()->createButton(OgreBites::TL_CENTER, "video", "Video", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "controls", "Controls", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "back", "Back", 200);
}

void MenuScreen::destroyOptionsView()
{
	getTrayManager()->destroyWidget("video");
	getTrayManager()->destroyWidget("controls");
	getTrayManager()->destroyWidget("back");
}

void MenuScreen::createVideoView()
{
	getTrayManager()->createButton(OgreBites::TL_CENTER, "resolution", "Resolution", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "back", "back", 200);
}

void MenuScreen::destroyVideoView()
{
	getTrayManager()->destroyWidget("resolution");
	getTrayManager()->destroyWidget("back");
}

void MenuScreen::createControlsView()
{
	getTrayManager()->createButton(OgreBites::TL_CENTER, "player", "Player " + std::to_string(_selectedPlayer + 1), 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "left", "Left btn", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "right", "Right btn", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "accelerate", "Accelerate btn", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "brake", "Brake btn", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "reset", "Reset btn", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "back", "back", 200);
}

void MenuScreen::destroyControlsView()
{
	getTrayManager()->destroyWidget("player");
	getTrayManager()->destroyWidget("left");
	getTrayManager()->destroyWidget("right");
	getTrayManager()->destroyWidget("accelerate");
	getTrayManager()->destroyWidget("brake");
	getTrayManager()->destroyWidget("reset");
	getTrayManager()->destroyWidget("back");
}

void MenuScreen::updatecontrolsView()
{
	OgreBites::Button* left = static_cast<OgreBites::Button*>(getTrayManager()->getWidget("left"));
	if (_controls[_selectedPlayer].leftAnalog) {
		left->setCaption("Left btn analog " + std::to_string(_controls[_selectedPlayer].left));
	}
	else {
		left->setCaption("Left btn" + std::to_string(_controls[_selectedPlayer].left));
	}

	OgreBites::Button* right = static_cast<OgreBites::Button*>(getTrayManager()->getWidget("right"));
	if (_controls[_selectedPlayer].rightAnalog) {
		right->setCaption("Right btn analog " + std::to_string(_controls[_selectedPlayer].right));
	}
	else {
		right->setCaption("Right btn " + std::to_string(_controls[_selectedPlayer].right));
	}
	
	OgreBites::Button* accelerate = static_cast<OgreBites::Button*>(getTrayManager()->getWidget("accelerate"));
	if (_controls[_selectedPlayer].accelerateAnalog) {
		accelerate->setCaption("accelerate btn analog " + std::to_string(_controls[_selectedPlayer].accelerate));
	}
	else {
		accelerate->setCaption("accelerate btn " + std::to_string(_controls[_selectedPlayer].accelerate));
	}
	
	OgreBites::Button* brake = static_cast<OgreBites::Button*>(getTrayManager()->getWidget("brake"));
	if (_controls[_selectedPlayer].brakeAnalog) {
		brake->setCaption("brake btn analog " + std::to_string(_controls[_selectedPlayer].brake));
	}
	else {
		brake->setCaption("brake btn " + std::to_string(_controls[_selectedPlayer].brake));
	}
	
	OgreBites::Button* reset = static_cast<OgreBites::Button*>(getTrayManager()->getWidget("reset"));
	if (_controls[_selectedPlayer].resetAnalog) {
		reset->setCaption("reset btn analog " + std::to_string(_controls[_selectedPlayer].reset));
	}
	else{
		reset->setCaption("reset btn " + std::to_string(_controls[_selectedPlayer].reset));
	}
}

void MenuScreen::createPlayView()
{
	getTrayManager()->createButton(OgreBites::TL_CENTER, "player_count", "Player count 1", 200);
	getTrayManager()->createButton(OgreBites::TL_CENTER, "back", "back", 200);
}

void MenuScreen::destroyPlayView()
{
	getTrayManager()->destroyWidget("player_count");
	getTrayManager()->destroyWidget("back");
}