#include "stdafx.h"
#include "BasicScreen.h"

using namespace Screens;

BasicScreen::BasicScreen(ScreenTypes type):
_type(type),
_event(ScreenEvents::NONE)
{
}


BasicScreen::~BasicScreen()
{
}

ScreenTypes BasicScreen::getScreenType()
{
	return _type;
}

void BasicScreen::setSceneManager(Ogre::SceneManager* mgr)
{
	_sceneManager = mgr;
}

void BasicScreen::setTrayManager(OgreBites::SdkTrayManager* mgr)
{
	_trayManager = mgr;
}

void BasicScreen::setEventDispatcher(GameEvent::GameEventDispatcher* evt)
{
	_eventDispatcher = evt;
}

void BasicScreen::setBulletManager(BulletManager* mgr)
{
	_bulletManager = mgr;
}

Ogre::SceneManager* BasicScreen::getSceneManager()
{
	return _sceneManager;
}
OgreBites::SdkTrayManager* BasicScreen::getTrayManager()
{
	return _trayManager;
}

GameEvent::GameEventDispatcher* BasicScreen::getEventDispatcher()
{
	return _eventDispatcher;
}

BulletManager* BasicScreen::getBulletManager()
{
	return _bulletManager;
}

void BasicScreen::setScreenEvent(ScreenEvents evt)
{
	_event = evt;
}

ScreenEvents BasicScreen::getScreenEvent()
{
	return _event;
}