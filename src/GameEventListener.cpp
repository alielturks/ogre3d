#include "stdafx.h"
#include "GameEventListener.h"

using namespace GameEvent;

GameEventDispatcher::GameEventDispatcher()
{
}

GameEventDispatcher::~GameEventDispatcher()
{
}

void GameEventDispatcher::addListener(GameEventListener* listener)
{
	for (auto it = _listeners.begin(); it != _listeners.end(); ++it) {
		if ((*it) == listener) {
			//This listener is already added
			return;
		}
	}
	_listeners.push_back(listener);
}

void GameEventDispatcher::removeListener(GameEventListener* listener)
{
	for (auto it = _listeners.begin(); it != _listeners.end(); ++it) {
		if ((*it) == listener) {
			it = _listeners.erase(it);
			return;
		}
	}
}

void GameEventDispatcher::dispatchEvent(EventTypes type)
{
	for (auto it = _listeners.begin(); it != _listeners.end(); ++it) {
		(*it)->eventTriggered(type);
	}
}