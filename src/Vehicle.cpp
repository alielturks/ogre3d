#include "stdafx.h"
#include "Vehicle.h"

Vehicle::Vehicle()
{
}

Vehicle::~Vehicle()
{

}

void Vehicle::init()
{
	//_vehicleInfo.maxSteerValue = 0.5;

	vehicleTuning = new btRaycastVehicle::btVehicleTuning();
	btVehicleRaycaster* vehicleRaycaster = new btDefaultVehicleRaycaster(_world);
	int rightIndex = 0;
	int upIndex = 1;
	int forwardIndex = 2;
	_raycastVehicle = new btRaycastVehicle(*vehicleTuning, _chasis, vehicleRaycaster);
	//vehicleTuning->m_maxSuspensionTravelCm = 1;
	//vehicleTuning->m_suspensionStiffness = 1;
	float connectionHeight = -0.9f;//1.2f;
	addWheel(btVector3(3.5, connectionHeight, 3.0), 0, true);
	addWheel(btVector3(-3.5, connectionHeight, 3.0), 1, true);
	addWheel(btVector3(3.5, connectionHeight, -3.5), 2, false);
	addWheel(btVector3(-3.5, connectionHeight, -3.5), 3, false);
	_raycastVehicle->setCoordinateSystem(rightIndex, upIndex, forwardIndex);
	_world->addVehicle(_raycastVehicle);
}

void Vehicle::setChasis(btRigidBody* body)
{
	_chasis = body;
}
void Vehicle::setWorld(btDynamicsWorld* world)
{
	_world = world;
}

void Vehicle::accelerate(bool val)
{
	_stateAccelerate = val;
}

void Vehicle::backward(bool val)
{
	_stateBackward = val;
}

void Vehicle::brake(bool val)
{
	_stateBrake = val;
}

void Vehicle::steerLeft(bool val)
{
	_stateSteerLeft = val;
}

void Vehicle::steerRight(bool val)
{
	_stateSteerRight = val;
}

void Vehicle::update(const Ogre::FrameEvent& evt)
{
	if (_stateAccelerate) {
		_forwardMomentum += evt.timeSinceLastFrame / 5.0f;
		_backwardMomentum = 0;
		if (_forwardMomentum > 1) {
			_forwardMomentum = 1;
		}
		_raycastVehicle->getRigidBody()->activate();
		if (_vehicleInfo.front_drive) {
			_raycastVehicle->applyEngineForce(_vehicleInfo.speed * _forwardMomentum, 0);
			_raycastVehicle->applyEngineForce(_vehicleInfo.speed * _forwardMomentum, 1);
		}
		if (_vehicleInfo.rear_drive) {
			_raycastVehicle->applyEngineForce(_vehicleInfo.speed * _forwardMomentum, 2);
			_raycastVehicle->applyEngineForce(_vehicleInfo.speed * _forwardMomentum, 3);
		}
	}
	else if (_stateBackward) {
		_backwardMomentum += evt.timeSinceLastFrame / 5.0f;
		_forwardMomentum = 0;
		if (_backwardMomentum > 1) {
			_backwardMomentum = 1;
		}
		_raycastVehicle->getRigidBody()->activate();
		if (_vehicleInfo.rear_drive) {
			_raycastVehicle->applyEngineForce(-(_vehicleInfo.speed / 5.0f) * _backwardMomentum, 0);
			_raycastVehicle->applyEngineForce(-(_vehicleInfo.speed / 5.0f) * _backwardMomentum, 1);
		}
		if (_vehicleInfo.front_drive) {
			_raycastVehicle->applyEngineForce(-(_vehicleInfo.speed / 5.0f) * _backwardMomentum, 2);
			_raycastVehicle->applyEngineForce(-(_vehicleInfo.speed / 5.0f) * _backwardMomentum, 3);
		}
		
	}
	else {
		if (_forwardMomentum > 0) {
			_forwardMomentum -= evt.timeSinceLastFrame;
			if (_forwardMomentum < 0) {
				_forwardMomentum = 0;
			}
		}
		if (_backwardMomentum > 0) {
			_backwardMomentum -= evt.timeSinceLastFrame;
			if (_backwardMomentum < 0) {
				_backwardMomentum = 0;
			}
		}
		_raycastVehicle->applyEngineForce(0, 0);
		_raycastVehicle->applyEngineForce(0, 1);
		_raycastVehicle->applyEngineForce(0, 2);
		_raycastVehicle->applyEngineForce(0, 3);
	}

	if (_stateBrake) {
		_raycastVehicle->getRigidBody()->activate();
		_raycastVehicle->setBrake(_vehicleInfo.brakeForce, 0);
		_raycastVehicle->setBrake(_vehicleInfo.brakeForce, 1);
		_raycastVehicle->setBrake(_vehicleInfo.brakeForce, 2);
		_raycastVehicle->setBrake(_vehicleInfo.brakeForce, 3);
	}

	if (_stateSteerLeft) {
		_raycastVehicle->getRigidBody()->activate();

		_raycastVehicle->setSteeringValue(_raycastVehicle->getSteeringValue(0) + evt.timeSinceLastFrame, 0);
		_raycastVehicle->setSteeringValue(_raycastVehicle->getSteeringValue(1) + evt.timeSinceLastFrame, 1);

		if (_raycastVehicle->getSteeringValue(0) > _vehicleInfo.maxSteerValue) {
			_raycastVehicle->setSteeringValue(_vehicleInfo.maxSteerValue, 0);
			_raycastVehicle->setSteeringValue(_vehicleInfo.maxSteerValue, 1);
		}
	} else if (_stateSteerRight) {
		_raycastVehicle->getRigidBody()->activate();


		_raycastVehicle->setSteeringValue(_raycastVehicle->getSteeringValue(0) - evt.timeSinceLastFrame, 0);
		_raycastVehicle->setSteeringValue(_raycastVehicle->getSteeringValue(1) - evt.timeSinceLastFrame, 1);


		if (_raycastVehicle->getSteeringValue(0) < -_vehicleInfo.maxSteerValue) {
			_raycastVehicle->setSteeringValue(-_vehicleInfo.maxSteerValue, 0);
			_raycastVehicle->setSteeringValue(-_vehicleInfo.maxSteerValue, 1);
		}
	}
	else {
		_raycastVehicle->setSteeringValue(_raycastVehicle->getSteeringValue(0) / 1.02f, 0);
		_raycastVehicle->setSteeringValue(_raycastVehicle->getSteeringValue(1) / 1.02f, 1);
	}

	if (_stateRoll) {
		//_chasis->applyTorque(btVector3(0, 5000, 0));
	}

	for (int i = 0; i < 4; i++) {
		_wheelInfo.at(i) = _raycastVehicle->getWheelTransformWS(i);

		_wheelNodes.at(i)->setPosition(Ogre::Vector3(_wheelInfo.at(i).getOrigin().getX(), _wheelInfo.at(i).getOrigin().getY() - 1, _wheelInfo.at(i).getOrigin().getZ()));
		_wheelNodes.at(i)->setOrientation(_wheelInfo.at(i).getRotation().getW(), _wheelInfo.at(i).getRotation().getX(), _wheelInfo.at(i).getRotation().getY(), _wheelInfo.at(i).getRotation().getZ());
	}	
	if (_iceWheelsEnabled) {
		_iceWheelsTime -= evt.timeSinceLastFrame;
		if (_iceWheelsTime < 0) {
			_iceWheelsEnabled = false;
			_iceWheelsTime = 0;
			_raycastVehicle->getWheelInfo(0).m_frictionSlip = _vehicleInfo.friction;
			_raycastVehicle->getWheelInfo(1).m_frictionSlip = _vehicleInfo.friction;
			_raycastVehicle->getWheelInfo(2).m_frictionSlip = _vehicleInfo.friction;
			_raycastVehicle->getWheelInfo(3).m_frictionSlip = _vehicleInfo.friction;
		}
	}
}

void Vehicle::setTrayManager(OgreBites::SdkTrayManager* mgr)
{
	_trayManager = mgr;
}

void Vehicle::addWheel(btVector3& position, int index, bool frontWheel)
{
	//
	_wheelNodes.push_back(_sceneManager->getSceneNode("tire_" + std::to_string(_index) + "_" + std::to_string(index)));
	btVector3 wheelDirectionCS0(0, -1, 0);
	btVector3 wheelAxleCS(-1, 0, 0);
	
	btRaycastVehicle::btVehicleTuning* vTuning = new btRaycastVehicle::btVehicleTuning();
	vTuning->m_frictionSlip = _vehicleInfo.friction;
	vTuning->m_maxSuspensionForce = _vehicleInfo.suspensionForce; //
	vTuning->m_maxSuspensionTravelCm = _vehicleInfo.suspensionTravel;
	vTuning->m_suspensionCompression = _vehicleInfo.suspensionCompression;
	vTuning->m_suspensionDamping = _vehicleInfo.suspensionDamping; //Bounciness
	vTuning->m_suspensionStiffness = _vehicleInfo.suspensionStiffness;

	btWheelInfo info = _raycastVehicle->addWheel(position, wheelDirectionCS0, wheelAxleCS, 1.5, 1, *vTuning, frontWheel);
	_raycastVehicle->setSteeringValue(0, index);
	_wheelInfo.push_back(info.m_worldTransform);
}

void Vehicle::setWheels(std::vector<btRigidBody*> wheels)
{
	_wheels = wheels;
}

std::vector<btTransform>* Vehicle::getWheelInfo()
{
	return &_wheelInfo;
}

void Vehicle::setSceneManager(Ogre::SceneManager* mgr)
{
	_sceneManager = mgr;
}

void Vehicle::setIndex(int ind)
{
	_index = ind;
}

void Vehicle::resetCar()
{
	_chasis->activate();
	_chasis->setWorldTransform(btTransform(btQuaternion(0, 0, 0, 1), _chasis->getWorldTransform().getOrigin()));
	//_chasis->getWorldTransform().getOrigin();
	_chasis->setAngularVelocity(btVector3(0, 0, 0));
	_chasis->setLinearVelocity(btVector3(0, 0, 0));
}

void Vehicle::setSteer(float val)
{
	_steerValue = val;

}

float Vehicle::getSpeed()
{
	return _raycastVehicle->getCurrentSpeedKmHour();
}

void Vehicle::roll(bool val)
{
	_chasis->applyCentralImpulse(btVector3(0, 8000, 0));
	//_chasis->applyTorqueImpulse(btVector3(0, 20000, 0));
	//_stateRoll = val;
}

int Vehicle::getIndex()
{
	return _index;
}

void Vehicle::applyBoost(float val)
{
	_vehicleInfo.speed += val * 1000;
}

void Vehicle::setSpeed(const float s)
{
	_vehicleInfo.speed = s;
}

void Vehicle::setStartPosition(float x, float y, float z)
{
	_startPosition.setX(x);
	_startPosition.setY(y);
	_startPosition.setZ(z);
}

void Vehicle::setVehicleInfo(ServerRequests::VehicleInfo info)
{
	_vehicleInfo = info;
}

void Vehicle::enableIceWheels(float val, float time)
{
	_iceWheelsEnabled = true;
	_iceWheelsTime = time;
	_raycastVehicle->getWheelInfo(0).m_frictionSlip = 0;
	_raycastVehicle->getWheelInfo(1).m_frictionSlip = 0;
	_raycastVehicle->getWheelInfo(2).m_frictionSlip = 0;
	_raycastVehicle->getWheelInfo(3).m_frictionSlip = 0;
}

float Vehicle::getIceWheelsTime()
{
	return _iceWheelsTime;
}