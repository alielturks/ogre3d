#include "stdafx.h"
#include "Requests/RequestHandler.h"

using namespace ServerRequests;

RequestHandler::RequestHandler()
{
}


RequestHandler::~RequestHandler()
{
}

void RequestHandler::downloadFile(std::string url, std::string folder, std::string filename)
{
	_fileDownloader.download(url, folder, filename);
	std::cout << "File download complete!" << std::endl;
}

void RequestHandler::retrieveJson(std::string url, RequestTypes type)
{
	//Set the requested url
	_jsonParser.setUrl(url);

	switch (type) {
	case RequestTypes::JSON_PLAYER_LIST:
		_jsonParser.readPlayerList();
		break;
	case RequestTypes::JSON_VERSION:
		_jsonParser.readVersion();
		break;
	case RequestTypes::JSON_VEHICLES:
		_jsonParser.readVehicles();
		break;
	case RequestTypes::JSON_TRACK:
		_jsonParser.readTrack();
		break;

	default:
		_jsonParser.parse();
		break;
	}
}

JSONParser* RequestHandler::getJsonParser()
{
	return &_jsonParser;
}

void RequestHandler::setRequestResultsPointer(RequestResults* r)
{
	_requestResults = r;
	_jsonParser.setRequestResultsPointer(r);
}