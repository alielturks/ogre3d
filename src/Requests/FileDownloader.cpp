#include "stdafx.h"
#include "Requests/FileDownloader.h"

using namespace ServerRequests;

FileDownloader::FileDownloader()
{
}


FileDownloader::~FileDownloader()
{
}

void FileDownloader::download(std::string url, std::string folder, std::string filename)
{
	std::string full_path = folder + "/" + filename;
	struct stat buffer;
	if (stat(full_path.c_str(), &buffer) == 0) {
		std::clog << url << " already downloaded!" << std::endl;
		return;
	}
	
	std::clog << "File downloading requested '" << url << "'" << std::endl;
	std::clog << "File will be saved in " << folder << " and with the name " << filename << std::endl;

	parseFolderString(folder);

	CURL *curl;
	FILE *fp;
	CURLcode res;
	curl = curl_easy_init();
	std::string outputFile = folder + "/" + filename;
	std::clog << "output file " << outputFile << std::endl;
	if (curl) {
		fp = fopen(outputFile.c_str(), "wb");
		if (fp) {
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
			curl_easy_setopt(curl, CURLOPT_NOPROGRESS, false);
			curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, progress_func);
			res = curl_easy_perform(curl);
			/* always cleanup */
			curl_easy_cleanup(curl);
			fclose(fp);
		}
		else {
			std::clog << "Could not create file for saving!" << std::endl;
		}
	}
}

size_t FileDownloader::write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}

int FileDownloader::progress_func(void* ptr, double TotalToDownload, double NowDownloaded, double TotalToUpload, double NowUploaded)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(0) << (NowDownloaded / TotalToDownload * 100);
	std::clog << "Progress " << std::setprecision(2) << stream.str() << "% (" << (float)NowDownloaded / 1024.0f << "/" << (float)TotalToDownload << "kb)" << std::endl;
	return 0;
}

void FileDownloader::parseFolderString(std::string path)
{
	std::string token;
	std::string parsed;
	while (token != path){
		token = path.substr(0, path.find_first_of("/"));
		path = path.substr(path.find_first_of("/") + 1);
		
		//If we already have folders created, add "/"
		if (parsed.length() > 0) {
			parsed += "/";
		}
		parsed += token;

		//Convert char * to LPCWSTR
		//wchar_t wtext[100];
		
		//mbstowcs(wtext, parsed.c_str(), strlen(parsed.c_str()) + 1);//Plus null
		
		std::cout << "creating directory => " << parsed << std::endl;

		//Finally create directory
		CreateDirectory(parsed.c_str(), NULL);
	}
}