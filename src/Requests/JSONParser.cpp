#include "stdafx.h"
#include "Requests/JSONParser.h"

using namespace rapidjson;
using namespace ServerRequests;

JSONParser::JSONParser()
{
}


JSONParser::~JSONParser()
{
}


void JSONParser::parse()
{
	makeRequest();

	//We create JSON object from this
	const char* json = _buffer.c_str();
	Document d;
	d.Parse(json);

	// We can change retrieved values or read them
	Value& s = d["ip"];
	//s.SetString("this is ip!");
	std::clog << "Reading value from json => " << s.GetString() << std::endl;

	// 3. Stringify the DOM
	StringBuffer outputJson;
	Writer<StringBuffer> writer(outputJson);
	d.Accept(writer);

	// Output {"project":"rapidjson","stars":11}
	std::clog << "JSON response looks like this => " << outputJson.GetString() << std::endl;
}

void JSONParser::makeRequest()
{
	std::clog << "Making request to '" << _url << "'" << std::endl;

	//Clear the buffer before we do anything
	_buffer.clear();

	CURL *curl;
	CURLcode res;
	struct curl_slist *headers = NULL; // init to NULL is important 
	headers = curl_slist_append(headers, "Accept: application/json");

	curl = curl_easy_init();
	if (curl) {

		curl_easy_setopt(curl, CURLOPT_URL, _url.c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, loadJson);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &_buffer);
		res = curl_easy_perform(curl);

		if (CURLE_OK == res) {
			char *ct;
			/* ask for the content-type */
			//res = curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &ct);
			//if ((CURLE_OK == res) && ct) {
				//printf("We received Content-Type: %s\n", ct);
			//}
		}
	}
	/* always cleanup */
	curl_easy_cleanup(curl);
}

size_t JSONParser::loadJson(void *ptr, size_t size, size_t nmemb, void *stream)
{
	//We apend neccessary data to the stream object
	((std::string*)stream)->append((char*)ptr, size * nmemb);
	return size * nmemb;
}

Document* JSONParser::getJsonObject()
{
	return &_outgoingJson;
}

void JSONParser::readPlayerList()
{
	makeRequest();
	const char* json = _buffer.c_str();
	Document d;
	d.Parse(json);

	if (d.IsArray()) {
		for (auto it = d.Begin(); it != d.End(); ++it) {
			if ((*it).IsObject()) {
				if((*it).HasMember("username")) {
					std::clog << "Player " << (*it)["username"].GetString() << std::endl;
				}
				if ((*it).HasMember("id")) {
					std::clog << "ID " << (*it)["id"].GetInt() << std::endl;
				}
				std::clog << std::endl;
			}
		}
	}
}

void JSONParser::readVersion()
{
	makeRequest();
	const char* json = _buffer.c_str();
	Document d;
	d.Parse(json);
	if (d.IsObject()) {
		if (d.HasMember("current")) {
			std::clog << "Current version on server " << d["current"].GetString() << std::endl;
		}
		if (d.HasMember("previous")) {
			std::clog << "Previous version on server " << d["previous"].GetString() << std::endl;
		}
		if (d.HasMember("date")) {
			std::clog << "Updated on " << d["date"].GetString() << std::endl;
		}
	}
}

void JSONParser::setUrl(std::string url)
{
	_url = url;
}

void JSONParser::readVehicles()
{
	// Clear previous vehicles if there are any
	_requestResults->clearVehicles();

	checkAndRead("json/cars.json");

	std::string name("");
	float mass = 0;
	float speed = 0;

	VehicleInfo veh;

	const char* json = _buffer.c_str();
	Document d;
	d.Parse(json);
	std::clog << "Name\tMass\tSpeed" << std::endl;
	if (d.IsArray()) {
		for (auto it = d.Begin(); it != d.End(); ++it) {
			if ((*it).IsObject()) {
				if ((*it).HasMember("name")) {
					name = (*it)["name"].GetString();
				}
				if ((*it).HasMember("mass")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.mass = std::stof((*it)["mass"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "Exception: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("speed")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.speed = std::stof((*it)["speed"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "Exception: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("friction")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.friction = std::stof((*it)["friction"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "Exception: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("suspension_force")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.suspensionForce = std::stof((*it)["suspension_force"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "suspension_force: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("suspension_travel")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.suspensionTravel = std::stof((*it)["suspension_travel"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "suspension_travel: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("suspension_compression")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.suspensionCompression = std::stof((*it)["suspension_compression"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "suspension_compression: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("suspension_damping")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.suspensionDamping = std::stof((*it)["suspension_damping"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "suspension_damping: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("suspension_stiffness")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.suspensionStiffness = std::stof((*it)["suspension_stiffness"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "suspension_stiffness: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("max_steer_value")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.maxSteerValue = std::stof((*it)["max_steer_value"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "max_steer_value: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("brake_force")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.brakeForce = std::stof((*it)["brake_force"].GetString());
					}
					catch (std::exception &e) {
						std::clog << "brake_force: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("front_drive")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.front_drive = (bool) std::stoi((*it)["front_drive"].GetString());
						std::clog << "front drive " << veh.front_drive << std::endl;
					}
					catch (std::exception &e) {
						std::clog << "front_drive: " << e.what() << std::endl;
					}
				}
				if ((*it).HasMember("rear_drive")) {
					//stof will throw exepction if wrong value is provided
					try {
						veh.rear_drive = (bool) std::stoi((*it)["rear_drive"].GetString());
						std::clog << "rear drive " << veh.rear_drive << std::endl;
					}
					catch (std::exception &e) {
						std::clog << "rear_drive: " << e.what() << std::endl;
					}
				}

				std::clog << name << "\t" << mass << "\t" << speed << std::endl;

				//Add new vehicle to the request results class
				_requestResults->addVehicle(veh);
			}
		}
	}
}

void JSONParser::readTrack()
{
	checkAndRead("json/map.json");
	
	const char* json = _buffer.c_str();
	Document d;
	d.Parse(json);

	//Clear previously loaded map if there is any
	_requestResults->clearMap();

	if (d.HasMember("width") && d.HasMember("height") && d.HasMember("scale")) {
		_requestResults->setTrackSizeAndScale(d["width"].GetInt(), d["height"].GetInt(), d["scale"].GetInt());
	}

	if (d.HasMember("map") && d["map"].IsArray()) {
		int y = 0;
		int x = 0;
		for (auto row = d["map"].Begin(); row != d["map"].End(); ++row) {
			if ((*row).IsArray()) {
				for (auto col = (*row).Begin(); col != (*row).End(); ++col) {
					_requestResults->addMapPoint(x, y, (*col).GetDouble());
					x++;
				}
			}
			y++;
		}
	}
}

void JSONParser::setRequestResultsPointer(RequestResults* r)
{
	_requestResults = r;
}


void JSONParser::checkAndRead(std::string filepath)
{
	_buffer.clear();
	std::ifstream file(filepath);
	while (std::getline(file, _buffer)) {
	}
	if (_buffer.empty()) {
		std::clog << "There is no " << filepath << " file saved locally! Making request to get one!" << std::endl;
		makeRequest();
	}
	else {
		std::clog << filepath << " file already present on system!" << std::endl;
	}
}