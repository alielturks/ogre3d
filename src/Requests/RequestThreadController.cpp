#include "stdafx.h"
#include "Requests/RequestThreadController.h"

using namespace ServerRequests;

RequestThreadController::RequestThreadController():
_currentTask(TaskList::NONE),
_serverAddress("http://46.101.138.23/api"),
_running(false),
_shutdown(false)
{
	for (unsigned int i = 0; i < TaskList::NONE; i++) {
		_jobsDone[(TaskList)i] = false;
	}
	_requestHandler.setRequestResultsPointer(&_requestResults);
}


RequestThreadController::~RequestThreadController()
{
}

void RequestThreadController::addTask(TaskList t)
{
	_taskListMutex.lock();
	_taskList.push_back(t);
	_jobsDone[t] = false;
	std::clog << "adding task " << t << std::endl;
	std::clog << "total tasks = " << _taskList.size() << std::endl;
	//We have job pending, process it
	_running = true;
	_taskListMutex.unlock();
}

void RequestThreadController::getTask()
{
	_taskListMutex.lock();
	if (_taskList.empty()) {
		_running = false;
		_currentTask = TaskList::NONE;
	}
	else {
		//obtain first item in the list
		_currentTask = _taskList.front();
		//remove first item of the list
		_taskList.pop_front();
	}
	_taskListMutex.unlock();
}

bool RequestThreadController::go()
{
	if (_running) {
		std::clog << "running" << std::endl;
		//Check if there is jobs to process
		getTask();
		if (_currentTask != TaskList::NONE) {
			std::clog << "tasklist != none" << std::endl;
			switch (_currentTask) {
			case TaskList::GET_VEHICLES:
				std::clog << "getVehicles" << std::endl;
				_requestHandler.downloadFile(_serverAddress + "/v", "json", "cars.json");
				_requestHandler.retrieveJson(_serverAddress + "/v", ServerRequests::RequestTypes::JSON_VEHICLES);

				break;
			case TaskList::GET_TRACK:
				std::clog << "getTrack" << std::endl;
				_requestHandler.downloadFile(_serverAddress + "/track", "json", "map.json");
				_requestHandler.retrieveJson(_serverAddress + "/track", ServerRequests::RequestTypes::JSON_TRACK);
				break;
			}

			_taskListMutex.lock();
			_jobsDone[_currentTask] = true;
			_taskListMutex.unlock();
			//All done, current job needs to be set to none
			_currentTask = TaskList::NONE;
		}

		//If any of the jobs is still not done, return, otherwise initiate the thread shutdown
		bool shouldShutdown = true;
		for (auto it = _jobsDone.begin(); it != _jobsDone.end(); ++it) {
			if (!(*it).second) {
				shouldShutdown = false;
			}
		}
		if (shouldShutdown) {
			shutdown();
		}
	}

	if (_shutdown) {
		std::clog << "shutdown == true, go returning false" << std::endl;
		return false;
	}

	return true;
}

void RequestThreadController::shutdown()
{
	std::clog << "Shutdown initialized" << std::endl;
	_shutdown = true;
}

bool RequestThreadController::getJobStatus(TaskList t) {
	_taskListMutex.lock();
	bool result = _jobsDone[t];
	_taskListMutex.unlock();
	return result;
}

TaskList RequestThreadController::getCurrentJob()
{
	return _currentTask;
}

RequestResults* RequestThreadController::getRequestResults()
{
	return &_requestResults;
}

std::string RequestThreadController::getCurrentJobDescription()
{
	switch (_currentTask) {
	case TaskList::GET_TRACK:
		return "Getting track information";
		break;
	case TaskList::GET_VEHICLES:
		return "Getting vehicles";
		break;
	default:
		return "...";
		break;
	}
}