#include "stdafx.h"
#include "Requests\RequestResults.h"

using namespace ServerRequests;

RequestResults::RequestResults()
{
}


RequestResults::~RequestResults()
{
}

void RequestResults::addVehicle(VehicleInfo i)
{
	_vehiclesMutex.lock();
	_vehicles.push_back(i);
	std::clog << "Adding new vehicle, total " << _vehicles.size() << " vehicles" << std::endl;
	_vehiclesMutex.unlock();
}

void RequestResults::clearVehicles()
{
	_vehiclesMutex.lock();
	_vehicles.clear();
	_vehiclesMutex.unlock();
	std::clog << "Clearing old vehicles" << std::endl;
}

std::list<VehicleInfo> RequestResults::getVehicles()
{
	std::list<VehicleInfo> vlist = _vehicles;
	return vlist;
}

void RequestResults::addMapPoint(int x, int y, float val)
{
	//std::clog << "Adding map point " << x << ";" << y << " => " << val << std::endl;
	_trackInfo.mapPoints.push_back(val);
}

void RequestResults::clearMap()
{
	_trackInfo.mapPoints.clear();
	_trackInfo.width = 0;
	_trackInfo.height = 0;
}

TrackInfo* RequestResults::getTrackInfo()
{
	return &_trackInfo;
}

void RequestResults::setTrackSizeAndScale(int width, int height, float scale)
{
	std::clog << "Track width and height set [" << width << ";" << height << "] and scale " << scale << std::endl;
	_trackInfo.width = width;
	_trackInfo.height = height;
	_trackInfo.scale = scale;
}