#include "stdafx.h"
#include "Player.h"


Player::Player()
{
	_playerData.points = 0;
	_playerData.health = 100;
}


Player::~Player()
{
}

void Player::setVehicle(Vehicle* v, Ogre::SceneNode* n)
{
	_vehicleInfo.raycastVehicle = v;
	_vehicleInfo.vehicleNode = n;
	_playerData.id = v->getIndex();
}

void Player::init(OgreBites::SdkTrayManager* mgr)
{
	_gui.speed = mgr->createLabel(OgreBites::TL_NONE, "PlayerSpeed" + std::to_string(_playerData.id), "0", 100);
	_gui.points = mgr->createLabel(OgreBites::TL_NONE, "PlayerPoints" + std::to_string(_playerData.id), "0", 100);
	_gui.bonus = mgr->createLabel(OgreBites::TL_NONE, "PlayerBonus" + std::to_string(_playerData.id), "", 100);
	_gui.speed->_setLeft(0.05);
	_gui.speed->_setTop(0.05);
	_gui.points->_setLeft(0.05);
	_gui.points->_setTop(0.1);
	_gui.bonus->_setLeft(0.05);
	_gui.bonus->_setTop(0.15);
}

void Player::keyPressed(const OIS::KeyEvent &arg)
{
	if (_controls.keyboardEnabled) {
		if (arg.key == _controls.accelerate) {
			_vehicleInfo.raycastVehicle->accelerate(true);
		}
		if (arg.key == _controls.brake) {
			_vehicleInfo.raycastVehicle->brake(true);
		}
		if (arg.key == _controls.left) {
			_vehicleInfo.raycastVehicle->steerLeft(true);
		}
		if (arg.key == _controls.right) {
			_vehicleInfo.raycastVehicle->steerRight(true);
		}
		if (arg.key == _controls.back) {
			_vehicleInfo.raycastVehicle->backward(true);
		}
		if (arg.key == _controls.reset) {
			_vehicleInfo.raycastVehicle->resetCar();
		}
	}
}

void Player::keyReleased(const OIS::KeyEvent &arg)
{
	if (_controls.keyboardEnabled) {
		if (arg.key == _controls.accelerate) {
			_vehicleInfo.raycastVehicle->accelerate(false);
		}
		if (arg.key == _controls.brake) {
			_vehicleInfo.raycastVehicle->brake(false);
		}
		if (arg.key == _controls.left) {
			_vehicleInfo.raycastVehicle->steerLeft(false);
		}
		if (arg.key == _controls.right) {
			_vehicleInfo.raycastVehicle->steerRight(false);
		}
		if (arg.key == _controls.back) {
			_vehicleInfo.raycastVehicle->backward(false);
		}
	}
}

void Player::frameStarted(const Ogre::FrameEvent& evt)
{
	_vehicleInfo.raycastVehicle->update(evt);
	update(evt);
}

void Player::eventTriggered(GameEvent::EventTypes evt)
{
	switch (evt) {
	case GameEvent::EventTypes::RACE_STARTED:
		_vehicleInfo.raycastVehicle->resetCar();
		break;
	}
}

void Player::setCamera(Ogre::Camera* cam)
{
	_camera = cam;
	_camera->setPosition(_vehicleInfo.vehicleNode->getPosition());
}

void Player::update(const Ogre::FrameEvent& evt)
{
	updateCameraPosition(evt);
	updateGui();
}

void Player::updateCameraPosition(const Ogre::FrameEvent& evt)
{
	Ogre::Vector3 camPos = _camera->getPosition();
	const Ogre::Vector3 playerPos = _vehicleInfo.vehicleNode->getPosition();
	const float diffX = camPos.x - playerPos.x;
	const float diffY = camPos.y - playerPos.y;
	const float diffZ = camPos.z - playerPos.z;

	if (diffX * diffX + diffY * diffY + diffZ * diffZ > 900) {
		camPos.x -= (camPos.x - playerPos.x) * evt.timeSinceLastFrame;
		camPos.y -= (camPos.y - playerPos.y - 10) * evt.timeSinceLastFrame;
		camPos.z -= (camPos.z - playerPos.z) * evt.timeSinceLastFrame;

		_camera->setPosition(camPos);
	}
	_camera->lookAt(_vehicleInfo.vehicleNode->getPosition());
}

void Player::updateGui()
{
	{
		std::stringstream stream;
		stream << std::fixed << std::setprecision(0) << _vehicleInfo.raycastVehicle->getSpeed();
		_gui.speed->setCaption("KPH " + stream.str());
	}
	{
		std::stringstream stream;
		stream << std::fixed << std::setprecision(0) << _playerData.points;
		_gui.points->setCaption(stream.str());
	}

	if (_vehicleInfo.raycastVehicle->getIceWheelsTime() > 0) {
		if (!_gui.bonus->isVisible()) {
			_gui.bonus->show();
		}
		std::stringstream stream;
		stream << std::fixed << std::setprecision(0) << (int)_vehicleInfo.raycastVehicle->getIceWheelsTime();
		_gui.bonus->setCaption("Ice wheels " + stream.str());
	}
	else {
		if (_gui.bonus->isVisible()) {
			_gui.bonus->hide();
		}
	}
}

void Player::updateEffects()
{
}

void Player::showGui()
{
	_gui.speed->show();
	_gui.points->show();
}

void Player::hideGui()
{
	_gui.speed->hide();
	_gui.points->hide();
}

const Player::PlayerData Player::getPlayerData()
{
	return _playerData;
}

Ogre::Vector3 Player::getPlayerPos()
{
	return _vehicleInfo.vehicleNode->getPosition();
}

void Player::addPoint()
{
	_playerData.points++;
}

void Player::collectDrop(Drop* drop)
{
	switch (drop->getType()) {
	case Drop::DropType::CAR_UPGRADE:
		_vehicleInfo.raycastVehicle->applyBoost(drop->getValue());
		addPoint();
		break;
	case Drop::DropType::CAR_SPIN:
		_vehicleInfo.raycastVehicle->roll(true);
		addPoint();
		break;
	case Drop::DropType::CAR_ICE:
		_vehicleInfo.raycastVehicle->enableIceWheels(drop->getValue(), drop->getValueTime());
		break;
	}
}

void Player::setControls(unsigned int accelerate, unsigned int backward, unsigned int brake, unsigned int left, unsigned int right, unsigned int reset, bool keyboardEnabled)
{
	_controls.accelerate = accelerate;
	_controls.brake = brake;
	_controls.back = backward;
	_controls.left = left;
	_controls.right = right;
	_controls.keyboardEnabled = keyboardEnabled;
	_controls.reset = reset;
}

void Player::setSceneManager(Ogre::SceneManager* mgr)
{
	_sceneManager = mgr;
}