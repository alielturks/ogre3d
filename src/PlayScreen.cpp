#include "stdafx.h"
#include "PlayScreen.h"
using namespace Screens;

PlayScreen::PlayScreen():
BasicScreen(ScreenTypes::PLAY)
{
	_logger = Ogre::LogManager::getSingletonPtr()->createLog("PlayScreen.log", false, false);
}


PlayScreen::~PlayScreen()
{
}

void PlayScreen::create()
{
	_logger->logMessage("creating screen");
	getTrayManager()->hideCursor();
	getSceneManager()->setSkyDome(true, "My/CloudySky", 1, 4, 45000, false);
	getSceneManager()->setSkyBox(true, "Examples/TrippySkyBox", 50000, true);
	getSceneManager()->setAmbientLight(Ogre::ColourValue(0.3, 0.28, 0.28));

	{
		/*Ogre::Light* light = getSceneManager()->createLight("FirstLight");
		light->setType(Ogre::Light::LT_POINT);
		light->setPosition(-50, 50, -100);
		light->setCastShadows(true);
		//light->setAttenuation(3250, 1.0, 0.0014, 0.000007);
		light->setDiffuseColour(1.0, 1.0, 1.0);
		light->setSpecularColour(0.0, 1.0, 0.0);*/
	}

	{
		Ogre::Light* light = getSceneManager()->createLight("SecondLight");
		light->setType(Ogre::Light::LT_POINT);
		light->setPosition(0, 100, 0);
		light->setCastShadows(true);
		//light->setAttenuation(3250, 1.0, 0.0014, 0.000007);
		light->setDiffuseColour(1.0, 1.0, 1.0);
		light->setSpecularColour(1.0, 1.0, 1.0);
	}

	getSceneManager()->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	//##################################
	for (int i = -3; i < 3; i++) {
		for (int k = -3; k < 3; k++) {
			for (int j = 1; j < 5; j++) {
				std::string name = "Cube" + std::to_string(i) + "_" + std::to_string(k) + "_" + std::to_string(j);
				//getBulletManager()->addCube(name, btVector3(1, 1, 1), btVector3(i * 3, j * 3, k * 3), 0.5, 0.1, 0.1, 0.2);
			}
		}
	}

	for (int i = -3; i < 3; i++) {
		for (int k = -3; k < 3; k++) {
			//for (int j = 1; j < 10; j++) {
			std::string name = "Sphere" + std::to_string(i) + "_" + std::to_string(k);
			//getBulletManager()->addSphere(name, 1, btVector3(3 * i, 100, 3 * k), 0.5, 0.1, 0.1, 0.2);
			//}
		}
	}
}

void PlayScreen::destroy()
{
	_logger->logMessage("destroying screen");
}

void PlayScreen::update(const Ogre::FrameEvent evt)
{
}