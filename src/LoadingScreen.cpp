#include "stdafx.h"
#include "LoadingScreen.h"
using namespace Screens;

LoadingScreen::LoadingScreen():
BasicScreen(ScreenTypes::LOADING)
{
}


LoadingScreen::~LoadingScreen()
{
}

void LoadingScreen::create()
{
	_loadDescription = getTrayManager()->createLabel(OgreBites::TL_CENTER, "loading", "...", 800);
}

void LoadingScreen::destroy()
{
	getTrayManager()->destroyWidget(_loadDescription->getName());
	_loadDescription = 0;
}

void LoadingScreen::update(const Ogre::FrameEvent evt)
{
	_loadDescription->setCaption(_requestThreadController->getCurrentJobDescription());

	//Check if all the required tasks are completed and then go go to main menu screen
	if (_requestThreadController->getJobStatus(ServerRequests::TaskList::GET_TRACK) &&
		_requestThreadController->getJobStatus(ServerRequests::TaskList::GET_VEHICLES)) {
		setScreenEvent(Screens::ScreenEvents::GO_TO_MENU_SCREEN);
	}
}

void LoadingScreen::buttonHit(OgreBites::Button* button)
{
}

void LoadingScreen::setRequestThreadController(ServerRequests::RequestThreadController* r)
{
	_requestThreadController = r;
}
