#pragma once
#include "stdafx.h"

class Drop
{
public:

	enum DropType {
		WEAPON,
		CAR_UPGRADE,
		GRAVITY,
		CASH,
		POINTS,
		CAR_SPIN,
		CAR_ICE
	};

	/**
	 * Constructor
	 * @i - unique object id
	 * @t - object type
	 */
	Drop(int i, DropType t);
	~Drop();
	
	/**
	 * Initialize object - create all the scene nodes, particle systems, entitities, etc
	 */
	void init(Ogre::SceneManager* mgr, Ogre::SceneNode* node);

	void update(const Ogre::FrameEvent& evt);

	/**
	 * Set the position
	 */
	void setPosition(Ogre::Vector3 pos);

	DropType getType();

	/**
	 * Check if the provided point is close enough to collect this drop
	 */
	bool checkCollision(Ogre::Vector3 pos);

	/**
	 * Set the drop value
	 */
	void setValue(float v);

	/**
	 * Get the drop value
	 */
	float getValue();

	/**
	 * Returns this objects ID
	 */
	int getID();
	
	/**
	 * Return time how long this value is active
	 */
	float getValueTime();

	/**
	 * Set how long this drop value is active
	 */
	void setValueTime(float t);

	void show();
	void hide();
private:
	/**
	 * Ogre scene node
	 */
	Ogre::SceneNode* _sceneNode;

	/**
	 * Ogre entity
	 */
	Ogre::Entity* _entity;

	/**
	 * Ogre particle system
	 */
	Ogre::ParticleSystem* _particleSystem;

	/**
	 * Scene manager
	 */
	Ogre::SceneManager* _sceneManager;

	/**
	 * Defined drop type
	 * Is it a weapon or something else
	 */
	DropType _type;

	/**
	 * How long (in seconds) this object should be on the map
	 */
	float _lifetime;
	
	/**
	 * Do we need to show this object
	 */
	bool _isActive;

	/**
	 * ID of this object
	 */
	int _id;

	/**
	 * Value of the drop
	 */
	float _value;

	/**
	* How long this drop value is active
	*/
	float _valueTime;

	/**
	 * Destroys this drop
	 * Deletes all the scene nodes, entities and particle systems
	 * that are associated with this object
	 */
	void destroy();

	/**
	 * Set the drop cube texture based on it's type
	 */
	void setTexture();
};

