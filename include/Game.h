/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"
#include "Player.h"
#include "LensFlare.h"

#include <stdio.h>
#include <stdlib.h>
#include "conio.h"
#include <atomic>
#include <thread>
#include <mutex>
#include <boost/scoped_ptr.hpp>
#include <ostream>

#include "BulletManager.h"
#include "Screens.h"
#include "BasicScreen.h"
#include "PlayScreen.h"
#include "MenuScreen.h"
#include "LoadingScreen.h"
#include "GameEventListener.h"
#include "Player.h"
#include "Drop.h"
#include "Requests\RequestThreadController.h"
#include "Globals.h"
#include "MeshUtilities.h"

//---------------------------------------------------------------------------

class Game : public BaseApplication
{
private:
	BulletManager bulletManager;
	LensFlare* lensFlare;

	/**
	 * Game event dispatcher
	 */
	GameEvent::GameEventDispatcher _eventDispatcher;

	/**
	 * BasicScreen instance
	 */
	Screens::BasicScreen* _screen;

	/**
	 * Switch to another screen
	 */
	void changeScreen(Screens::ScreenTypes type);

	std::vector<Player*> _players;

	std::list<Drop> _drops;

	ServerRequests::RequestThreadController* _requestThreadController;

	OgreBites::Label* _gravityLabel;

public:
	Game(void);
	virtual ~Game(void);

	void createMesh(std::string name, std::vector<btVector3>* v);

	void setRequestHandler(ServerRequests::RequestThreadController* a);

	/**
	* Called when any of the events are triggered
	*/
	virtual void eventTriggered(GameEvent::EventTypes evt);

	void createDrop();

protected:
    virtual void createScene(void);
	virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

	virtual bool frameStarted(const FrameEvent& evt);
	virtual bool frameEnded(const FrameEvent& evt);

	virtual void buttonHit(OgreBites::Button *b);
	virtual bool mouseMoved(const OIS::MouseEvent &arg);
	virtual void createFrameListener(void);
	
	virtual bool keyPressed(const OIS::KeyEvent &arg);
	virtual bool keyReleased(const OIS::KeyEvent &arg);

	virtual bool axisMoved(const OIS::JoyStickEvent &e, int axis);
	virtual bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
	virtual bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
	virtual bool povMoved(const OIS::JoyStickEvent &arg, int pov);

	virtual void preViewportUpdate(const Ogre::RenderTargetViewportEvent& evt);

	void createCar();
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
