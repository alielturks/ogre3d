#include "stdafx.h"

#pragma once
#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Basic button class.
	=============================================================================*/
	class Button : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		Button(const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width)
		{
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate("SdkTrays/Button", "BorderPanel", name);
			mBP = (Ogre::BorderPanelOverlayElement*)mElement;
			mTextArea = (Ogre::TextAreaOverlayElement*)mBP->getChild(mBP->getName() + "/ButtonCaption");
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
			mTextArea->setCharHeight(mTextArea->getCharHeight() - 3);
#endif
			mTextArea->setTop(-(mTextArea->getCharHeight() / 2));
			mElement->setHeight(50);

			if (width > 0)
			{
				mElement->setWidth(width);
				mFitToContents = false;
			}
			else mFitToContents = true;

			setCaption(caption);
			mState = BS_UP;
		}

		virtual ~Button() {}

		void setCenter(int count, int order)
		{
			int totalHeight = count * mElement->getHeight();

			mElement->_setLeft(0.5);
			mElement->_setTop(0.5);
			mElement->setLeft(mElement->getLeft() - mElement->getWidth() / 2.0f);
			if (order == 0) {
				mElement->setTop(mElement->getTop() - mElement->getHeight() / 2.0f);
			} else if (order == 1) {
				mElement->setTop(mElement->getTop() + mElement->getHeight() / 2.0f);
			}
		}

		const Ogre::DisplayString& getCaption()
		{
			return mTextArea->getCaption();
		}

		void setCaption(const Ogre::DisplayString& caption)
		{
			mTextArea->setCaption(caption);
			if (mFitToContents) mElement->setWidth(getCaptionWidth(caption, mTextArea) + mElement->getHeight() - 12);
		}

		const ButtonState& getState() { return mState; }

		void _cursorPressed(const Ogre::Vector2& cursorPos)
		{
			if (isCursorOver(mElement, cursorPos, 4)) setState(BS_DOWN);
		}

		void _cursorReleased(const Ogre::Vector2& cursorPos)
		{
			if (mState == BS_DOWN)
			{
				setState(BS_OVER);
				if (mListener) mListener->buttonHit(this);
			}
		}

		void _cursorMoved(const Ogre::Vector2& cursorPos)
		{
			if (isCursorOver(mElement, cursorPos, 4))
			{
				if (mState == BS_UP) setState(BS_OVER);
			}
			else
			{
				if (mState != BS_UP) setState(BS_UP);
			}
		}

		void _focusLost()
		{
			setState(BS_UP);   // reset button if cursor was lost
		}

		void setButtonType(ButtonTypes type) {
			_buttonType = type;
		}

		int getButtonType()
		{
			return _buttonType;
		}

	protected:

		ButtonTypes _buttonType;

		void setState(const ButtonState& bs)
		{
			if (bs == BS_OVER)
			{
				mBP->setBorderMaterialName("SdkTrays/Button/Over");
				mBP->setMaterialName("SdkTrays/Button/Over");
			}
			else if (bs == BS_UP)
			{
				mBP->setBorderMaterialName("SdkTrays/Button/Up");
				mBP->setMaterialName("SdkTrays/Button/Up");
			}
			else
			{
				mBP->setBorderMaterialName("SdkTrays/Button/Down");
				mBP->setMaterialName("SdkTrays/Button/Down");
			}

			mState = bs;
		}

		ButtonState mState;
		Ogre::BorderPanelOverlayElement* mBP;
		Ogre::TextAreaOverlayElement* mTextArea;
		bool mFitToContents;
	};

}
