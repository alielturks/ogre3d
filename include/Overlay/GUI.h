/*
 -----------------------------------------------------------------------------
 This source file is part of OGRE
 (Object-oriented Graphics Rendering Engine)
 For the latest info, see http://www.ogre3d.org/
 
 Copyright (c) 2000-2013 Torus Knot Software Ltd
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 -----------------------------------------------------------------------------
 */
#include "stdafx.h"

#pragma once

//All the GUI elements
#include "GUIDefinitions.h"
#include "TrayListener.h"
#include "Widget.h"
#include "Label.h"
#include "Button.h"
#include "CheckBox.h"
#include "TextBox.h"
#include "SelectMenu.h"
#include "Separator.h"
#include "Slider.h"
#include "DecorWidget.h"
#include "ParamsPanel.h"
#include "ProgressBar.h"
#include <iostream>

#if OGRE_COMPILER == OGRE_COMPILER_MSVC
// TODO - remove this
#   pragma warning (disable : 4244)
#endif

namespace OgreBites
{

	typedef std::vector<Widget*> WidgetList;
	typedef Ogre::VectorIterator<WidgetList> WidgetIterator;

	/*=============================================================================
	| Main class to manage a cursor, backdrop, trays and widgets.
	=============================================================================*/
	class SdkTrayManager : public SdkTrayListener, public Ogre::ResourceGroupListener
    {
    public:

		/*-----------------------------------------------------------------------------
		| Creates backdrop, cursor, and trays.
		-----------------------------------------------------------------------------*/
		SdkTrayManager(const Ogre::String& name, Ogre::RenderWindow* window, InputContext inputContext, SdkTrayListener* listener = 0) :
		  mName(name), mWindow(window), mInputContext(inputContext), mWidgetDeathRow(), mListener(listener), mWidgetPadding(8),
                mWidgetSpacing(2), mTrayPadding(0), mTrayDrag(false), mExpandedMenu(0), mDialog(0), mOk(0), mYes(0),
                mNo(0), mCursorWasVisible(false), mFpsLabel(0), mStatsPanel(0), mLogo(0), mLoadBar(0),
				mGroupInitProportion(0.0f), mGroupLoadProportion(0.0f), mLoadInc(0.0f)
		{
            mTimer = Ogre::Root::getSingleton().getTimer();
            mLastStatUpdateTime = 0;

			Ogre::OverlayManager& om = Ogre::OverlayManager::getSingleton();

			Ogre::String nameBase = mName + "/";
			std::replace(nameBase.begin(), nameBase.end(), ' ', '_');

			// create overlay layers for everything
			mBackdropLayer = om.create(nameBase + "BackdropLayer");
			mTraysLayer = om.create(nameBase + "WidgetsLayer");
			mPriorityLayer = om.create(nameBase + "PriorityLayer");
			mCursorLayer = om.create(nameBase + "CursorLayer");
			mBackdropLayer->setZOrder(100);
			mTraysLayer->setZOrder(200);
			mPriorityLayer->setZOrder(500);
			mCursorLayer->setZOrder(600);

			Ogre::Overlay::Overlay2DElementsIterator itor = mTraysLayer->get2DElementsIterator();
			//while (itor.hasMoreElements()) itor.getNext()->_notifyZOrder(200 * 100);

			// make backdrop and cursor overlay containers
			mCursor = (Ogre::OverlayContainer*)om.createOverlayElementFromTemplate("SdkTrays/Cursor", "Panel", nameBase + "Cursor");
			mCursorLayer->add2D(mCursor);
			mBackdrop = (Ogre::OverlayContainer*)om.createOverlayElement("Panel", nameBase + "Backdrop");
			mBackdropLayer->add2D(mBackdrop);
			mDialogShade = (Ogre::OverlayContainer*)om.createOverlayElement("Panel", nameBase + "DialogShade");
			mDialogShade->setMaterialName("SdkTrays/Shade");
			mDialogShade->hide();
			mPriorityLayer->add2D(mDialogShade);

			Ogre::String trayNames[] =
			{ "TopLeft", "Top", "TopRight", "Left", "Center", "Right", "BottomLeft", "Bottom", "BottomRight" };

			for (unsigned int i = 0; i < 9; i++)    // make the real trays
			{
				mTrays[i] = (Ogre::OverlayContainer*)om.createOverlayElementFromTemplate
					("SdkTrays/Tray", "BorderPanel", nameBase + trayNames[i] + "Tray");
				mTraysLayer->add2D(mTrays[i]);

				mTrayWidgetAlign[i] = Ogre::GHA_CENTER;

				// align trays based on location
				if (i == TL_TOP || i == TL_CENTER || i == TL_BOTTOM) mTrays[i]->setHorizontalAlignment(Ogre::GHA_CENTER);
				if (i == TL_LEFT || i == TL_CENTER || i == TL_RIGHT) mTrays[i]->setVerticalAlignment(Ogre::GVA_CENTER);
				if (i == TL_TOPRIGHT || i == TL_RIGHT || i == TL_BOTTOMRIGHT) mTrays[i]->setHorizontalAlignment(Ogre::GHA_RIGHT);
				if (i == TL_BOTTOMLEFT || i == TL_BOTTOM || i == TL_BOTTOMRIGHT) mTrays[i]->setVerticalAlignment(Ogre::GVA_BOTTOM);
			}

			// create the null tray for free-floating widgets
			mTrays[9] = (Ogre::OverlayContainer*)om.createOverlayElement("Panel", nameBase + "NullTray");
			mTrayWidgetAlign[9] = Ogre::GHA_LEFT;
			mTraysLayer->add2D(mTrays[9]);
			
			showTrays();
			showCursor();
		}

		/*-----------------------------------------------------------------------------
		| Destroys background, cursor, widgets, and trays.
		-----------------------------------------------------------------------------*/
		virtual ~SdkTrayManager()
		{
			Ogre::OverlayManager& om = Ogre::OverlayManager::getSingleton();

			destroyAllWidgets();

			for (unsigned int i = 0; i < mWidgetDeathRow.size(); i++)   // delete widgets queued for destruction
			{
				delete mWidgetDeathRow[i];
			}
			mWidgetDeathRow.clear();

			om.destroy(mBackdropLayer);
			om.destroy(mTraysLayer);
			om.destroy(mPriorityLayer);
			om.destroy(mCursorLayer);

			closeDialog();
			hideLoadingBar();

			Widget::nukeOverlayElement(mBackdrop);
			Widget::nukeOverlayElement(mCursor);
			Widget::nukeOverlayElement(mDialogShade);

			for (unsigned int i = 0; i < 10; i++)
			{
				Widget::nukeOverlayElement(mTrays[i]);
			}
		}

		/*-----------------------------------------------------------------------------
		| Converts a 2D screen coordinate (in pixels) to a 3D ray into the scene.
		-----------------------------------------------------------------------------*/
		static Ogre::Ray screenToScene(Ogre::Camera* cam, const Ogre::Vector2& pt)
		{
			return cam->getCameraToViewportRay(pt.x, pt.y);
		}

		/*-----------------------------------------------------------------------------
		| Converts a 3D scene position to a 2D screen position (in relative screen size, 0.0-1.0).
		-----------------------------------------------------------------------------*/
		static Ogre::Vector2 sceneToScreen(Ogre::Camera* cam, const Ogre::Vector3& pt)
		{
			Ogre::Vector3 result = cam->getProjectionMatrix() * cam->getViewMatrix() * pt;
			return Ogre::Vector2((result.x + 1) / 2, (-result.y + 1) / 2);
		}

		// these methods get the underlying overlays and overlay elements

		Ogre::OverlayContainer* getTrayContainer(TrayLocation trayLoc) { return mTrays[trayLoc]; }
		Ogre::Overlay* getBackdropLayer() { return mBackdropLayer; }
		Ogre::Overlay* getTraysLayer() { return mTraysLayer; }
		Ogre::Overlay* getCursorLayer() { return mCursorLayer; }
		Ogre::OverlayContainer* getBackdropContainer() { return mBackdrop; }
		Ogre::OverlayContainer* getCursorContainer() { return mCursor; }
		Ogre::OverlayElement* getCursorImage() { return mCursor->getChild(mCursor->getName() + "/CursorImage"); }

		void setListener(SdkTrayListener* listener)
		{
			Ogre::LogManager::getSingletonPtr()->logMessage("Listener set up!!!!");
			mListener = listener;
		}

		SdkTrayListener* getListener()
		{
			return mListener;
		}

		void showAll()
		{
			showBackdrop();
			showTrays();
			showCursor();
		}

		void hideAll()
		{
			hideBackdrop();
			hideTrays();
			hideCursor();
		}

		/*-----------------------------------------------------------------------------
		| Displays specified material on backdrop, or the last material used if
		| none specified. Good for pause menus like in the browser.
		-----------------------------------------------------------------------------*/
		void showBackdrop(const Ogre::String& materialName = Ogre::StringUtil::BLANK)
		{
			if (materialName != Ogre::StringUtil::BLANK) mBackdrop->setMaterialName(materialName);
			mBackdropLayer->show();
		}

		void hideBackdrop()
		{
			mBackdropLayer->hide();
		}

		/*-----------------------------------------------------------------------------
		| Displays specified material on cursor, or the last material used if
		| none specified. Used to change cursor type.
		-----------------------------------------------------------------------------*/
		void showCursor(const Ogre::String& materialName = Ogre::StringUtil::BLANK)
		{
			if (materialName != Ogre::StringUtil::BLANK) getCursorImage()->setMaterialName(materialName);

			if (!mCursorLayer->isVisible())
			{
				mCursorLayer->show();
				refreshCursor();
			}
		}

		void hideCursor()
		{
			mCursorLayer->hide();

			// give widgets a chance to reset in case they're in the middle of something
			for (unsigned int i = 0; i < 10; i++)
			{
				for (unsigned int j = 0; j < mWidgets[i].size(); j++)
				{
					mWidgets[i][j]->_focusLost();
				}
			}

			setExpandedMenu(0);
		}

		/*-----------------------------------------------------------------------------
		| Updates cursor position based on unbuffered mouse state. This is necessary
		| because if the tray manager has been cut off from mouse events for a time,
		| the cursor position will be out of date.
		-----------------------------------------------------------------------------*/
		void refreshCursor()
		{
#if (OGRE_NO_VIEWPORT_ORIENTATIONMODE == 0) || (OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS)
            // TODO:
            // the position should be based on the orientation, for now simply return
            return;
#endif
            Ogre::Real x, y;
            if(mInputContext.getCursorPosition(x, y))
                mCursor->setPosition(x, y);
		}

		void showTrays()
		{
			mTraysLayer->show();
			mPriorityLayer->show();
		}

		void hideTrays()
		{
			mTraysLayer->hide();
			mPriorityLayer->hide();

			// give widgets a chance to reset in case they're in the middle of something
			for (unsigned int i = 0; i < 10; i++)
			{
				for (unsigned int j = 0; j < mWidgets[i].size(); j++)
				{
					mWidgets[i][j]->_focusLost();
				}
			}

			setExpandedMenu(0);
		}

		bool isCursorVisible() { return mCursorLayer->isVisible(); }
		bool isBackdropVisible() { return mBackdropLayer->isVisible(); }
		bool areTraysVisible() { return mTraysLayer->isVisible(); }

		/*-----------------------------------------------------------------------------
		| Sets horizontal alignment of a tray's contents.
		-----------------------------------------------------------------------------*/
		void setTrayWidgetAlignment(TrayLocation trayLoc, Ogre::GuiHorizontalAlignment gha)
		{
			mTrayWidgetAlign[trayLoc] = gha;

			for (unsigned int i = 0; i < mWidgets[trayLoc].size(); i++)
			{
				mWidgets[trayLoc][i]->getOverlayElement()->setHorizontalAlignment(gha);
			}
		}

		// padding and spacing methods

		void setWidgetPadding(Ogre::Real padding)
		{
			mWidgetPadding = std::max<int>((int)padding, 0);
			adjustTrays();
		}

		void setWidgetSpacing(Ogre::Real spacing)
		{
			mWidgetSpacing = std::max<int>((int)spacing, 0);
			adjustTrays();
		}
		void setTrayPadding(Ogre::Real padding)
		{
			mTrayPadding = std::max<int>((int)padding, 0);
			adjustTrays();
		}

		virtual Ogre::Real getWidgetPadding() const { return mWidgetPadding; }
		virtual Ogre::Real getWidgetSpacing() const { return mWidgetSpacing; }
		virtual Ogre::Real getTrayPadding() const { return mTrayPadding; }

		/*-----------------------------------------------------------------------------
		| Fits trays to their contents and snaps them to their anchor locations.
		-----------------------------------------------------------------------------*/
		virtual void adjustTrays()
		{
			for (unsigned int i = 0; i < 9; i++)   // resizes and hides trays if necessary
			{
				Ogre::Real trayWidth = 0;
				Ogre::Real trayHeight = mWidgetPadding;
				std::vector<Ogre::OverlayElement*> labelsAndSeps;

				if (mWidgets[i].empty())   // hide tray if empty
				{
					mTrays[i]->hide();
					continue;
				}
				else mTrays[i]->show();

				// arrange widgets and calculate final tray size and position
				for (unsigned int j = 0; j < mWidgets[i].size(); j++)
				{
					Ogre::OverlayElement* e = mWidgets[i][j]->getOverlayElement();

					if (j != 0) trayHeight += mWidgetSpacing;   // don't space first widget

					e->setVerticalAlignment(Ogre::GVA_TOP);
					e->setTop(trayHeight);

					switch (e->getHorizontalAlignment())
					{
					case Ogre::GHA_LEFT:
						e->setLeft(mWidgetPadding);
						break;
					case Ogre::GHA_RIGHT:
						e->setLeft(-(e->getWidth() + mWidgetPadding));
						break;
					default:
						e->setLeft(-(e->getWidth() / 2));
					}

					// prevents some weird texture filtering problems (just some)
					e->setPosition((int)e->getLeft(), (int)e->getTop());
					e->setDimensions((int)e->getWidth(), (int)e->getHeight());

					trayHeight += e->getHeight();

					Label* l = dynamic_cast<Label*>(mWidgets[i][j]);
					if (l && l->_isFitToTray())
					{
						labelsAndSeps.push_back(e);
						continue;
					}
					Separator* s = dynamic_cast<Separator*>(mWidgets[i][j]);
					if (s && s->_isFitToTray()) 
					{
						labelsAndSeps.push_back(e);
						continue;
					}

					if (e->getWidth() > trayWidth) trayWidth = e->getWidth();
				}

				// add paddings and resize trays
				mTrays[i]->setWidth(trayWidth + 2 * mWidgetPadding);
				mTrays[i]->setHeight(trayHeight + mWidgetPadding);

				for (unsigned int j = 0; j < labelsAndSeps.size(); j++)
				{
					labelsAndSeps[j]->setWidth((int)trayWidth);
					labelsAndSeps[j]->setLeft(-(int)(trayWidth / 2));
				}
			}

			for (unsigned int i = 0; i < 9; i++)    // snap trays to anchors
			{
				if (i == TL_TOPLEFT || i == TL_LEFT || i == TL_BOTTOMLEFT)
					mTrays[i]->setLeft(mTrayPadding);
				if (i == TL_TOP || i == TL_CENTER || i == TL_BOTTOM)
					mTrays[i]->setLeft(-mTrays[i]->getWidth() / 2);
				if (i == TL_TOPRIGHT || i == TL_RIGHT || i == TL_BOTTOMRIGHT)
					mTrays[i]->setLeft(-(mTrays[i]->getWidth() + mTrayPadding));

				if (i == TL_TOPLEFT || i == TL_TOP || i == TL_TOPRIGHT)
					mTrays[i]->setTop(mTrayPadding);
				if (i == TL_LEFT || i == TL_CENTER || i == TL_RIGHT)
					mTrays[i]->setTop(-mTrays[i]->getHeight() / 2);
				if (i == TL_BOTTOMLEFT || i == TL_BOTTOM || i == TL_BOTTOMRIGHT)
					mTrays[i]->setTop(-mTrays[i]->getHeight() - mTrayPadding);

				// prevents some weird texture filtering problems (just some)
				mTrays[i]->setPosition((int)mTrays[i]->getLeft(), (int)mTrays[i]->getTop());
				mTrays[i]->setDimensions((int)mTrays[i]->getWidth(), (int)mTrays[i]->getHeight());
			}
		}

		/*-----------------------------------------------------------------------------
		| Returns a 3D ray into the scene that is directly underneath the cursor.
		-----------------------------------------------------------------------------*/
		Ogre::Ray getCursorRay(Ogre::Camera* cam)
		{
			return screenToScene(cam, Ogre::Vector2(mCursor->_getLeft(), mCursor->_getTop()));
		}

		Button* createButton(TrayLocation trayLoc, const Ogre::String& name, const Ogre::String& caption, Ogre::Real width = 0)
		{
			Button* b = new Button(name, caption, width);
			moveWidgetToTray(b, trayLoc);
			b->_assignListener(mListener);
			return b;
		}

		TextBox* createTextBox(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption,
			Ogre::Real width, Ogre::Real height)
		{
			TextBox* tb = new TextBox(name, caption, width, height);
			moveWidgetToTray(tb, trayLoc);
			tb->_assignListener(mListener);
			return tb;
		}

		SelectMenu* createThickSelectMenu(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption,
			Ogre::Real width, unsigned int maxItemsShown, const Ogre::StringVector& items = Ogre::StringVector())
		{
			SelectMenu* sm = new SelectMenu(name, caption, width, 0, maxItemsShown);
			moveWidgetToTray(sm, trayLoc);
			sm->_assignListener(mListener);
			if (!items.empty()) sm->setItems(items);
			return sm;
		}

		SelectMenu* createLongSelectMenu(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption,
			Ogre::Real width, Ogre::Real boxWidth, unsigned int maxItemsShown, const Ogre::StringVector& items = Ogre::StringVector())
		{
			SelectMenu* sm = new SelectMenu(name, caption, width, boxWidth, maxItemsShown);
			moveWidgetToTray(sm, trayLoc);
			sm->_assignListener(mListener);
			if (!items.empty()) sm->setItems(items);
			return sm;
		}

		SelectMenu* createLongSelectMenu(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption,
			Ogre::Real boxWidth, unsigned int maxItemsShown, const Ogre::StringVector& items = Ogre::StringVector())
		{
			return createLongSelectMenu(trayLoc, name, caption, 0, boxWidth, maxItemsShown, items);
		}

		Label* createLabel(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width = 0)
		{
			Label* l = new Label(name, caption, width);
			moveWidgetToTray(l, trayLoc);
			l->_assignListener(mListener);
			return l;
		}

		Separator* createSeparator(TrayLocation trayLoc, const Ogre::String& name, Ogre::Real width = 0)
		{
			Separator* s = new Separator(name, width);
			moveWidgetToTray(s, trayLoc);
			return s;
		}

		Slider* createThickSlider(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption,
			Ogre::Real width, Ogre::Real valueBoxWidth, Ogre::Real minValue, Ogre::Real maxValue, unsigned int snaps)
		{
			Slider* s = new Slider(name, caption, width, 0, valueBoxWidth, minValue, maxValue, snaps);
			moveWidgetToTray(s, trayLoc);
			s->_assignListener(mListener);
			return s;
		}

		Slider* createLongSlider(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width,
			Ogre::Real trackWidth, Ogre::Real valueBoxWidth, Ogre::Real minValue, Ogre::Real maxValue, unsigned int snaps)
		{
			if (trackWidth <= 0) trackWidth = 1;
			Slider* s = new Slider(name, caption, width, trackWidth, valueBoxWidth, minValue, maxValue, snaps);
			moveWidgetToTray(s, trayLoc);
			s->_assignListener(mListener);
			return s;
		}

		Slider* createLongSlider(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption,
			Ogre::Real trackWidth, Ogre::Real valueBoxWidth, Ogre::Real minValue, Ogre::Real maxValue, unsigned int snaps)
		{
			return createLongSlider(trayLoc, name, caption, 0, trackWidth, valueBoxWidth, minValue, maxValue, snaps);
		}

		ParamsPanel* createParamsPanel(TrayLocation trayLoc, const Ogre::String& name, Ogre::Real width, unsigned int lines)
		{
			ParamsPanel* pp = new ParamsPanel(name, width, lines);
			moveWidgetToTray(pp, trayLoc);
			return pp;
		}

		ParamsPanel* createParamsPanel(TrayLocation trayLoc, const Ogre::String& name, Ogre::Real width,
			const Ogre::StringVector& paramNames)
		{
			ParamsPanel* pp = new ParamsPanel(name, width, (Ogre::uint)paramNames.size());
			pp->setAllParamNames(paramNames);
			moveWidgetToTray(pp, trayLoc);
			return pp;
		}

		CheckBox* createCheckBox(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption,
			Ogre::Real width = 0)
		{
			CheckBox* cb = new CheckBox(name, caption, width);
			moveWidgetToTray(cb, trayLoc);
			cb->_assignListener(mListener);
			return cb;
		}

		DecorWidget* createDecorWidget(TrayLocation trayLoc, const Ogre::String& name, const Ogre::String& templateName)
		{
			DecorWidget* dw = new DecorWidget(name, templateName);
			moveWidgetToTray(dw, trayLoc);
			return dw;
		}

		ProgressBar* createProgressBar(TrayLocation trayLoc, const Ogre::String& name, const Ogre::DisplayString& caption,
			Ogre::Real width, Ogre::Real commentBoxWidth)
		{
			ProgressBar* pb = new ProgressBar(name, caption, width, commentBoxWidth);
			moveWidgetToTray(pb, trayLoc);
			return pb;
		}

		/*-----------------------------------------------------------------------------
		| Shows frame statistics widget set in the specified location.
		-----------------------------------------------------------------------------*/
		void showFrameStats(TrayLocation trayLoc, int place = -1)
		{
			if (!areFrameStatsVisible())
			{
				Ogre::StringVector stats;
				stats.push_back("Average FPS");
				stats.push_back("Best FPS");
				stats.push_back("Worst FPS");
				stats.push_back("Triangles");
				stats.push_back("Batches");

				mFpsLabel = createLabel(TL_NONE, mName + "/FpsLabel", "FPS:", 180);
				mFpsLabel->_assignListener(this);
				mStatsPanel = createParamsPanel(TL_NONE, mName + "/StatsPanel", 180, stats);
			}

			moveWidgetToTray(mFpsLabel, trayLoc, place);
			moveWidgetToTray(mStatsPanel, trayLoc, locateWidgetInTray(mFpsLabel) + 1);
		}

		/*-----------------------------------------------------------------------------
		| Hides frame statistics widget set.
		-----------------------------------------------------------------------------*/
		void hideFrameStats()
		{
			if (areFrameStatsVisible())
			{
				destroyWidget(mFpsLabel);
				destroyWidget(mStatsPanel);
				mFpsLabel = 0;
				mStatsPanel = 0;
			}
		}

		bool areFrameStatsVisible()
		{
			return mFpsLabel != 0;
		}

		/*-----------------------------------------------------------------------------
		| Toggles visibility of advanced statistics.
		-----------------------------------------------------------------------------*/
		void toggleAdvancedFrameStats()
		{
			if (mFpsLabel) labelHit(mFpsLabel);
		}

		/*-----------------------------------------------------------------------------
		| Shows logo in the specified location.
		-----------------------------------------------------------------------------*/
		void showLogo(TrayLocation trayLoc, int place = -1)
		{
			if (!isLogoVisible()) mLogo = createDecorWidget(TL_NONE, mName + "/Logo", "SdkTrays/Logo");
			moveWidgetToTray(mLogo, trayLoc, place);
		}

		void hideLogo()
		{
			if (isLogoVisible())
			{
				destroyWidget(mLogo);
				mLogo = 0;
			}
		}

		bool isLogoVisible()
		{
			return mLogo != 0;
		}

		/*-----------------------------------------------------------------------------
		| Shows loading bar. Also takes job settings: the number of resource groups
		| to be initialised, the number of resource groups to be loaded, and the
		| proportion of the job that will be taken up by initialisation. Usually,
		| script parsing takes up most time, so the default value is 0.7.
		-----------------------------------------------------------------------------*/
		void showLoadingBar(unsigned int numGroupsInit = 1, unsigned int numGroupsLoad = 1,
			Ogre::Real initProportion = 0.7)
		{
			if (mDialog) closeDialog();
			if (mLoadBar) hideLoadingBar();

			mLoadBar = new ProgressBar(mName + "/LoadingBar", "Loading...", 400, 308);
			Ogre::OverlayElement* e = mLoadBar->getOverlayElement();
			mDialogShade->addChild(e);
			e->setVerticalAlignment(Ogre::GVA_CENTER);
			e->setLeft(-(e->getWidth() / 2));
			e->setTop(-(e->getHeight() / 2));

			Ogre::ResourceGroupManager::getSingleton().addResourceGroupListener(this);
			mCursorWasVisible = isCursorVisible();
			hideCursor();
			mDialogShade->show();

			// calculate the proportion of job required to init/load one group

			if (numGroupsInit == 0 && numGroupsLoad != 0)
			{
				mGroupInitProportion = 0;
				mGroupLoadProportion = 1;
			}
			else if (numGroupsLoad == 0 && numGroupsInit != 0)
			{
				mGroupLoadProportion = 0;
				if (numGroupsInit != 0) mGroupInitProportion = 1;
			}
			else if (numGroupsInit == 0 && numGroupsLoad == 0)
			{
				mGroupInitProportion = 0;
				mGroupLoadProportion = 0;
			}
			else
			{
				mGroupInitProportion = initProportion / numGroupsInit;
				mGroupLoadProportion = (1 - initProportion) / numGroupsLoad;
			}
		}

		void hideLoadingBar()
		{
			if (mLoadBar)
			{
				mLoadBar->cleanup();
				delete mLoadBar;
				mLoadBar = 0;

				Ogre::ResourceGroupManager::getSingleton().removeResourceGroupListener(this);
				if (mCursorWasVisible) showCursor();
				mDialogShade->hide();
			}
		}

		bool isLoadingBarVisible()
		{
			return mLoadBar != 0;
		}

		/*-----------------------------------------------------------------------------
		| Pops up a message dialog with an OK button.
		-----------------------------------------------------------------------------*/
		void showOkDialog(const Ogre::DisplayString& caption, const Ogre::DisplayString& message)
		{
			if (mLoadBar) hideLoadingBar();

			Ogre::OverlayElement* e;

			if (mDialog)
			{
				mDialog->setCaption(caption);
				mDialog->setText(message);

				if (mOk) return;
				else
				{
					mYes->cleanup();
					mNo->cleanup();
					delete mYes;
					delete mNo;
					mYes = 0;
					mNo = 0;
				}
			}
			else
			{
				// give widgets a chance to reset in case they're in the middle of something
				for (unsigned int i = 0; i < 10; i++)
				{
					for (unsigned int j = 0; j < mWidgets[i].size(); j++)
					{
						mWidgets[i][j]->_focusLost();
					}
				}

				mDialogShade->show();

				mDialog = new TextBox(mName + "/DialogBox", caption, 300, 208);
				mDialog->setText(message);
				e = mDialog->getOverlayElement();
				mDialogShade->addChild(e);
				e->setVerticalAlignment(Ogre::GVA_CENTER);
				e->setLeft(-(e->getWidth() / 2));
				e->setTop(-(e->getHeight() / 2));

				mCursorWasVisible = isCursorVisible();
				showCursor();
			}

			mOk = new Button(mName + "/OkButton", "OK", 60);
			mOk->_assignListener(this);
			e = mOk->getOverlayElement();
			mDialogShade->addChild(e);
			e->setVerticalAlignment(Ogre::GVA_CENTER);
			e->setLeft(-(e->getWidth() / 2));
			e->setTop(mDialog->getOverlayElement()->getTop() + mDialog->getOverlayElement()->getHeight() + 5);
		}

		/*-----------------------------------------------------------------------------
		| Pops up a message dialog with an OK button.
		-----------------------------------------------------------------------------*/
		void showStreamDialog(const Ogre::DisplayString& caption, const Ogre::DisplayString& message, const Ogre::DisplayString& exerciseName)
		{
			if (mLoadBar) hideLoadingBar();

			Ogre::OverlayElement* e;

			if (mDialog)
			{
				mDialog->setCaption(caption);
				mDialog->setText(message);

				if (mOk) return;
				else
				{
					mYes->cleanup();
					mNo->cleanup();
					delete mYes;
					delete mNo;
					mYes = 0;
					mNo = 0;
				}
			}
			else
			{
				// give widgets a chance to reset in case they're in the middle of something
				for (unsigned int i = 0; i < 10; i++)
				{
					for (unsigned int j = 0; j < mWidgets[i].size(); j++)
					{
						mWidgets[i][j]->_focusLost();
					}
				}

				mDialogShade->show();

				mDialog = new TextBox(mName + "/DialogBox", "", 300, 208);
				mDialog->setText("");
				mDialog->hide();
				e = mDialog->getOverlayElement();
				mDialogShade->addChild(e);
				//mDialog2->setMetricsMode(Ogre::GMM_PIXELS);
				//mDialog2->setHorizontalAlignment(Ogre::GHA_CENTER);
				//mDialog2->setVerticalAlignment(Ogre::GVA_CENTER);
				//mDialog2->setLeft(-160);
				//mDialog2->setTop(-120);
				e->setMaterialName("DynamicTextureMaterial");
				
				e->setVerticalAlignment(Ogre::GVA_CENTER);
				e->setWidth(320);
				e->setHeight(240);
				e->setLeft(-(e->getWidth() / 2));
				e->setTop(-(e->getHeight() / 2));

				mStreamView = Ogre::OverlayManager::getSingleton().createOverlayElement("Panel", mName + "/ActualStream");
				mStreamView->setMetricsMode(Ogre::GMM_PIXELS);
				mStreamView->setHorizontalAlignment(Ogre::GHA_CENTER);
				mStreamView->setVerticalAlignment(Ogre::GVA_CENTER);
				mStreamView->setDimensions(320, 240);
				mStreamView->setLeft(-160);
				mStreamView->setTop(-120);
				//This texture is initialized from the CustomTexture class
				mStreamView->setMaterialName("DynamicTextureMaterial");
				((Ogre::OverlayContainer*)mDialogShade)->addChild(mStreamView);

				mStreamLabel = new Label("StreamLabel", exerciseName, 300);
				mStreamLabel->_assignListener(this);
				e = mStreamLabel->getOverlayElement();
				mDialogShade->addChild(e);
				e->setVerticalAlignment(Ogre::GVA_CENTER);
				e->setLeft(-(e->getWidth() / 2));
				e->setTop(0);

				mStreamViewCaptureButton = new Button("SaveModelButton", "Save model", 200);
				mStreamViewCaptureButton->_assignListener(this);
				e = mStreamViewCaptureButton->getOverlayElement();
				mDialogShade->addChild(e);
				e->setVerticalAlignment(Ogre::GVA_CENTER);
				e->setLeft(-(e->getWidth() / 2));
				e->setTop(130);

				mCursorWasVisible = isCursorVisible();
				showCursor();
				
			}

			mOk = new Button(mName + "/OkButton", "Done", 60);
			mOk->setButtonType(ButtonTypes::STREAM_DIALOG_CLOSE);
			mOk->_assignListener(this);
			e = mOk->getOverlayElement();
			mDialogShade->addChild(e);
			e->setVerticalAlignment(Ogre::GVA_CENTER);
			e->setLeft(-(e->getWidth() / 2));
			//e->setTop(mDialog->getOverlayElement()->getTop() + mDialog->getOverlayElement()->getHeight() + 5);
			e->setTop(180);
		}

		/*-----------------------------------------------------------------------------
		| Pops up a question dialog with Yes and No buttons.
		-----------------------------------------------------------------------------*/
		void showYesNoDialog(const Ogre::DisplayString& caption, const Ogre::DisplayString& question)
		{
			if (mLoadBar) hideLoadingBar();

			Ogre::OverlayElement* e;

			if (mDialog)
			{
				mDialog->setCaption(caption);
				mDialog->setText(question);

				if (mOk)
				{
					mOk->cleanup();
					delete mOk;
					mOk = 0;
				}
				else return;
			}
			else
			{
				// give widgets a chance to reset in case they're in the middle of something
				for (unsigned int i = 0; i < 10; i++)
				{
					for (unsigned int j = 0; j < mWidgets[i].size(); j++)
					{
						mWidgets[i][j]->_focusLost();
					}
				}

				mDialogShade->show();

				mDialog = new TextBox(mName + "/DialogBox", caption, 300, 208);
				mDialog->setText(question);
				e = mDialog->getOverlayElement();
				mDialogShade->addChild(e);
				e->setVerticalAlignment(Ogre::GVA_CENTER);
				e->setLeft(-(e->getWidth() / 2));
				e->setTop(-(e->getHeight() / 2));

				mCursorWasVisible = isCursorVisible();
				showCursor();
			}

			mYes = new Button(mName + "/YesButton", "Yes", 58);
			mYes->_assignListener(this);
			e = mYes->getOverlayElement();
			mDialogShade->addChild(e);
			e->setVerticalAlignment(Ogre::GVA_CENTER);
			e->setLeft(-(e->getWidth() + 2));
			e->setTop(mDialog->getOverlayElement()->getTop() + mDialog->getOverlayElement()->getHeight() + 5);

			mNo = new Button(mName + "/NoButton", "No", 50);
			mNo->_assignListener(this);
			e = mNo->getOverlayElement();
			mDialogShade->addChild(e);
			e->setVerticalAlignment(Ogre::GVA_CENTER);
			e->setLeft(3);
			e->setTop(mDialog->getOverlayElement()->getTop() + mDialog->getOverlayElement()->getHeight() + 5);
		}

		/*-----------------------------------------------------------------------------
		| Hides whatever dialog is currently showing.
		-----------------------------------------------------------------------------*/
		void closeDialog()
		{
			if (mStreamView) {
				Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();
				overlayManager.destroyOverlayElement(mStreamView->getName());
				//delete mStreamView;
				mStreamView = 0;
			}

			if (mStreamViewCaptureButton) {
				mStreamViewCaptureButton->cleanup();
				delete mStreamViewCaptureButton;
				mStreamViewCaptureButton = 0;
			}

			if (mStreamLabel) {
				mStreamLabel->cleanup();
				delete mStreamLabel;
				mStreamLabel = 0;
			}

			if (mDialog)
			{
				if (mOk)
				{
					mOk->cleanup();
					delete mOk;
					mOk = 0;
				}
				else
				{
					mYes->cleanup();
					mNo->cleanup();
					delete mYes;
					delete mNo;
					mYes = 0;
					mNo = 0;
				}

				mDialogShade->hide();
				mDialog->cleanup();
				delete mDialog;
				mDialog = 0;

				if (!mCursorWasVisible) hideCursor();
			}
		}

		/*-----------------------------------------------------------------------------
		| Determines if any dialog is currently visible.
		-----------------------------------------------------------------------------*/
		bool isDialogVisible()
		{
			return mDialog != 0;
		}

		/*-----------------------------------------------------------------------------
		| Gets a widget from a tray by place.
		-----------------------------------------------------------------------------*/
		Widget* getWidget(TrayLocation trayLoc, unsigned int place)
		{
			if (place < mWidgets[trayLoc].size()) return mWidgets[trayLoc][place];
			return 0;
		}

		/*-----------------------------------------------------------------------------
		| Gets a widget from a tray by name.
		-----------------------------------------------------------------------------*/
		Widget* getWidget(TrayLocation trayLoc, const Ogre::String& name)
		{
			for (unsigned int i = 0; i < mWidgets[trayLoc].size(); i++)
			{
				if (mWidgets[trayLoc][i]->getName() == name) return mWidgets[trayLoc][i];
			}
			return 0;
		}

		/*-----------------------------------------------------------------------------
		| Gets a widget by name.
		-----------------------------------------------------------------------------*/
		Widget* getWidget(const Ogre::String& name)
		{
			for (unsigned int i = 0; i < 10; i++)
			{
				for (unsigned int j = 0; j < mWidgets[i].size(); j++)
				{
					if (mWidgets[i][j]->getName() == name) return mWidgets[i][j];
				}
			}
			return 0;
		}

		/*-----------------------------------------------------------------------------
		| Gets the number of widgets in total.
		-----------------------------------------------------------------------------*/
		unsigned int getNumWidgets()
		{
			unsigned int total = 0;

			for (unsigned int i = 0; i < 10; i++)
			{
				total += mWidgets[i].size();
			}

			return total;
		}

		/*-----------------------------------------------------------------------------
		| Gets the number of widgets in a tray.
		-----------------------------------------------------------------------------*/
		size_t getNumWidgets(TrayLocation trayLoc)
		{
			return mWidgets[trayLoc].size();
		}

		/*-----------------------------------------------------------------------------
		| Gets all the widgets of a specific tray.
		-----------------------------------------------------------------------------*/
		WidgetIterator getWidgetIterator(TrayLocation trayLoc)
		{
			return WidgetIterator(mWidgets[trayLoc].begin(), mWidgets[trayLoc].end());
		}

		/*-----------------------------------------------------------------------------
		| Gets a widget's position in its tray.
		-----------------------------------------------------------------------------*/
		int locateWidgetInTray(Widget* widget)
		{
			for (unsigned int i = 0; i < mWidgets[widget->getTrayLocation()].size(); i++)
			{
				if (mWidgets[widget->getTrayLocation()][i] == widget) return i;
			}
			return -1;
		}

		/*-----------------------------------------------------------------------------
		| Destroys a widget.
		-----------------------------------------------------------------------------*/
		void destroyWidget(Widget* widget)
		{
			if (!widget) OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, "Widget does not exist.", "TrayManager::destroyWidget");

			// in case special widgets are destroyed manually, set them to 0
			if (widget == mLogo) mLogo = 0;
			else if (widget == mStatsPanel) mStatsPanel = 0;
			else if (widget == mFpsLabel) mFpsLabel = 0;

			mTrays[widget->getTrayLocation()]->removeChild(widget->getName());
			/*Ogre::OverlayContainer::ChildIterator itor = mTrays[widget->getTrayLocation()]->getChildIterator();
			while (itor.hasMoreElements()) itor.getNext()->_notifyZOrder(200 * 100);*/

			WidgetList& wList = mWidgets[widget->getTrayLocation()];
			wList.erase(std::find(wList.begin(), wList.end(), widget));
			if (widget == mExpandedMenu) setExpandedMenu(0);

			widget->cleanup();

			mWidgetDeathRow.push_back(widget);
			fixOrderIssue();
			adjustTrays();
		}

		void destroyWidget(TrayLocation trayLoc, unsigned int place)
		{
			destroyWidget(getWidget(trayLoc, place));
		}

		void destroyWidget(TrayLocation trayLoc, const Ogre::String& name)
		{
			destroyWidget(getWidget(trayLoc, name));
		}

		void destroyWidget(const Ogre::String& name)
		{
			destroyWidget(getWidget(name));
		}

		void fixOrderIssue()
		{
			Ogre::Overlay::Overlay2DElementsIterator itor = mTraysLayer->get2DElementsIterator();
			int count = 1;

			while (itor.hasMoreElements()) {
				//itor.getNext()->_notifyZOrder(mTraysLayer->getZOrder() + count);
				count++;
				Ogre::OverlayContainer* element = itor.getNext();
				Ogre::LogManager::getSingletonPtr()->logMessage("Iterator name " + element->getName());
				if (element->getName() != "InterfaceName/NullTray") {
					element->_notifyZOrder((mTraysLayer->getZOrder() + 1) * 100);
				}
				else {
					element->_notifyZOrder((mTraysLayer->getZOrder()+1) * 10);
					Ogre::OverlayContainer::ChildContainerIterator it = element->getChildContainerIterator();
					while (it.hasMoreElements()) {
						Ogre::OverlayContainer* cont = it.getNext();
						if (cont->getName().find("sea_star") != std::string::npos) {
							Ogre::LogManager::getSingletonPtr()->logMessage("SubIterator name " + cont->getName());
							cont->_notifyZOrder(mTraysLayer->getZOrder() * 11);
						}
						else if (cont->getName().find("bubble") != std::string::npos) {
							cont->_notifyZOrder(mTraysLayer->getZOrder() * 12);
						}
						else if (cont->getName().find("fruit") != std::string::npos) {
							cont->_notifyZOrder(mTraysLayer->getZOrder() * 13);
						}
					}
				}
			}
		}

		/*-----------------------------------------------------------------------------
		| Destroys all widgets in a tray.
		-----------------------------------------------------------------------------*/
		void destroyAllWidgetsInTray(TrayLocation trayLoc)
		{
			while (!mWidgets[trayLoc].empty()) destroyWidget(mWidgets[trayLoc][0]);
		}

		/*-----------------------------------------------------------------------------
		| Destroys all widgets.
		-----------------------------------------------------------------------------*/
		void destroyAllWidgets()
		{
			for (unsigned int i = 0; i < 10; i++)  // destroy every widget in every tray (including null tray)
			{
				destroyAllWidgetsInTray((TrayLocation)i);
			}
		}

		/*-----------------------------------------------------------------------------
		| Adds a widget to a specified tray.
		-----------------------------------------------------------------------------*/
		void moveWidgetToTray(Widget* widget, TrayLocation trayLoc, int place = -1, bool onTop = false)
		{
			if (!widget) OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, "Widget does not exist.", "TrayManager::moveWidgetToTray");

			// remove widget from old tray
			WidgetList& wList = mWidgets[widget->getTrayLocation()];
			WidgetList::iterator it = std::find(wList.begin(), wList.end(), widget);
			if (it != wList.end())
			{
				wList.erase(it);
				mTrays[widget->getTrayLocation()]->removeChild(widget->getName());
			}

			// insert widget into new tray at given position, or at the end if unspecified or invalid
			if (place == -1 || place > (int)mWidgets[trayLoc].size()) place = (int)mWidgets[trayLoc].size();
			mWidgets[trayLoc].insert(mWidgets[trayLoc].begin() + place, widget);
			mTrays[trayLoc]->addChild(widget->getOverlayElement());

			widget->getOverlayElement()->setHorizontalAlignment(mTrayWidgetAlign[trayLoc]);
			
			// adjust trays if necessary
			if (widget->getTrayLocation() != TL_NONE || trayLoc != TL_NONE) adjustTrays();

			widget->_assignToTray(trayLoc);

			fixOrderIssue();

		}

		void moveWidgetToTray(const Ogre::String& name, TrayLocation trayLoc, unsigned int place = -1)
		{
			moveWidgetToTray(getWidget(name), trayLoc, place);
		}

		void moveWidgetToTray(TrayLocation currentTrayLoc, const Ogre::String& name, TrayLocation targetTrayLoc,
			int place = -1)
		{
			moveWidgetToTray(getWidget(currentTrayLoc, name), targetTrayLoc, place);
		}

		void moveWidgetToTray(TrayLocation currentTrayLoc, unsigned int currentPlace, TrayLocation targetTrayLoc,
			int targetPlace = -1)
		{
			moveWidgetToTray(getWidget(currentTrayLoc, currentPlace), targetTrayLoc, targetPlace);
		}

		/*-----------------------------------------------------------------------------
		| Removes a widget from its tray. Same as moving it to the null tray.
		-----------------------------------------------------------------------------*/
		void removeWidgetFromTray(Widget* widget)
		{
			moveWidgetToTray(widget, TL_NONE);
		}

		void removeWidgetFromTray(const Ogre::String& name)
		{
			removeWidgetFromTray(getWidget(name));
		}

		void removeWidgetFromTray(TrayLocation trayLoc, const Ogre::String& name)
		{
			removeWidgetFromTray(getWidget(trayLoc, name));
		}

		void removeWidgetFromTray(TrayLocation trayLoc, int place)
		{
			removeWidgetFromTray(getWidget(trayLoc, place));
		}

		/*-----------------------------------------------------------------------------
		| Removes all widgets from a widget tray.
		-----------------------------------------------------------------------------*/
		void clearTray(TrayLocation trayLoc)
		{
			if (trayLoc == TL_NONE) return;      // can't clear the null tray

			while (!mWidgets[trayLoc].empty())   // remove every widget from given tray
			{
				removeWidgetFromTray(mWidgets[trayLoc][0]);
			}
		}

		/*-----------------------------------------------------------------------------
		| Removes all widgets from all widget trays.
		-----------------------------------------------------------------------------*/
		void clearAllTrays()
		{
			for (unsigned int i = 0; i < 9; i++)
			{
				clearTray((TrayLocation)i);
			}
		}

		/*-----------------------------------------------------------------------------
		| Process frame events. Updates frame statistics widget set and deletes
		| all widgets queued for destruction.
		-----------------------------------------------------------------------------*/
		bool frameRenderingQueued(const Ogre::FrameEvent& evt)
		{
			for (unsigned int i = 0; i < mWidgetDeathRow.size(); i++)
			{
				delete mWidgetDeathRow[i];
			}
			mWidgetDeathRow.clear();


            unsigned long currentTime = mTimer->getMilliseconds();
			if (areFrameStatsVisible() && currentTime - mLastStatUpdateTime > 250)
			{
                Ogre::RenderTarget::FrameStats stats = mWindow->getStatistics();

                mLastStatUpdateTime = currentTime;

				Ogre::String s("FPS: ");
				s += Ogre::StringConverter::toString((int)stats.lastFPS);
				
				mFpsLabel->setCaption(s);

				if (mStatsPanel->getOverlayElement()->isVisible())
				{
					Ogre::StringVector values;
					Ogre::StringStream oss;
					
					oss.str("");
					oss << std::fixed << std::setprecision(1) << stats.avgFPS;
					Ogre::String str = oss.str();
					values.push_back(str);

					oss.str("");
					oss << std::fixed << std::setprecision(1) << stats.bestFPS;
					str = oss.str();
					values.push_back(str);

					oss.str("");
					oss << std::fixed << std::setprecision(1) << stats.worstFPS;
					str = oss.str();
					values.push_back(str);

					str = Ogre::StringConverter::toString(stats.triangleCount);
					values.push_back(str);

					str = Ogre::StringConverter::toString(stats.batchCount);
					values.push_back(str);

					mStatsPanel->setAllParamValues(values);
				}
			}

			return true;
		}

        void windowUpdate()
        {
#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS && OGRE_PLATFORM != OGRE_PLATFORM_NACL
            mWindow->update();
#endif
        }

		void resourceGroupScriptingStarted(const Ogre::String& groupName, size_t scriptCount)
		{
			mLoadInc = mGroupInitProportion / scriptCount;
			mLoadBar->setCaption("Parsing...");
			windowUpdate();
		}

		void scriptParseStarted(const Ogre::String& scriptName, bool& skipThisScript)
		{
			mLoadBar->setComment(scriptName);
			windowUpdate();
		}

		void scriptParseEnded(const Ogre::String& scriptName, bool skipped)
		{
			mLoadBar->setProgress(mLoadBar->getProgress() + mLoadInc);
			windowUpdate();
		}

		void resourceGroupScriptingEnded(const Ogre::String& groupName) {}

		void resourceGroupLoadStarted(const Ogre::String& groupName, size_t resourceCount)
		{
			mLoadInc = mGroupLoadProportion / resourceCount;
			mLoadBar->setCaption("Loading...");
			windowUpdate();
		}

		void resourceLoadStarted(const Ogre::ResourcePtr& resource)
		{
			mLoadBar->setComment(resource->getName());
			windowUpdate();
		}

		void resourceLoadEnded()
		{
			mLoadBar->setProgress(mLoadBar->getProgress() + mLoadInc);
			windowUpdate();
		}

		void worldGeometryStageStarted(const Ogre::String& description)
		{
			mLoadBar->setComment(description);
			windowUpdate();
		}

		void worldGeometryStageEnded()
		{
			mLoadBar->setProgress(mLoadBar->getProgress() + mLoadInc);
			windowUpdate();
		}

		void resourceGroupLoadEnded(const Ogre::String& groupName) {}

		/*-----------------------------------------------------------------------------
		| Toggles visibility of advanced statistics.
		-----------------------------------------------------------------------------*/
		void labelHit(Label* label)
		{
			if (mStatsPanel->getOverlayElement()->isVisible())
			{
				mStatsPanel->getOverlayElement()->hide();
				mFpsLabel->getOverlayElement()->setWidth(150);
				removeWidgetFromTray(mStatsPanel);
			}
			else
			{
				mStatsPanel->getOverlayElement()->show();
				mFpsLabel->getOverlayElement()->setWidth(180);
				moveWidgetToTray(mStatsPanel, mFpsLabel->getTrayLocation(), locateWidgetInTray(mFpsLabel) + 1);
			}
		}

		/*-----------------------------------------------------------------------------
		| Toggles visibility of advanced statistics.
		-----------------------------------------------------------------------------*/
		void labelHit(CuModelView* label)
		{
			if (mStatsPanel->getOverlayElement()->isVisible())
			{
				mStatsPanel->getOverlayElement()->hide();
				mFpsLabel->getOverlayElement()->setWidth(150);
				removeWidgetFromTray(mStatsPanel);
			}
			else
			{
				mStatsPanel->getOverlayElement()->show();
				mFpsLabel->getOverlayElement()->setWidth(180);
				moveWidgetToTray(mStatsPanel, mFpsLabel->getTrayLocation(), locateWidgetInTray(mFpsLabel) + 1);
			}
		}

		/*-----------------------------------------------------------------------------
		| Destroys dialog widgets, notifies listener, and ends high priority session.
		-----------------------------------------------------------------------------*/
		void buttonHit(Button* button)
		{
			if (mListener)
			{
				if (button == mOk) {
					mListener->okDialogClosed(mDialog->getText());
				}
				else if (button == mStreamViewCaptureButton) {
					mListener->buttonHit(mStreamViewCaptureButton);
					return;
				}
				else {
					mListener->yesNoDialogClosed(mDialog->getText(), button == mYes);
				}
			}
			closeDialog();
		}

		/*-----------------------------------------------------------------------------
		| Processes mouse button down events. Returns true if the event was
		| consumed and should not be passed on to other handlers.
		-----------------------------------------------------------------------------*/
#if (OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS) || (OGRE_PLATFORM == OGRE_PLATFORM_ANDROID)
		bool injectMouseDown(const OIS::MultiTouchEvent& evt)
#else
		bool injectMouseDown(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
#endif
		{
#if (OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS) && (OGRE_PLATFORM != OGRE_PLATFORM_ANDROID)
			// only process left button when stuff is visible
			if (!mCursorLayer->isVisible() || id != OIS::MB_Left) return false;
#else
            // only process touches when stuff is visible
			if (!mCursorLayer->isVisible()) return false;
#endif
			Ogre::Vector2 cursorPos(mCursor->getLeft(), mCursor->getTop());
		
			mTrayDrag = false;

			if (mExpandedMenu)   // only check top priority widget until it passes on
			{
				mExpandedMenu->_cursorPressed(cursorPos);
				if (!mExpandedMenu->isExpanded()) setExpandedMenu(0);
				return true;
			}

			if (mDialog)   // only check top priority widget until it passes on
			{
				mDialog->_cursorPressed(cursorPos);
				if (mOk) {
					mOk->_cursorPressed(cursorPos);
				}
				else {
					mYes->_cursorPressed(cursorPos);
					mNo->_cursorPressed(cursorPos);
				}

				if (mStreamViewCaptureButton) {
					mStreamViewCaptureButton->_cursorPressed(cursorPos);
				}
				return true;
			}

			for (unsigned int i = 0; i < 9; i++)   // check if mouse is over a non-null tray
			{
				if (mTrays[i]->isVisible() && Widget::isCursorOver(mTrays[i], cursorPos, 2))
				{
					mTrayDrag = true;   // initiate a drag that originates in a tray
					break;
				}
			}

			for (unsigned int i = 0; i < mWidgets[9].size(); i++)  // check if mouse is over a non-null tray's widgets
			{
				if (mWidgets[9][i]->getOverlayElement()->isVisible() &&
					Widget::isCursorOver(mWidgets[9][i]->getOverlayElement(), cursorPos))
				{
					mTrayDrag = true;   // initiate a drag that originates in a tray
					break;
				}
			}

			if (!mTrayDrag) return false;   // don't process if mouse press is not in tray

			for (unsigned int i = 0; i < 10; i++)
			{
				if (!mTrays[i]->isVisible()) continue;

				for (unsigned int j = 0; j < mWidgets[i].size(); j++)
				{
					Widget* w = mWidgets[i][j];
					if (!w->getOverlayElement()->isVisible()) continue;
					w->_cursorPressed(cursorPos);    // send event to widget

					SelectMenu* m = dynamic_cast<SelectMenu*>(w);
					if (m && m->isExpanded())       // a menu has begun a top priority session
					{
						setExpandedMenu(m);
						return true;
					}
				}
			}

			return true;   // a tray click is not to be handled by another party
		}

		/*-----------------------------------------------------------------------------
		| Processes mouse button up events. Returns true if the event was
		| consumed and should not be passed on to other handlers.
		-----------------------------------------------------------------------------*/
#if (OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS) || (OGRE_PLATFORM == OGRE_PLATFORM_ANDROID)
		bool injectMouseUp(const OIS::MultiTouchEvent& evt)
#else
		bool injectMouseUp(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
#endif
		{
#if (OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS) && (OGRE_PLATFORM != OGRE_PLATFORM_ANDROID)
			// only process left button when stuff is visible
			if (!mCursorLayer->isVisible() || id != OIS::MB_Left) return false;
#else
            // only process touches when stuff is visible
			if (!mCursorLayer->isVisible()) return false;
#endif
			Ogre::Vector2 cursorPos(mCursor->getLeft(), mCursor->getTop());

			if (mExpandedMenu)   // only check top priority widget until it passes on
			{
				mExpandedMenu->_cursorReleased(cursorPos);
				return true;
			}

			if (mDialog)   // only check top priority widget until it passes on
			{
				mDialog->_cursorReleased(cursorPos);
				if (mOk) {
					mOk->_cursorReleased(cursorPos);
				}
				else
				{
					mYes->_cursorReleased(cursorPos);
					// very important to check if second button still exists, because first button could've closed the popup
					if (mNo) mNo->_cursorReleased(cursorPos); 
				}

				if (mStreamViewCaptureButton) {
					mStreamViewCaptureButton->_cursorReleased(cursorPos);
				}
				return true;
			}

			if (!mTrayDrag) return false;    // this click did not originate in a tray, so don't process

			Widget* w;

			for (unsigned int i = 0; i < 10; i++)
			{
				if (!mTrays[i]->isVisible()) continue;

				for (unsigned int j = 0; j < mWidgets[i].size(); j++)
				{
					w = mWidgets[i][j];
					if (!w->getOverlayElement()->isVisible()) continue;
					w->_cursorReleased(cursorPos);    // send event to widget
				}
			}

			mTrayDrag = false;   // stop this drag
			return true;         // this click did originate in this tray, so don't pass it on
		}

		/*-----------------------------------------------------------------------------
		| Updates cursor position. Returns true if the event was
		| consumed and should not be passed on to other handlers.
		-----------------------------------------------------------------------------*/
#if (OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS) || (OGRE_PLATFORM == OGRE_PLATFORM_ANDROID)
		bool injectMouseMove(const OIS::MultiTouchEvent& evt)
#else
		bool injectMouseMove(const OIS::MouseEvent& evt)
#endif
		{
			if (!mCursorLayer->isVisible()) return false;   // don't process if cursor layer is invisible

            Ogre::Vector2 cursorPos(evt.state.X.abs, evt.state.Y.abs);
			mCursor->setPosition(cursorPos.x, cursorPos.y);

			if (mExpandedMenu)   // only check top priority widget until it passes on
			{
				mExpandedMenu->_cursorMoved(cursorPos);
				return true;
			}

			if (mDialog)   // only check top priority widget until it passes on
			{
				mDialog->_cursorMoved(cursorPos);
				if (mOk) {
					mOk->_cursorMoved(cursorPos);
				}
				else {
					mYes->_cursorMoved(cursorPos);
					mNo->_cursorMoved(cursorPos);
				}

				if (mStreamViewCaptureButton) {
					mStreamViewCaptureButton->_cursorMoved(cursorPos);
				}
				return true;
			}

			Widget* w;

			for (unsigned int i = 0; i < 10; i++)
			{
				if (!mTrays[i]->isVisible()) continue;

				for (unsigned int j = 0; j < mWidgets[i].size(); j++)
				{
					w = mWidgets[i][j];
					if (!w->getOverlayElement()->isVisible()) continue;
					w->_cursorMoved(cursorPos);    // send event to widget
				}
			}

			if (mTrayDrag) return true;  // don't pass this event on if we're in the middle of a drag
			return false;
		}

    protected:

		/*-----------------------------------------------------------------------------
		| Internal method to prioritise / deprioritise expanded menus.
		-----------------------------------------------------------------------------*/
		void setExpandedMenu(SelectMenu* m)
		{
			if (!mExpandedMenu && m)
			{
				Ogre::OverlayContainer* c = (Ogre::OverlayContainer*)m->getOverlayElement();
				Ogre::OverlayContainer* eb = (Ogre::OverlayContainer*)c->getChild(m->getName() + "/MenuExpandedBox");
				eb->_update();
				eb->setPosition
					((unsigned int)(eb->_getDerivedLeft() * Ogre::OverlayManager::getSingleton().getViewportWidth()),
					(unsigned int)(eb->_getDerivedTop() * Ogre::OverlayManager::getSingleton().getViewportHeight()));
				c->removeChild(eb->getName());
				mPriorityLayer->add2D(eb);
			}
			else if(mExpandedMenu && !m)
			{
				Ogre::OverlayContainer* eb = mPriorityLayer->getChild(mExpandedMenu->getName() + "/MenuExpandedBox");
				mPriorityLayer->remove2D(eb);
				((Ogre::OverlayContainer*)mExpandedMenu->getOverlayElement())->addChild(eb);
			}

			mExpandedMenu = m;
		}

		Ogre::String mName;                   // name of this tray system
		Ogre::RenderWindow* mWindow;          // render window
		InputContext mInputContext;
		Ogre::Overlay* mBackdropLayer;        // backdrop layer
		Ogre::Overlay* mTraysLayer;           // widget layer
		Ogre::Overlay* mPriorityLayer;        // top priority layer
		Ogre::Overlay* mCursorLayer;          // cursor layer
		Ogre::OverlayContainer* mBackdrop;    // backdrop
		Ogre::OverlayContainer* mTrays[10];   // widget trays
	    WidgetList mWidgets[10];              // widgets
		WidgetList mWidgetDeathRow;           // widget queue for deletion
		Ogre::OverlayContainer* mCursor;      // cursor
		SdkTrayListener* mListener;           // tray listener
		Ogre::Real mWidgetPadding;            // widget padding
		Ogre::Real mWidgetSpacing;            // widget spacing
		Ogre::Real mTrayPadding;              // tray padding
		bool mTrayDrag;                       // a mouse press was initiated on a tray
		SelectMenu* mExpandedMenu;            // top priority expanded menu widget
		TextBox* mDialog;                     // top priority dialog widget
		Ogre::OverlayElement* mStreamView;		//Stream view frame
		Button* mStreamViewCaptureButton;		//Stream view button
		Label* mStreamLabel;						//Stream view label
		Ogre::OverlayContainer* mDialogShade; // top priority dialog shade
		Button* mOk;                          // top priority OK button
		Button* mYes;                         // top priority Yes button
		Button* mNo;                          // top priority No button
		bool mCursorWasVisible;               // cursor state before showing dialog
		Label* mFpsLabel;                     // FPS label
		ParamsPanel* mStatsPanel;             // frame stats panel
		DecorWidget* mLogo;                   // logo
		ProgressBar* mLoadBar;                // loading bar
		Ogre::Real mGroupInitProportion;      // proportion of load job assigned to initialising one resource group
		Ogre::Real mGroupLoadProportion;      // proportion of load job assigned to loading one resource group
		Ogre::Real mLoadInc;                  // loading increment
		Ogre::GuiHorizontalAlignment mTrayWidgetAlign[10];   // tray widget alignments
        Ogre::Timer* mTimer;                  // Root::getSingleton().getTimer()
        unsigned long mLastStatUpdateTime;    // The last time the stat text were updated

    };
}
