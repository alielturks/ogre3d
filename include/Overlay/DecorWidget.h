#pragma once

#include "stdafx.h"
#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Custom, decorative widget created from a template.
	=============================================================================*/
	class DecorWidget : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		DecorWidget(const Ogre::String& name, const Ogre::String& templateName)
		{
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate(templateName, "", name);
		}
	};

}
