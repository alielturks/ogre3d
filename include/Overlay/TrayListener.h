#include "stdafx.h"

#pragma once

#include "GUIDefinitions.h"

namespace OgreBites {

	//We need to create mocked class to avoid circular referencing
	class Button;
	class SelectMenu;
	class Label;
	class CuModelView;
	class Slider;
	class CheckBox;

	/*=============================================================================
	| Listener class for responding to tray events.
	=============================================================================*/
	class SdkTrayListener
	{
	public:

		virtual ~SdkTrayListener() {}
		virtual void buttonHit(Button* button) {}
		virtual void itemSelected(SelectMenu* menu) {}
		virtual void labelHit(Label* label) {}
		virtual void labelHit(CuModelView* label) {}
		virtual void sliderMoved(Slider* slider) {}
		virtual void checkBoxToggled(CheckBox* box) {}
		virtual void okDialogClosed(const Ogre::DisplayString& message) {}
		virtual void yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit) {}
	};

}
