#include "stdafx.h"

#pragma once

#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Basic label widget.
	=============================================================================*/
	class Label : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		Label(const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width)
		{
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate("SdkTrays/Label", "BorderPanel", name);
			mTextArea = (Ogre::TextAreaOverlayElement*)((Ogre::OverlayContainer*)mElement)->getChild(getName() + "/LabelCaption");
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
			mTextArea->setCharHeight(mTextArea->getCharHeight() - 3);
#endif
			setCaption(caption);
			if (width <= 0) mFitToTray = true;
			else
			{
				mFitToTray = false;
				mElement->setWidth(width);
			}
		}

		const Ogre::DisplayString& getCaption()
		{
			return mTextArea->getCaption();
		}

		void setCaption(const Ogre::DisplayString& caption)
		{
			mTextArea->setCaption(caption);
		}

		void _setLeft(float y)
		{
			mElement->_setLeft(y);
		}

		void _setTop(float x)
		{
			mElement->_setTop(x);
		}

		void _cursorPressed(const Ogre::Vector2& cursorPos)
		{
			if (mListener && isCursorOver(mElement, cursorPos, 3)) mListener->labelHit(this);
		}

		bool _isFitToTray()
		{
			return mFitToTray;
		}

	protected:

		Ogre::TextAreaOverlayElement* mTextArea;
		bool mFitToTray;
	};

}
