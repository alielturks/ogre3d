#include "stdafx.h"

#pragma once

#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Basic selection menu widget.
	=============================================================================*/
	class SelectMenu : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		SelectMenu(const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width,
			Ogre::Real boxWidth, unsigned int maxItemsShown)
			: mHighlightIndex(0)
			, mDisplayIndex(0)
			, mDragOffset(0.0f)
		{
			mSelectionIndex = -1;
			mFitToContents = false;
			mCursorOver = false;
			mExpanded = false;
			mDragging = false;
			mMaxItemsShown = maxItemsShown;
			mItemsShown = 0;
			mElement = (Ogre::BorderPanelOverlayElement*)Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate
				("SdkTrays/SelectMenu", "BorderPanel", name);
			mTextArea = (Ogre::TextAreaOverlayElement*)((Ogre::OverlayContainer*)mElement)->getChild(name + "/MenuCaption");
			mSmallBox = (Ogre::BorderPanelOverlayElement*)((Ogre::OverlayContainer*)mElement)->getChild(name + "/MenuSmallBox");
			mSmallBox->setWidth(width - 10);
			mSmallTextArea = (Ogre::TextAreaOverlayElement*)mSmallBox->getChild(name + "/MenuSmallBox/MenuSmallText");
			mElement->setWidth(width);
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
			mTextArea->setCharHeight(mTextArea->getCharHeight() - 3);
			mSmallTextArea->setCharHeight(mSmallTextArea->getCharHeight() - 3);
#endif

			if (boxWidth > 0)  // long style
			{
				if (width <= 0) mFitToContents = true;
				mSmallBox->setWidth(boxWidth);
				mSmallBox->setTop(2);
				mSmallBox->setLeft(width - boxWidth - 5);
				mElement->setHeight(mSmallBox->getHeight() + 4);
				//Doesn't work on release
				mTextArea->setHorizontalAlignment(Ogre::GuiHorizontalAlignment::GHA_LEFT);
				//Doesn't work on release
				mTextArea->setAlignment(Ogre::TextAreaOverlayElement::Alignment::Left);
				mTextArea->setLeft(12);
				mTextArea->setTop(10);
			}

			mExpandedBox = (Ogre::BorderPanelOverlayElement*)((Ogre::OverlayContainer*)mElement)->getChild(name + "/MenuExpandedBox");
			mExpandedBox->setWidth(mSmallBox->getWidth() + 10);
			mExpandedBox->hide();
			mScrollTrack = (Ogre::BorderPanelOverlayElement*)mExpandedBox->getChild(mExpandedBox->getName() + "/MenuScrollTrack");
			mScrollHandle = (Ogre::PanelOverlayElement*)mScrollTrack->getChild(mScrollTrack->getName() + "/MenuScrollHandle");

			setCaption(caption);
		}

		bool isExpanded()
		{
			return mExpanded;
		}

		const Ogre::DisplayString& getCaption()
		{
			return mTextArea->getCaption();
		}

		void setCaption(const Ogre::DisplayString& caption)
		{
			mTextArea->setCaption(caption);
			if (mFitToContents)
			{
				mElement->setWidth(getCaptionWidth(caption, mTextArea) + mSmallBox->getWidth() + 23);
				mSmallBox->setLeft(mElement->getWidth() - mSmallBox->getWidth() - 5);
			}
		}

		const Ogre::StringVector& getItems()
		{
			return mItems;
		}

		size_t getNumItems()
		{
			return mItems.size();
		}

		void setItems(const Ogre::StringVector& items)
		{
			mItems = items;
			mSelectionIndex = -1;

			for (unsigned int i = 0; i < mItemElements.size(); i++)   // destroy all the item elements
			{
				nukeOverlayElement(mItemElements[i]);
			}
			mItemElements.clear();

			mItemsShown = std::max<int>(2, std::min<int>(mMaxItemsShown, (int)mItems.size()));

			for (unsigned int i = 0; i < mItemsShown; i++)   // create all the item elements
			{
				Ogre::BorderPanelOverlayElement* e =
					(Ogre::BorderPanelOverlayElement*)Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate
					("SdkTrays/SelectMenuItem", "BorderPanel",
					mExpandedBox->getName() + "/Item" + Ogre::StringConverter::toString(i + 1));

				e->setTop(6 + i * (mSmallBox->getHeight() - 8));
				e->setWidth(mExpandedBox->getWidth() - 32);

				mExpandedBox->addChild(e);
				mItemElements.push_back(e);
			}

			if (!items.empty()) selectItem(0, false);
			else mSmallTextArea->setCaption("");
		}

		void addItem(const Ogre::DisplayString& item)
		{
			mItems.push_back(item);
			setItems(mItems);
		}

		void removeItem(const Ogre::DisplayString& item)
		{
			Ogre::StringVector::iterator it;

			for (it = mItems.begin(); it != mItems.end(); it++)
			{
				if (item == *it) break;
			}

			if (it != mItems.end())
			{
				mItems.erase(it);
				if (mItems.size() < mItemsShown)
				{
					mItemsShown = (int)mItems.size();
					nukeOverlayElement(mItemElements.back());
					mItemElements.pop_back();
				}
			}
			else
			{
				Ogre::String desc = "Menu \"" + getName() + "\" contains no item \"" + item + "\".";
				OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "SelectMenu::removeItem");
			}
		}

		void removeItem(unsigned int index)
		{
			Ogre::StringVector::iterator it;
			unsigned int i = 0;

			for (it = mItems.begin(); it != mItems.end(); it++)
			{
				if (i == index) break;
				i++;
			}

			if (it != mItems.end())
			{
				mItems.erase(it);
				if (mItems.size() < mItemsShown)
				{
					mItemsShown = (int)mItems.size();
					nukeOverlayElement(mItemElements.back());
					mItemElements.pop_back();
				}
			}
			else
			{
				Ogre::String desc = "Menu \"" + getName() + "\" contains no item at position " +
					Ogre::StringConverter::toString(index) + ".";
				OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "SelectMenu::removeItem");
			}
		}

		void clearItems()
		{
			mItems.clear();
			mSelectionIndex = -1;
			mSmallTextArea->setCaption("");
		}

		void selectItem(unsigned int index, bool notifyListener = true)
		{
			if (index >= mItems.size())
			{
				Ogre::String desc = "Menu \"" + getName() + "\" contains no item at position " +
					Ogre::StringConverter::toString(index) + ".";
				OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "SelectMenu::selectItem");
			}

			mSelectionIndex = index;
			fitCaptionToArea(mItems[index], mSmallTextArea, mSmallBox->getWidth() - mSmallTextArea->getLeft() * 2);

			if (mListener && notifyListener) mListener->itemSelected(this);
		}

		void selectItem(const Ogre::DisplayString& item, bool notifyListener = true)
		{
			for (unsigned int i = 0; i < mItems.size(); i++)
			{
				if (item == mItems[i])
				{
					selectItem(i, notifyListener);
					return;
				}
			}

			Ogre::String desc = "Menu \"" + getName() + "\" contains no item \"" + item + "\".";
			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "SelectMenu::selectItem");
		}

		Ogre::DisplayString getSelectedItem()
		{
			if (mSelectionIndex == -1)
			{
				Ogre::String desc = "Menu \"" + getName() + "\" has no item selected.";
				OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "SelectMenu::getSelectedItem");
				return "";
			}
			else return mItems[mSelectionIndex];
		}

		int getSelectionIndex()
		{
			return mSelectionIndex;
		}

		void _cursorPressed(const Ogre::Vector2& cursorPos)
		{
			Ogre::OverlayManager& om = Ogre::OverlayManager::getSingleton();

			if (mExpanded)
			{
				if (mScrollHandle->isVisible())   // check for scrolling
				{
					Ogre::Vector2 co = Widget::cursorOffset(mScrollHandle, cursorPos);

					if (co.squaredLength() <= 81)
					{
						mDragging = true;
						mDragOffset = co.y;
						return;
					}
					else if (Widget::isCursorOver(mScrollTrack, cursorPos))
					{
						Ogre::Real newTop = mScrollHandle->getTop() + co.y;
						Ogre::Real lowerBoundary = mScrollTrack->getHeight() - mScrollHandle->getHeight();
						mScrollHandle->setTop(Ogre::Math::Clamp<int>((int)newTop, 0, (int)lowerBoundary));

						Ogre::Real scrollPercentage = Ogre::Math::Clamp<Ogre::Real>(newTop / lowerBoundary, 0, 1);
						setDisplayIndex((unsigned int)(scrollPercentage * (mItems.size() - mItemElements.size()) + 0.5));
						return;
					}
				}

				if (!isCursorOver(mExpandedBox, cursorPos, 3)) retract();
				else
				{
					Ogre::Real l = mItemElements.front()->_getDerivedLeft() * om.getViewportWidth() + 5;
					Ogre::Real t = mItemElements.front()->_getDerivedTop() * om.getViewportHeight() + 5;
					Ogre::Real r = l + mItemElements.back()->getWidth() - 10;
					Ogre::Real b = mItemElements.back()->_getDerivedTop() * om.getViewportHeight() +
						mItemElements.back()->getHeight() - 5;

					if (cursorPos.x >= l && cursorPos.x <= r && cursorPos.y >= t && cursorPos.y <= b)
					{
						if (mHighlightIndex != mSelectionIndex) selectItem(mHighlightIndex);
						retract();
					}
				}
			}
			else
			{
				if (mItems.size() < 2) return;   // don't waste time showing a menu if there's no choice

				if (isCursorOver(mSmallBox, cursorPos, 4))
				{
					mExpandedBox->show();
					mSmallBox->hide();

					// calculate how much vertical space we need
					Ogre::Real idealHeight = mItemsShown * (mSmallBox->getHeight() - 8) + 20;
					mExpandedBox->setHeight(idealHeight);
					mScrollTrack->setHeight(mExpandedBox->getHeight() - 20);

					mExpandedBox->setLeft(mSmallBox->getLeft() - 4);

					// if the expanded menu goes down off the screen, make it go up instead
					if (mSmallBox->_getDerivedTop() * om.getViewportHeight() + idealHeight > om.getViewportHeight())
					{
						mExpandedBox->setTop(mSmallBox->getTop() + mSmallBox->getHeight() - idealHeight + 3);
						// if we're in thick style, hide the caption because it will interfere with the expanded menu
						if (mTextArea->getHorizontalAlignment() == Ogre::GHA_CENTER) mTextArea->hide();
					}
					else mExpandedBox->setTop(mSmallBox->getTop() + 3);

					mExpanded = true;
					mHighlightIndex = mSelectionIndex;
					setDisplayIndex(mHighlightIndex);

					if (mItemsShown < mItems.size())  // update scrollbar position
					{
						mScrollHandle->show();
						Ogre::Real lowerBoundary = mScrollTrack->getHeight() - mScrollHandle->getHeight();
						mScrollHandle->setTop((int)(mDisplayIndex * lowerBoundary / (mItems.size() - mItemElements.size())));
					}
					else mScrollHandle->hide();
				}
			}
		}

		void _cursorReleased(const Ogre::Vector2& cursorPos)
		{
			mDragging = false;
		}

		void _cursorMoved(const Ogre::Vector2& cursorPos)
		{
			Ogre::OverlayManager& om = Ogre::OverlayManager::getSingleton();

			if (mExpanded)
			{
				if (mDragging)
				{
					Ogre::Vector2 co = Widget::cursorOffset(mScrollHandle, cursorPos);
					Ogre::Real newTop = mScrollHandle->getTop() + co.y - mDragOffset;
					Ogre::Real lowerBoundary = mScrollTrack->getHeight() - mScrollHandle->getHeight();
					mScrollHandle->setTop(Ogre::Math::Clamp<int>((int)newTop, 0, (int)lowerBoundary));

					Ogre::Real scrollPercentage = Ogre::Math::Clamp<Ogre::Real>(newTop / lowerBoundary, 0, 1);
					int newIndex = (int)(scrollPercentage * (mItems.size() - mItemElements.size()) + 0.5);
					if (newIndex != mDisplayIndex) setDisplayIndex(newIndex);
					return;
				}

				Ogre::Real l = mItemElements.front()->_getDerivedLeft() * om.getViewportWidth() + 5;
				Ogre::Real t = mItemElements.front()->_getDerivedTop() * om.getViewportHeight() + 5;
				Ogre::Real r = l + mItemElements.back()->getWidth() - 10;
				Ogre::Real b = mItemElements.back()->_getDerivedTop() * om.getViewportHeight() +
					mItemElements.back()->getHeight() - 5;

				if (cursorPos.x >= l && cursorPos.x <= r && cursorPos.y >= t && cursorPos.y <= b)
				{
					int newIndex = (int)(mDisplayIndex + (cursorPos.y - t) / (b - t) * mItemElements.size());
					if (mHighlightIndex != newIndex)
					{
						mHighlightIndex = newIndex;
						setDisplayIndex(mDisplayIndex);
					}
				}
			}
			else
			{
				if (isCursorOver(mSmallBox, cursorPos, 4))
				{
					mSmallBox->setMaterialName("SdkTrays/MiniTextBox/Over");
					mSmallBox->setBorderMaterialName("SdkTrays/MiniTextBox/Over");
					mCursorOver = true;
				}
				else
				{
					if (mCursorOver)
					{
						mSmallBox->setMaterialName("SdkTrays/MiniTextBox");
						mSmallBox->setBorderMaterialName("SdkTrays/MiniTextBox");
						mCursorOver = false;
					}
				}
			}
		}

		void _focusLost()
		{
			if (mExpandedBox->isVisible()) retract();
		}

	protected:

		/*-----------------------------------------------------------------------------
		| Internal method - sets which item goes at the top of the expanded menu.
		-----------------------------------------------------------------------------*/
		void setDisplayIndex(unsigned int index)
		{
			index = std::min<int>(index, (int)(mItems.size() - mItemElements.size()));
			mDisplayIndex = index;
			Ogre::BorderPanelOverlayElement* ie;
			Ogre::TextAreaOverlayElement* ta;

			for (int i = 0; i < (int)mItemElements.size(); i++)
			{
				ie = mItemElements[i];
				ta = (Ogre::TextAreaOverlayElement*)ie->getChild(ie->getName() + "/MenuItemText");

				fitCaptionToArea(mItems[mDisplayIndex + i], ta, ie->getWidth() - 2 * ta->getLeft());

				if ((mDisplayIndex + i) == mHighlightIndex)
				{
					ie->setMaterialName("SdkTrays/MiniTextBox/Over");
					ie->setBorderMaterialName("SdkTrays/MiniTextBox/Over");
				}
				else
				{
					ie->setMaterialName("SdkTrays/MiniTextBox");
					ie->setBorderMaterialName("SdkTrays/MiniTextBox");
				}
			}
		}

		/*-----------------------------------------------------------------------------
		| Internal method - cleans up an expanded menu.
		-----------------------------------------------------------------------------*/
		void retract()
		{
			mDragging = false;
			mExpanded = false;
			mExpandedBox->hide();
			mTextArea->show();
			mSmallBox->show();
			mSmallBox->setMaterialName("SdkTrays/MiniTextBox");
			mSmallBox->setBorderMaterialName("SdkTrays/MiniTextBox");
		}

		Ogre::BorderPanelOverlayElement* mSmallBox;
		Ogre::BorderPanelOverlayElement* mExpandedBox;
		Ogre::TextAreaOverlayElement* mTextArea;
		Ogre::TextAreaOverlayElement* mSmallTextArea;
		Ogre::BorderPanelOverlayElement* mScrollTrack;
		Ogre::PanelOverlayElement* mScrollHandle;
		std::vector<Ogre::BorderPanelOverlayElement*> mItemElements;
		unsigned int mMaxItemsShown;
		unsigned int mItemsShown;
		bool mCursorOver;
		bool mExpanded;
		bool mFitToContents;
		bool mDragging;
		Ogre::StringVector mItems;
		int mSelectionIndex;
		int mHighlightIndex;
		int mDisplayIndex;
		Ogre::Real mDragOffset;
	};

}
