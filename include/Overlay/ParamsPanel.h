#include "stdafx.h"

#pragma once

#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Basic parameters panel widget.
	=============================================================================*/
	class ParamsPanel : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		ParamsPanel(const Ogre::String& name, Ogre::Real width, unsigned int lines)
		{
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate
				("SdkTrays/ParamsPanel", "BorderPanel", name);
			Ogre::OverlayContainer* c = (Ogre::OverlayContainer*)mElement;
			mNamesArea = (Ogre::TextAreaOverlayElement*)c->getChild(getName() + "/ParamsPanelNames");
			mValuesArea = (Ogre::TextAreaOverlayElement*)c->getChild(getName() + "/ParamsPanelValues");
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
			mNamesArea->setCharHeight(mNamesArea->getCharHeight() - 3);
			mValuesArea->setCharHeight(mValuesArea->getCharHeight() - 3);
#endif
			mElement->setWidth(width);
			mElement->setHeight(mNamesArea->getTop() * 2 + lines * mNamesArea->getCharHeight());
		}

		void setAllParamNames(const Ogre::StringVector& paramNames)
		{
			mNames = paramNames;
			mValues.clear();
			mValues.resize(mNames.size(), "");
			mElement->setHeight(mNamesArea->getTop() * 2 + mNames.size() * mNamesArea->getCharHeight());
			updateText();
		}

		const Ogre::StringVector& getAllParamNames()
		{
			return mNames;
		}

		void setAllParamValues(const Ogre::StringVector& paramValues)
		{
			mValues = paramValues;
			mValues.resize(mNames.size(), "");
			updateText();
		}

		void setParamValue(const Ogre::DisplayString& paramName, const Ogre::DisplayString& paramValue)
		{
			for (unsigned int i = 0; i < mNames.size(); i++)
			{
				if (mNames[i] == DISPLAY_STRING_TO_STRING(paramName))
				{
					mValues[i] = DISPLAY_STRING_TO_STRING(paramValue);
					updateText();
					return;
				}
			}

			Ogre::String desc = "ParamsPanel \"" + getName() + "\" has no parameter \"" + DISPLAY_STRING_TO_STRING(paramName) + "\".";
			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "ParamsPanel::setParamValue");
		}

		void setParamValue(unsigned int index, const Ogre::DisplayString& paramValue)
		{
			if (index >= mNames.size())
			{
				Ogre::String desc = "ParamsPanel \"" + getName() + "\" has no parameter at position " +
					Ogre::StringConverter::toString(index) + ".";
				OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "ParamsPanel::setParamValue");
			}

			mValues[index] = DISPLAY_STRING_TO_STRING(paramValue);
			updateText();
		}

		Ogre::DisplayString getParamValue(const Ogre::DisplayString& paramName)
		{
			for (unsigned int i = 0; i < mNames.size(); i++)
			{
				if (mNames[i] == DISPLAY_STRING_TO_STRING(paramName)) return mValues[i];
			}

			Ogre::String desc = "ParamsPanel \"" + getName() + "\" has no parameter \"" + DISPLAY_STRING_TO_STRING(paramName) + "\".";
			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "ParamsPanel::getParamValue");
			return "";
		}

		Ogre::DisplayString getParamValue(unsigned int index)
		{
			if (index >= mNames.size())
			{
				Ogre::String desc = "ParamsPanel \"" + getName() + "\" has no parameter at position " +
					Ogre::StringConverter::toString(index) + ".";
				OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, desc, "ParamsPanel::getParamValue");
			}

			return mValues[index];
		}

		const Ogre::StringVector& getAllParamValues()
		{
			return mValues;
		}

	protected:

		/*-----------------------------------------------------------------------------
		| Internal method - updates text areas based on name and value lists.
		-----------------------------------------------------------------------------*/
		void updateText()
		{
			Ogre::DisplayString namesDS;
			Ogre::DisplayString valuesDS;

			for (unsigned int i = 0; i < mNames.size(); i++)
			{
				namesDS.append(mNames[i] + ":\n");
				valuesDS.append(mValues[i] + "\n");
			}

			mNamesArea->setCaption(namesDS);
			mValuesArea->setCaption(valuesDS);
		}

		Ogre::TextAreaOverlayElement* mNamesArea;
		Ogre::TextAreaOverlayElement* mValuesArea;
		Ogre::StringVector mNames;
		Ogre::StringVector mValues;
	};

}
