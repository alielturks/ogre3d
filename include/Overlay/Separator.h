#include "stdafx.h"

#pragma once

#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Basic separator widget.
	=============================================================================*/
	class Separator : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		Separator(const Ogre::String& name, Ogre::Real width)
		{
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate("SdkTrays/Separator", "Panel", name);
			if (width <= 0) mFitToTray = true;
			else
			{
				mFitToTray = false;
				mElement->setWidth(width);
			}
		}

		bool _isFitToTray()
		{
			return mFitToTray;
		}

	protected:

		bool mFitToTray;
	};

}
