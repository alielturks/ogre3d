#include "stdafx.h"

#pragma once

#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Basic slider widget.
	=============================================================================*/
	class Slider : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		Slider(const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width, Ogre::Real trackWidth,
			Ogre::Real valueBoxWidth, Ogre::Real minValue, Ogre::Real maxValue, unsigned int snaps)
			: mDragOffset(0.0f)
			, mValue(0.0f)
			, mMinValue(0.0f)
			, mMaxValue(0.0f)
			, mInterval(0.0f)
		{
			mDragging = false;
			mFitToContents = false;
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate
				("SdkTrays/Slider", "BorderPanel", name);
			mElement->setWidth(width);
			Ogre::OverlayContainer* c = (Ogre::OverlayContainer*)mElement;
			mTextArea = (Ogre::TextAreaOverlayElement*)c->getChild(getName() + "/SliderCaption");
			Ogre::OverlayContainer* valueBox = (Ogre::OverlayContainer*)c->getChild(getName() + "/SliderValueBox");
			valueBox->setWidth(valueBoxWidth);
			valueBox->setLeft(-(valueBoxWidth + 5));
			mValueTextArea = (Ogre::TextAreaOverlayElement*)valueBox->getChild(valueBox->getName() + "/SliderValueText");
			mTrack = (Ogre::BorderPanelOverlayElement*)c->getChild(getName() + "/SliderTrack");
			mHandle = (Ogre::PanelOverlayElement*)mTrack->getChild(mTrack->getName() + "/SliderHandle");
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
			mTextArea->setCharHeight(mTextArea->getCharHeight() - 3);
			mValueTextArea->setCharHeight(mValueTextArea->getCharHeight() - 3);
#endif

			if (trackWidth <= 0)  // tall style
			{
				mTrack->setWidth(width - 16);
			}
			else  // long style
			{
				if (width <= 0) mFitToContents = true;
				mElement->setHeight(34);
				mTextArea->setTop(10);
				valueBox->setTop(2);
				mTrack->setTop(-23);
				mTrack->setWidth(trackWidth);
				mTrack->setHorizontalAlignment(Ogre::GHA_RIGHT);
				mTrack->setLeft(-(trackWidth + valueBoxWidth + 5));
			}

			setCaption(caption);
			setRange(minValue, maxValue, snaps, false);
		}

		/*-----------------------------------------------------------------------------
		| Sets the minimum value, maximum value, and the number of snapping points.
		-----------------------------------------------------------------------------*/
		void setRange(Ogre::Real minValue, Ogre::Real maxValue, unsigned int snaps, bool notifyListener = true)
		{
			mMinValue = minValue;
			mMaxValue = maxValue;

			if (snaps <= 1 || mMinValue >= mMaxValue)
			{
				mInterval = 0;
				mHandle->hide();
				mValue = minValue;
				if (snaps == 1) mValueTextArea->setCaption(Ogre::StringConverter::toString(mMinValue));
				else mValueTextArea->setCaption("");
			}
			else
			{
				mHandle->show();
				mInterval = (maxValue - minValue) / (snaps - 1);
				setValue(minValue, notifyListener);
			}
		}

		const Ogre::DisplayString& getValueCaption()
		{
			return mValueTextArea->getCaption();
		}

		/*-----------------------------------------------------------------------------
		| You can use this method to manually format how the value is displayed.
		-----------------------------------------------------------------------------*/
		void setValueCaption(const Ogre::DisplayString& caption)
		{
			mValueTextArea->setCaption(caption);
		}

		void setValue(Ogre::Real value, bool notifyListener = true)
		{
			if (mInterval == 0) return;

			mValue = Ogre::Math::Clamp<Ogre::Real>(value, mMinValue, mMaxValue);

			setValueCaption(Ogre::StringConverter::toString(mValue));

			if (mListener && notifyListener) mListener->sliderMoved(this);

			if (!mDragging) mHandle->setLeft((int)((mValue - mMinValue) / (mMaxValue - mMinValue) *
				(mTrack->getWidth() - mHandle->getWidth())));
		}

		Ogre::Real getValue()
		{
			return mValue;
		}

		const Ogre::DisplayString& getCaption()
		{
			return mTextArea->getCaption();
		}

		void setCaption(const Ogre::DisplayString& caption)
		{
			mTextArea->setCaption(caption);

			if (mFitToContents) mElement->setWidth(getCaptionWidth(caption, mTextArea) +
				mValueTextArea->getParent()->getWidth() + mTrack->getWidth() + 26);
		}

		void _cursorPressed(const Ogre::Vector2& cursorPos)
		{
			if (!mHandle->isVisible()) return;

			Ogre::Vector2 co = Widget::cursorOffset(mHandle, cursorPos);

			if (co.squaredLength() <= 81)
			{
				mDragging = true;
				mDragOffset = co.x;
			}
			else if (Widget::isCursorOver(mTrack, cursorPos))
			{
				Ogre::Real newLeft = mHandle->getLeft() + co.x;
				Ogre::Real rightBoundary = mTrack->getWidth() - mHandle->getWidth();

				mHandle->setLeft(Ogre::Math::Clamp<int>((int)newLeft, 0, (int)rightBoundary));
				setValue(getSnappedValue(newLeft / rightBoundary));
			}
		}

		void _cursorReleased(const Ogre::Vector2& cursorPos)
		{
			if (mDragging)
			{
				mDragging = false;
				mHandle->setLeft((int)((mValue - mMinValue) / (mMaxValue - mMinValue) *
					(mTrack->getWidth() - mHandle->getWidth())));
			}
		}

		void _cursorMoved(const Ogre::Vector2& cursorPos)
		{
			if (mDragging)
			{
				Ogre::Vector2 co = Widget::cursorOffset(mHandle, cursorPos);
				Ogre::Real newLeft = mHandle->getLeft() + co.x - mDragOffset;
				Ogre::Real rightBoundary = mTrack->getWidth() - mHandle->getWidth();

				mHandle->setLeft(Ogre::Math::Clamp<int>((int)newLeft, 0, (int)rightBoundary));
				setValue(getSnappedValue(newLeft / rightBoundary));
			}
		}

		void _focusLost()
		{
			mDragging = false;
		}

	protected:

		/*-----------------------------------------------------------------------------
		| Internal method - given a percentage (from left to right), gets the
		| value of the nearest marker.
		-----------------------------------------------------------------------------*/
		Ogre::Real getSnappedValue(Ogre::Real percentage)
		{
			percentage = Ogre::Math::Clamp<Ogre::Real>(percentage, 0, 1);
			unsigned int whichMarker = (unsigned int)(percentage * (mMaxValue - mMinValue) / mInterval + 0.5);
			return whichMarker * mInterval + mMinValue;
		}

		Ogre::TextAreaOverlayElement* mTextArea;
		Ogre::TextAreaOverlayElement* mValueTextArea;
		Ogre::BorderPanelOverlayElement* mTrack;
		Ogre::PanelOverlayElement* mHandle;
		bool mDragging;
		bool mFitToContents;
		Ogre::Real mDragOffset;
		Ogre::Real mValue;
		Ogre::Real mMinValue;
		Ogre::Real mMaxValue;
		Ogre::Real mInterval;
	};

}
