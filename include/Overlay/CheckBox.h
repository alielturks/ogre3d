#pragma once
#include "stdafx.h"
#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Basic check box widget.
	=============================================================================*/
	class CheckBox : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		CheckBox(const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width)
		{
			mCursorOver = false;
			mFitToContents = width <= 0;
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate
				("SdkTrays/CheckBox", "BorderPanel", name);
			Ogre::OverlayContainer* c = (Ogre::OverlayContainer*)mElement;
			mTextArea = (Ogre::TextAreaOverlayElement*)c->getChild(getName() + "/CheckBoxCaption");
			mSquare = (Ogre::BorderPanelOverlayElement*)c->getChild(getName() + "/CheckBoxSquare");
			mX = mSquare->getChild(mSquare->getName() + "/CheckBoxX");
			mX->hide();
			mElement->setWidth(width);
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
			mTextArea->setCharHeight(mTextArea->getCharHeight() - 3);
#endif
			setCaption(caption);
		}

		const Ogre::DisplayString& getCaption()
		{
			return mTextArea->getCaption();
		}

		void setCaption(const Ogre::DisplayString& caption)
		{
			mTextArea->setCaption(caption);
			if (mFitToContents) mElement->setWidth(getCaptionWidth(caption, mTextArea) + mSquare->getWidth() + 23);
		}

		bool isChecked()
		{
			return mX->isVisible();
		}

		void setChecked(bool checked, bool notifyListener = true)
		{
			if (checked) mX->show();
			else mX->hide();
			if (mListener && notifyListener) mListener->checkBoxToggled(this);
		}

		void toggle(bool notifyListener = true)
		{
			setChecked(!isChecked(), notifyListener);
		}

		void _cursorPressed(const Ogre::Vector2& cursorPos)
		{
			if (mCursorOver && mListener) toggle();
		}

		void _cursorMoved(const Ogre::Vector2& cursorPos)
		{
			if (isCursorOver(mSquare, cursorPos, 5))
			{
				if (!mCursorOver)
				{
					mCursorOver = true;
					mSquare->setMaterialName("SdkTrays/MiniTextBox/Over");
					mSquare->setBorderMaterialName("SdkTrays/MiniTextBox/Over");
				}
			}
			else
			{
				if (mCursorOver)
				{
					mCursorOver = false;
					mSquare->setMaterialName("SdkTrays/MiniTextBox");
					mSquare->setBorderMaterialName("SdkTrays/MiniTextBox");
				}
			}
		}

		void _focusLost()
		{
			mSquare->setMaterialName("SdkTrays/MiniTextBox");
			mSquare->setBorderMaterialName("SdkTrays/MiniTextBox");
			mCursorOver = false;
		}

	protected:

		Ogre::TextAreaOverlayElement* mTextArea;
		Ogre::BorderPanelOverlayElement* mSquare;
		Ogre::OverlayElement* mX;
		bool mFitToContents;
		bool mCursorOver;
	};

}
