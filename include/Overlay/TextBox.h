#include "stdafx.h"

#pragma once

#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Scrollable text box widget.
	=============================================================================*/
	class TextBox : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		TextBox(const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width, Ogre::Real height)
		{
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate("SdkTrays/TextBox", "BorderPanel", name);
			mElement->setWidth(width);
			mElement->setHeight(height);
			Ogre::OverlayContainer* container = (Ogre::OverlayContainer*)mElement;
			mTextArea = (Ogre::TextAreaOverlayElement*)container->getChild(getName() + "/TextBoxText");
			mCaptionBar = (Ogre::BorderPanelOverlayElement*)container->getChild(getName() + "/TextBoxCaptionBar");
			mCaptionBar->setWidth(width - 4);
			mCaptionTextArea = (Ogre::TextAreaOverlayElement*)mCaptionBar->getChild(mCaptionBar->getName() + "/TextBoxCaption");
			setCaption(caption);
			mScrollTrack = (Ogre::BorderPanelOverlayElement*)container->getChild(getName() + "/TextBoxScrollTrack");
			mScrollHandle = (Ogre::PanelOverlayElement*)mScrollTrack->getChild(mScrollTrack->getName() + "/TextBoxScrollHandle");
			mScrollHandle->hide();
			mDragging = false;
			mScrollPercentage = 0;
			mStartingLine = 0;
			mPadding = 15;
			mText = "";
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
			mTextArea->setCharHeight(mTextArea->getCharHeight() - 3);
			mCaptionTextArea->setCharHeight(mCaptionTextArea->getCharHeight() - 3);
#endif
			refitContents();
		}

		void setPadding(Ogre::Real padding)
		{
			mPadding = padding;
			refitContents();
		}

		Ogre::Real getPadding()
		{
			return mPadding;
		}

		const Ogre::DisplayString& getCaption()
		{
			return mCaptionTextArea->getCaption();
		}

		void setCaption(const Ogre::DisplayString& caption)
		{
			mCaptionTextArea->setCaption(caption);
		}

		const Ogre::DisplayString& getText()
		{
			return mText;
		}

		/*-----------------------------------------------------------------------------
		| Sets text box content. Most of this method is for wordwrap.
		-----------------------------------------------------------------------------*/
		void setText(const Ogre::DisplayString& text)
		{
			mText = text;
			mLines.clear();

			Ogre::Font* font = (Ogre::Font*)Ogre::FontManager::getSingleton().getByName(mTextArea->getFontName()).getPointer();

			Ogre::String current = DISPLAY_STRING_TO_STRING(text);
			bool firstWord = true;
			unsigned int lastSpace = 0;
			unsigned int lineBegin = 0;
			Ogre::Real lineWidth = 0;
			Ogre::Real rightBoundary = mElement->getWidth() - 2 * mPadding + mScrollTrack->getLeft() + 10;

			for (unsigned int i = 0; i < current.length(); i++)
			{
				if (current[i] == ' ')
				{
					if (mTextArea->getSpaceWidth() != 0) lineWidth += mTextArea->getSpaceWidth();
					else lineWidth += font->getGlyphAspectRatio(' ') * mTextArea->getCharHeight();
					firstWord = false;
					lastSpace = i;
				}
				else if (current[i] == '\n')
				{
					firstWord = true;
					lineWidth = 0;
					mLines.push_back(current.substr(lineBegin, i - lineBegin));
					lineBegin = i + 1;
				}
				else
				{
					// use glyph information to calculate line width
					lineWidth += font->getGlyphAspectRatio(current[i]) * mTextArea->getCharHeight();
					if (lineWidth > rightBoundary)
					{
						if (firstWord)
						{
							current.insert(i, "\n");
							i = i - 1;
						}
						else
						{
							current[lastSpace] = '\n';
							i = lastSpace - 1;
						}
					}
				}
			}

			mLines.push_back(current.substr(lineBegin));

			unsigned int maxLines = getHeightInLines();

			if (mLines.size() > maxLines)     // if too much text, filter based on scroll percentage
			{
				mScrollHandle->show();
				filterLines();
			}
			else       // otherwise just show all the text
			{
				mTextArea->setCaption(current);
				mScrollHandle->hide();
				mScrollPercentage = 0;
				mScrollHandle->setTop(0);
			}
		}

		/*-----------------------------------------------------------------------------
		| Sets text box content horizontal alignment.
		-----------------------------------------------------------------------------*/
		void setTextAlignment(Ogre::TextAreaOverlayElement::Alignment ta)
		{
			if (ta == Ogre::TextAreaOverlayElement::Left) mTextArea->setHorizontalAlignment(Ogre::GHA_LEFT);
			else if (ta == Ogre::TextAreaOverlayElement::Center) mTextArea->setHorizontalAlignment(Ogre::GHA_CENTER);
			else mTextArea->setHorizontalAlignment(Ogre::GHA_RIGHT);
			refitContents();
		}

		void clearText()
		{
			setText("");
		}

		void appendText(const Ogre::DisplayString& text)
		{
			setText(getText() + text);
		}

		/*-----------------------------------------------------------------------------
		| Makes adjustments based on new padding, size, or alignment info.
		-----------------------------------------------------------------------------*/
		void refitContents()
		{
			mScrollTrack->setHeight(mElement->getHeight() - mCaptionBar->getHeight() - 20);
			mScrollTrack->setTop(mCaptionBar->getHeight() + 10);

			mTextArea->setTop(mCaptionBar->getHeight() + mPadding - 5);
			if (mTextArea->getHorizontalAlignment() == Ogre::GHA_RIGHT) mTextArea->setLeft(-mPadding + mScrollTrack->getLeft());
			else if (mTextArea->getHorizontalAlignment() == Ogre::GHA_LEFT) mTextArea->setLeft(mPadding);
			else mTextArea->setLeft(mScrollTrack->getLeft() / 2);

			setText(getText());
		}

		/*-----------------------------------------------------------------------------
		| Sets how far scrolled down the text is as a percentage.
		-----------------------------------------------------------------------------*/
		void setScrollPercentage(Ogre::Real percentage)
		{
			mScrollPercentage = Ogre::Math::Clamp<Ogre::Real>(percentage, 0, 1);
			mScrollHandle->setTop((int)(percentage * (mScrollTrack->getHeight() - mScrollHandle->getHeight())));
			filterLines();
		}

		/*-----------------------------------------------------------------------------
		| Gets how far scrolled down the text is as a percentage.
		-----------------------------------------------------------------------------*/
		Ogre::Real getScrollPercentage()
		{
			return mScrollPercentage;
		}

		/*-----------------------------------------------------------------------------
		| Gets how many lines of text can fit in this window.
		-----------------------------------------------------------------------------*/
		unsigned int getHeightInLines()
		{
			return (unsigned int)((mElement->getHeight() - 2 * mPadding - mCaptionBar->getHeight() + 5) / mTextArea->getCharHeight());
		}

		void _cursorPressed(const Ogre::Vector2& cursorPos)
		{
			if (!mScrollHandle->isVisible()) return;   // don't care about clicks if text not scrollable

			Ogre::Vector2 co = Widget::cursorOffset(mScrollHandle, cursorPos);

			if (co.squaredLength() <= 81)
			{
				mDragging = true;
				mDragOffset = co.y;
			}
			else if (Widget::isCursorOver(mScrollTrack, cursorPos))
			{
				Ogre::Real newTop = mScrollHandle->getTop() + co.y;
				Ogre::Real lowerBoundary = mScrollTrack->getHeight() - mScrollHandle->getHeight();
				mScrollHandle->setTop(Ogre::Math::Clamp<int>((int)newTop, 0, (int)lowerBoundary));

				// update text area contents based on new scroll percentage
				mScrollPercentage = Ogre::Math::Clamp<Ogre::Real>(newTop / lowerBoundary, 0, 1);
				filterLines();
			}
		}

		void _cursorReleased(const Ogre::Vector2& cursorPos)
		{
			mDragging = false;
		}

		void _cursorMoved(const Ogre::Vector2& cursorPos)
		{
			if (mDragging)
			{
				Ogre::Vector2 co = Widget::cursorOffset(mScrollHandle, cursorPos);
				Ogre::Real newTop = mScrollHandle->getTop() + co.y - mDragOffset;
				Ogre::Real lowerBoundary = mScrollTrack->getHeight() - mScrollHandle->getHeight();
				mScrollHandle->setTop(Ogre::Math::Clamp<int>((int)newTop, 0, (int)lowerBoundary));

				// update text area contents based on new scroll percentage
				mScrollPercentage = Ogre::Math::Clamp<Ogre::Real>(newTop / lowerBoundary, 0, 1);
				filterLines();
			}
		}

		void _focusLost()
		{
			mDragging = false;  // stop dragging if cursor was lost
		}

	protected:

		/*-----------------------------------------------------------------------------
		| Decides which lines to show.
		-----------------------------------------------------------------------------*/
		void filterLines()
		{
			Ogre::String shown = "";
			unsigned int maxLines = getHeightInLines();
			unsigned int newStart = (unsigned int)(mScrollPercentage * (mLines.size() - maxLines) + 0.5);

			mStartingLine = newStart;

			for (unsigned int i = 0; i < maxLines; i++)
			{
				shown += mLines[mStartingLine + i] + "\n";
			}

			mTextArea->setCaption(shown);    // show just the filtered lines
		}

		Ogre::TextAreaOverlayElement* mTextArea;
		Ogre::BorderPanelOverlayElement* mCaptionBar;
		Ogre::TextAreaOverlayElement* mCaptionTextArea;
		Ogre::BorderPanelOverlayElement* mScrollTrack;
		Ogre::PanelOverlayElement* mScrollHandle;
		Ogre::DisplayString mText;
		Ogre::StringVector mLines;
		Ogre::Real mPadding;
		bool mDragging;
		Ogre::Real mScrollPercentage;
		Ogre::Real mDragOffset;
		unsigned int mStartingLine;
	};

}
