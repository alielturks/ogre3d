#pragma once
#include "stdafx.h"

namespace OgreBites {
	enum ButtonTypes
	{
		MODEL_NEW = 1,
		MODEL_DELETE = 2,
		MODEL_USE = 3,

		TRAINING_PLAN_COUNT_UP = 101,
		TRAINING_PLAN_COUNT_DOWN = 102,
		TRAINING_PLAN_TRESHOLD_UP = 103,
		TRAINING_PLAN_TRESHOLD_DOWN = 104,
		TRAINING_PLAN_ADD_TO_LIST = 105,
		TRAINING_PLAN_REMOVE_FROM_LIST = 106,
		TRAINING_PLAN_PLAY_SINGLE = 107,

		STREAM_DIALOG_CLOSE = 201

	};

	enum TrayLocation   // enumerator values for widget tray anchoring locations
	{
		TL_TOPLEFT,
		TL_TOP,
		TL_TOPRIGHT,
		TL_LEFT,
		TL_CENTER,
		TL_RIGHT,
		TL_BOTTOMLEFT,
		TL_BOTTOM,
		TL_BOTTOMRIGHT,
		TL_NONE
	};

	enum ButtonState   // enumerator values for button states
	{
		BS_UP,
		BS_OVER,
		BS_DOWN
	};

}
