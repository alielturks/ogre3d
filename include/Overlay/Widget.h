#include "stdafx.h"

#pragma once

#include "GUIDefinitions.h"
#include "TrayListener.h"

namespace OgreBites {

	/*=============================================================================
	| Abstract base class for all widgets.
	=============================================================================*/

	class SdkTrayListener;

	class Widget
	{
	protected:

		Ogre::OverlayElement* mElement;
		TrayLocation mTrayLoc;
		SdkTrayListener* mListener;
		Ogre::Real vpWidth;
		Ogre::Real vpHeight;

		/**
		* Get and set viewport size
		*/
		void initVPSize()
		{
			Ogre::OverlayManager& oMgr = Ogre::OverlayManager::getSingleton();
			vpWidth = (Ogre::Real)(oMgr.getViewportWidth());
			vpHeight = (Ogre::Real)(oMgr.getViewportHeight());
		}

	public:

		Widget()
		{
			mTrayLoc = TL_NONE;
			mElement = 0;
			mListener = 0;
		}

		virtual ~Widget() {}

		void cleanup()
		{
			if (mElement) nukeOverlayElement(mElement);
			mElement = 0;
		}

		/*-----------------------------------------------------------------------------
		| Static utility method to recursively delete an overlay element plus
		| all of its children from the system.
		-----------------------------------------------------------------------------*/
		static void nukeOverlayElement(Ogre::OverlayElement* element)
		{
			Ogre::OverlayContainer* container = dynamic_cast<Ogre::OverlayContainer*>(element);
			if (container)
			{
				std::vector<Ogre::OverlayElement*> toDelete;

				Ogre::OverlayContainer::ChildIterator children = container->getChildIterator();
				while (children.hasMoreElements())
				{
					toDelete.push_back(children.getNext());
				}

				for (unsigned int i = 0; i < toDelete.size(); i++)
				{
					nukeOverlayElement(toDelete[i]);
				}
			}
			if (element)
			{
				Ogre::OverlayContainer* parent = element->getParent();
				if (parent) parent->removeChild(element->getName());
				Ogre::OverlayManager::getSingleton().destroyOverlayElement(element);
			}
		}

		/*-----------------------------------------------------------------------------
		| Static utility method to check if the cursor is over an overlay element.
		-----------------------------------------------------------------------------*/
		static bool isCursorOver(Ogre::OverlayElement* element, const Ogre::Vector2& cursorPos, Ogre::Real voidBorder = 0)
		{
			Ogre::OverlayManager& om = Ogre::OverlayManager::getSingleton();
			Ogre::Real l = element->_getDerivedLeft() * om.getViewportWidth();
			Ogre::Real t = element->_getDerivedTop() * om.getViewportHeight();
			Ogre::Real r = l + element->getWidth();
			Ogre::Real b = t + element->getHeight();

			return (cursorPos.x >= l + voidBorder && cursorPos.x <= r - voidBorder &&
				cursorPos.y >= t + voidBorder && cursorPos.y <= b - voidBorder);
		}

		/*-----------------------------------------------------------------------------
		| Static utility method used to get the cursor's offset from the center
		| of an overlay element in pixels.
		-----------------------------------------------------------------------------*/
		static Ogre::Vector2 cursorOffset(Ogre::OverlayElement* element, const Ogre::Vector2& cursorPos)
		{
			Ogre::OverlayManager& om = Ogre::OverlayManager::getSingleton();
			return Ogre::Vector2(cursorPos.x - (element->_getDerivedLeft() * om.getViewportWidth() + element->getWidth() / 2),
				cursorPos.y - (element->_getDerivedTop() * om.getViewportHeight() + element->getHeight() / 2));
		}

		/*-----------------------------------------------------------------------------
		| Static utility method used to get the width of a caption in a text area.
		-----------------------------------------------------------------------------*/
		static Ogre::Real getCaptionWidth(const Ogre::DisplayString& caption, Ogre::TextAreaOverlayElement* area)
		{
			Ogre::Font* font = (Ogre::Font*)Ogre::FontManager::getSingleton().getByName(area->getFontName()).getPointer();
			Ogre::String current = DISPLAY_STRING_TO_STRING(caption);
			Ogre::Real lineWidth = 0;

			for (unsigned int i = 0; i < current.length(); i++)
			{
				// be sure to provide a line width in the text area
				if (current[i] == ' ')
				{
					if (area->getSpaceWidth() != 0) lineWidth += area->getSpaceWidth();
					else lineWidth += font->getGlyphAspectRatio(' ') * area->getCharHeight();
				}
				else if (current[i] == '\n') break;
				// use glyph information to calculate line width
				else lineWidth += font->getGlyphAspectRatio(current[i]) * area->getCharHeight();
			}

			return (unsigned int)lineWidth;
		}

		/*-----------------------------------------------------------------------------
		| Static utility method to cut off a string to fit in a text area.
		-----------------------------------------------------------------------------*/
		static void fitCaptionToArea(const Ogre::DisplayString& caption, Ogre::TextAreaOverlayElement* area, Ogre::Real maxWidth)
		{
			Ogre::Font* f = (Ogre::Font*)Ogre::FontManager::getSingleton().getByName(area->getFontName()).getPointer();
			Ogre::String s = DISPLAY_STRING_TO_STRING(caption);

			size_t nl = s.find('\n');
			if (nl != Ogre::String::npos) s = s.substr(0, nl);

			Ogre::Real width = 0;

			for (unsigned int i = 0; i < s.length(); i++)
			{
				if (s[i] == ' ' && area->getSpaceWidth() != 0) width += area->getSpaceWidth();
				else width += f->getGlyphAspectRatio(s[i]) * area->getCharHeight();
				if (width > maxWidth)
				{
					s = s.substr(0, i);
					break;
				}
			}

			area->setCaption(s);
		}

		Ogre::OverlayElement* getOverlayElement()
		{
			return mElement;
		}

		const Ogre::String& getName()
		{
			return mElement->getName();
		}

		TrayLocation getTrayLocation()
		{
			return mTrayLoc;
		}

		void hide()
		{
			mElement->hide();
		}

		void show()
		{
			mElement->show();
		}

		bool isVisible()
		{
			return mElement->isVisible();
		}

		// callbacks

		virtual void _cursorPressed(const Ogre::Vector2& cursorPos) {}
		virtual void _cursorReleased(const Ogre::Vector2& cursorPos) {}
		virtual void _cursorMoved(const Ogre::Vector2& cursorPos) {}
		virtual void _focusLost() {}

		// internal methods used by SdkTrayManager. do not call directly.

		void _assignToTray(TrayLocation trayLoc) { mTrayLoc = trayLoc; }
		void _assignListener(SdkTrayListener* listener) { mListener = listener; }
	};

}
