#include "stdafx.h"

#pragma once

#include "Widget.h"

namespace OgreBites {
	/*=============================================================================
	| Basic progress bar widget.
	=============================================================================*/
	class ProgressBar : public Widget
	{
	public:

		// Do not instantiate any widgets directly. Use SdkTrayManager.
		ProgressBar(const Ogre::String& name, const Ogre::DisplayString& caption, Ogre::Real width, Ogre::Real commentBoxWidth)
			: mProgress(0.0f)
		{
			mElement = Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate
				("SdkTrays/ProgressBar", "BorderPanel", name);
			mElement->setWidth(width);
			Ogre::OverlayContainer* c = (Ogre::OverlayContainer*)mElement;
			mTextArea = (Ogre::TextAreaOverlayElement*)c->getChild(getName() + "/ProgressCaption");
			Ogre::OverlayContainer* commentBox = (Ogre::OverlayContainer*)c->getChild(getName() + "/ProgressCommentBox");
			commentBox->setWidth(commentBoxWidth);
			commentBox->setLeft(-(commentBoxWidth + 5));
			mCommentTextArea = (Ogre::TextAreaOverlayElement*)commentBox->getChild(commentBox->getName() + "/ProgressCommentText");
			mMeter = c->getChild(getName() + "/ProgressMeter");
			mMeter->setWidth(width - 10);
			mFill = ((Ogre::OverlayContainer*)mMeter)->getChild(mMeter->getName() + "/ProgressFill");
			setCaption(caption);
		}

		/*-----------------------------------------------------------------------------
		| Sets the progress as a percentage.
		-----------------------------------------------------------------------------*/
		void setProgress(Ogre::Real progress)
		{
			mProgress = Ogre::Math::Clamp<Ogre::Real>(progress, 0, 1);
			mFill->setWidth(std::max<int>((int)mFill->getHeight(), (int)(mProgress * (mMeter->getWidth() - 2 * mFill->getLeft()))));
		}

		/*-----------------------------------------------------------------------------
		| Gets the progress as a percentage.
		-----------------------------------------------------------------------------*/
		Ogre::Real getProgress()
		{
			return mProgress;
		}

		const Ogre::DisplayString& getCaption()
		{
			return mTextArea->getCaption();
		}

		void setCaption(const Ogre::DisplayString& caption)
		{
			mTextArea->setCaption(caption);
		}

		const Ogre::DisplayString& getComment()
		{
			return mCommentTextArea->getCaption();
		}

		void setComment(const Ogre::DisplayString& comment)
		{
			mCommentTextArea->setCaption(comment);
		}


	protected:

		Ogre::TextAreaOverlayElement* mTextArea;
		Ogre::TextAreaOverlayElement* mCommentTextArea;
		Ogre::OverlayElement* mMeter;
		Ogre::OverlayElement* mFill;
		Ogre::Real mProgress;
	};

}
