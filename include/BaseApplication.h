/*
-----------------------------------------------------------------------------
Filename:    BaseApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __BaseApplication_h_
#define __BaseApplication_h_

#include "stdafx.h"
#include "GUI.h"
#include "GameEventListener.h"

//---------------------------------------------------------------------------
struct joystick
{
	int deadZone;
	int max;

	int yaw;
	int pitch;

	int swivel;
	//   int swivelL;
};

class BaseApplication : 
	public Ogre::FrameListener, 
	public Ogre::WindowEventListener, 
	public OIS::KeyListener, 
	public OIS::MouseListener, 
	OgreBites::SdkTrayListener, 
	public OIS::JoyStickListener,
	public Ogre::RenderTargetListener,
	public GameEvent::GameEventListener

{
public:
    BaseApplication(void);
    virtual ~BaseApplication(void);

    virtual void go(void);
	Ogre::Camera* getCamera();

	/**
	* Called when any of the events are triggered
	*/
	virtual void eventTriggered(GameEvent::EventTypes evt);

protected:
    virtual bool setup();
    virtual bool configure(void);
    virtual void chooseSceneManager(void);
    virtual void createCamera(void);
    virtual void createFrameListener(void);
    virtual void createScene(void) = 0; // Override me!
    virtual void destroyScene(void);
    virtual void createViewports(void);
    virtual void setupResources(void);
    virtual void createResourceListener(void);
    virtual void loadResources(void);
    virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);
	virtual void preViewportUpdate(const Ogre::RenderTargetViewportEvent& evt);

	virtual bool frameStarted(const Ogre::FrameEvent& evt);
	virtual bool frameEnded(const Ogre::FrameEvent& evt);

    virtual bool keyPressed(const OIS::KeyEvent &arg);
    virtual bool keyReleased(const OIS::KeyEvent &arg);
    virtual bool mouseMoved(const OIS::MouseEvent &arg);
    virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

	/**
	 * Joystick events
	 */
	virtual bool axisMoved(const OIS::JoyStickEvent &e, int axis);
	virtual bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
	virtual bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
	virtual bool povMoved(const OIS::JoyStickEvent &arg, int pov);

	virtual void preRenderTargetUpdate(const Ogre::RenderTargetEvent& rte);
	virtual void postRenderTargetUpdate(const Ogre::RenderTargetEvent& rte);

    // Adjust mouse clipping area
    virtual void windowResized(Ogre::RenderWindow* rw);
    // Unattach OIS before window shutdown (very important under Linux)
    virtual void windowClosed(Ogre::RenderWindow* rw);

    Ogre::Root*                 mRoot;
    Ogre::SceneManager*         mSceneMgr;
    Ogre::RenderWindow*         mWindow;
    Ogre::String                mResourcesCfg;
    Ogre::String                mPluginsCfg;

    Ogre::OverlaySystem*        mOverlaySystem;

	Ogre::Viewport* vp1;
	Ogre::Viewport* vp2;

    // OgreBites
    OgreBites::InputContext     mInputContext;
    OgreBites::SdkTrayManager*	mTrayMgr;
    OgreBites::ParamsPanel*     mDetailsPanel;   	// Sample details panel
    bool                        mCursorWasVisible;	// Was cursor visible before dialog appeared?
    bool                        mShutDown;

    //OIS Input devices
    OIS::InputManager*          mInputManager;
    OIS::Mouse*                 mMouse;
    OIS::Keyboard*              mKeyboard;

	std::vector<OIS::JoyStick*> _joysticks;
	std::vector<OgreBites::SdkCameraMan*> _playerCameraMans;
	std::vector<Ogre::Camera*> _playerCameras;
	std::vector<Ogre::Viewport*> _playerViewports;

	joystick joyAxes;
    // Added for Mac compatibility
    Ogre::String                 m_ResourcePath;

	int last_x;
	int last_y;

	Ogre::Rectangle2D* mMiniscreen;
	Ogre::AnimationState* mAnimationState;
	std::vector<Ogre::AnimationState*> mAnimationStates;

#ifdef OGRE_STATIC_LIB
    Ogre::StaticPluginLoader m_StaticPluginLoader;
#endif
};

//---------------------------------------------------------------------------

#endif // #ifndef __BaseApplication_h_

//---------------------------------------------------------------------------