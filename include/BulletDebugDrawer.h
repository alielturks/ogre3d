#pragma once
#include "stdafx.h"
#include <LinearMath\btIDebugDraw.h>

class BulletDebugDrawer : public btIDebugDraw
{
	int m_debugMode;
	Ogre::SceneManager* _sceneManager;
public:
	BulletDebugDrawer(){};
	~BulletDebugDrawer(){};

	virtual void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color) {
		//Ogre::LogManager().getSingletonPtr()->logMessage("draw Line initialized");
	};
	virtual void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &fromColor, const btVector3 &toColor) {
		//Ogre::LogManager().getSingletonPtr()->logMessage("draw Line initialized 222");
	};
	virtual void 	drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color) {};
	virtual void 	reportErrorWarning(const char *warningString) {};
	virtual void 	draw3dText(const btVector3 &location, const char *textString) {};
	virtual void 	setDebugMode(int debugMode) { m_debugMode = debugMode;  };

	virtual int 	getDebugMode() const { return m_debugMode; };

	void setSceneManager(Ogre::SceneManager* _mgr) {
		_sceneManager = _mgr;
	}
};