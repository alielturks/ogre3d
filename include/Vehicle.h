#pragma once
#include "stdafx.h"
#include "BulletDynamics\Vehicle\btRaycastVehicle.h"
#include "BulletDynamics\Vehicle\btVehicleRaycaster.h"
#include "BulletDynamics\Vehicle\btWheelInfo.h"
#include "Overlay\GUI.h"
#include "Requests\RequestResults.h"


class Vehicle {
private:
	btRaycastVehicle* _raycastVehicle;
	btRaycastVehicle::btVehicleTuning* vehicleTuning;

	btRigidBody* _chasis;
	std::vector<btRigidBody*> _wheels;
	std::vector<btTransform> _wheelInfo;
	std::vector<Ogre::SceneNode*> _wheelNodes;
	btDynamicsWorld* _world;
	bool _stateAccelerate;
	bool _stateBackward;
	bool _stateBrake;
	bool _stateSteerLeft;
	bool _stateSteerRight;
	bool _stateRoll;

	float _steerValue;

	Ogre::SceneManager* _sceneManager;
	OgreBites::SdkTrayManager* _trayManager;
	int _index;

	void addWheel(btVector3 &position, int index, bool frontWheel);

	bool _boostEnabled;
	float _boostTime;

	float _defaultFrictionValue;
	bool _iceWheelsEnabled;
	float _iceWheelsTime;

	/**
	 * Initial position of the vehicle
	 */
	btVector3 _startPosition;

	/**
	 * Information about wheel paramaeters, friction, stifness etc.
	 */
	ServerRequests::VehicleInfo _vehicleInfo;

	/**
	 * Current engine momentum
	 */
	float _forwardMomentum;
	float _backwardMomentum;

public:

	Vehicle();
	~Vehicle();

	void init();

	void setChasis(btRigidBody* body);
	void setWheels(std::vector<btRigidBody*> wheels);
	void setWorld(btDynamicsWorld* world);
	void setSceneManager(Ogre::SceneManager* mgr);
	void setTrayManager(OgreBites::SdkTrayManager* mgr);

	void accelerate(bool val);
	void backward(bool val);
	void brake(bool val);
	void steerLeft(bool val);
	void steerRight(bool val);

	void roll(bool val);

	void setSteer(float val);

	void update(const Ogre::FrameEvent& evt);

	float getSpeed();

	void setIndex(int ind);

	int getIndex();

	void setSpeed(const float s);

	void resetCar();

	/**
	 * Set initial vehicle position
	 */
	void setStartPosition(float x, float y, float z);

	std::vector<btTransform>* getWheelInfo();

	/**
	 * Apply some kind of boost to the car
	 */
	void applyBoost(float val);

	/**
	 * Wheels have low friction now
	 */
	void enableIceWheels(float val, float time);

	float getIceWheelsTime();
	
	/**
	 *Set information about wheels
	 */
	void setVehicleInfo(ServerRequests::VehicleInfo info);
};