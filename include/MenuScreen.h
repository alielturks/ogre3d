#pragma once
#include "BasicScreen.h"
#include "OgreFrameListener.h"
#include "TrayListener.h"

namespace Screens {

	class MenuScreen : public BasicScreen, public OgreBites::SdkTrayListener, public OIS::JoyStickListener
	{
		enum ViewTypes {
			MAIN,
			PLAY,
			OPTIONS,
			OPTIONS_CONTROLS,
			OPTIONS_VIDEO
		};

		enum ControlOptions {
			LEFT,
			RIGHT,
			ACCELERATE,
			BRAKE,
			RESET,
			NONE
		};
		
		typedef struct Controls {
			int left;
			bool leftAnalog;
			int right;
			bool rightAnalog;
			int accelerate;
			bool accelerateAnalog;
			int brake;
			bool brakeAnalog;
			int reset;
			bool resetAnalog;
			void setLeft(int val, bool analog) {
				left = val;
				leftAnalog = analog;
			}
			void setRight(int val, bool analog) {
				right = val;
				rightAnalog = analog;
			}
			void setAccelerate(int val, bool analog) {
				accelerate = val;
				accelerateAnalog = analog;
			}
			void setBrake(int val, bool analog) {
				brake = val;
				brakeAnalog = analog;
			}
			void setReset(int val, bool analog) {
				reset = val;
				resetAnalog = analog;
			}
		};

	private:
		/**
		 * Logger handler
		 */
		Ogre::Log* _logger;

		ViewTypes _currentView;

		Controls* _controllerValue;
		ControlOptions _currentButtonListener;

		/**
		 * Currently selected player in Main->Options->Controls view
		 */
		unsigned int _selectedPlayer;

		Ogre::SceneNode* _backgroundCar;

		std::map<unsigned int, Controls> _controls;

		/**
		 * Create main view with play, options and exit buttons
		 */
		void createMainView();
		void destroyMainView();

		/**
		 * View with video, controller, back buttons
		 */
		void createOptionsView();
		void destroyOptionsView();

		/**
		 * Controller setup view
		 */
		void createControlsView();
		void destroyControlsView();
		void updatecontrolsView();

		/**
		 * Video settings view
		 */
		void createVideoView();
		void destroyVideoView();

		/**
		 * Play view
		 */
		void createPlayView();
		void destroyPlayView();

		/**
		 * Switch to different view
		 */
		void createView(ViewTypes type);

	protected:
	
	public:
		MenuScreen();
		~MenuScreen();

		virtual void create();
		virtual void destroy();
		virtual void update(const Ogre::FrameEvent evt);

		virtual void buttonHit(OgreBites::Button* button);

		/**
		* Joystick events
		*/
		virtual bool axisMoved(const OIS::JoyStickEvent &e, int axis);
		virtual bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
		virtual bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
		virtual bool povMoved(const OIS::JoyStickEvent &arg, int pov);
	};
}
