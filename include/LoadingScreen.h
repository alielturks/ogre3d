#pragma once
#include "BasicScreen.h"
#include "OgreFrameListener.h"
#include "TrayListener.h"
#include "Requests\RequestThreadController.h"

namespace Screens {

	class LoadingScreen : public BasicScreen, public OgreBites::SdkTrayListener
	{
	private:
		/**
		 * Label which will hold information about currently
		 * processed job
		 */
		OgreBites::Label* _loadDescription;

		/**
		 * Pointer to the RequestThreadController class
		 */
		ServerRequests::RequestThreadController* _requestThreadController;

	protected:
	
	public:
		LoadingScreen();
		~LoadingScreen();

		virtual void create();
		virtual void destroy();
		virtual void update(const Ogre::FrameEvent evt);

		virtual void buttonHit(OgreBites::Button* button);

		/**
		 * Set the requestThreadController class pointer
		 */
		void setRequestThreadController(ServerRequests::RequestThreadController* r);
	};
}
