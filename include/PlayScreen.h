#pragma once
#include "BasicScreen.h"
#include "OgreFrameListener.h"

namespace Screens {
	class PlayScreen : public BasicScreen
	{
	private:
		Ogre::Log* _logger;

	public:
		PlayScreen();
		~PlayScreen();

		virtual void create();
		virtual void update(const Ogre::FrameEvent evt);
		virtual void destroy();

	};
}
