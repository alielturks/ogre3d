#pragma once
#include "OgreSceneManager.h"
#include "GUI.h"
#include "GameEventListener.h"
#include "BulletManager.h"

namespace Screens {
	enum ScreenTypes {
		LOADING,
		MENU,
		PLAY
	};

	enum ScreenEvents {
		NONE, //remain in the same screen
		GO_TO_MENU_SCREEN, //we need to move to the menu screen
		GO_TO_PLAY_SCREEN, // we need to move to the game screen
		EXIT_GAME //we need to exit the game
	};

	class BasicScreen
	{
	private:
		/**
		 * Describe screen type
		 */
		ScreenTypes _type;

		/**
		 * Describe overall state of the game
		 * remain in the same screen or change to another screen
		 * or maybe exit game?
		 */
		ScreenEvents _event;

		Ogre::SceneManager* _sceneManager;
		OgreBites::SdkTrayManager* _trayManager;
		GameEvent::GameEventDispatcher* _eventDispatcher;
		BulletManager* _bulletManager;

	protected:
		Ogre::SceneManager* getSceneManager();
		OgreBites::SdkTrayManager* getTrayManager();
		GameEvent::GameEventDispatcher* getEventDispatcher();
		BulletManager* getBulletManager();
		void setScreenEvent(ScreenEvents evt);

	public:
		BasicScreen(ScreenTypes type);
		virtual ~BasicScreen();

		virtual void create() = 0;
		virtual void update(const Ogre::FrameEvent evt) = 0;
		virtual void destroy() = 0;

		/**
		 * Set ogre scene manager
		 */
		void setSceneManager(Ogre::SceneManager* mgr);

		/**
		 * Set tray manager
		 */
		void setTrayManager(OgreBites::SdkTrayManager* mgr);

		/**
		 * Set the event dispatcher
		 */
		void setEventDispatcher(GameEvent::GameEventDispatcher* evt);

		/**
		 * Set the bullet manager
		 */
		void setBulletManager(BulletManager* mgr);

		/** 
		 * Get this screen type, either it's menu or play screen
		 */
		ScreenTypes getScreenType();

		/**
		 * Get the current screen event
		 */
		ScreenEvents getScreenEvent();
	};
}
