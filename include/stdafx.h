// stdafx.h

// Ogre headers you need
#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#  include <OIS/OISEvents.h>
#  include <OIS/OISInputManager.h>
#  include <OIS/OISKeyboard.h>
#  include <OIS/OISMouse.h>
#  include <OGRE/SdkCameraMan.h>
#else
#  include <OISEvents.h>
#  include <OISInputManager.h>
#  include <OISKeyboard.h>
#  include <OISJoyStick.h>
#  include <OISMouse.h>
#  include <SdkCameraMan.h>
#endif

// any other header can be included, as usual
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef OGRE_STATIC_LIB
#  define OGRE_STATIC_GL
#  if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#    define OGRE_STATIC_Direct3D9
// D3D10 will only work on vista, so be careful about statically linking
#    if OGRE_USE_D3D10
#      define OGRE_STATIC_Direct3D10
#    endif
#  endif
#  define OGRE_STATIC_BSPSceneManager
#  define OGRE_STATIC_ParticleFX
#  define OGRE_STATIC_CgProgramManager
#  ifdef OGRE_USE_PCZ
#    define OGRE_STATIC_PCZSceneManager
#    define OGRE_STATIC_OctreeZone
#  else
#    define OGRE_STATIC_OctreeSceneManager
#  endif
#  include "OgreStaticPluginLoader.h"
#endif

#include <vector>
#include <btBulletDynamicsCommon.h>
#include "btBulletCollisionCommon.h"

#include "Ogre.h"
#include "OgreOverlaySystem.h"
#include <math.h>

#include "OgreTimer.h"

#include "InputContext.h"

#if OGRE_COMPILER == OGRE_COMPILER_MSVC
// TODO - remove this
#   pragma warning (disable : 4244)
#endif

#if OGRE_UNICODE_SUPPORT
#	if	OGRE_STRING_USE_CUSTOM_MEMORY_ALLOCATOR
#		define DISPLAY_STRING_TO_STRING(DS) (DS.asUTF8_c_str())
#	else
#		define DISPLAY_STRING_TO_STRING(DS) (DS.asUTF8())
#	endif
#else
#define DISPLAY_STRING_TO_STRING(DS) (DS)
#endif