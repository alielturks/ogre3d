#pragma once
#include <string>
#include <iostream>
#include <stdio.h>
#include <curl/curl.h>
//setprecision and fixed
#include <iomanip>
//stringstream
#include <sstream>
/* For older cURL versions you will also need
#include <curl/types.h>
#include <curl/easy.h>
*/

namespace ServerRequests {

	class FileDownloader
	{
	private:
		/**
		 * Parses input string and creates folder structure from that
		 * Example:
		 * if input is "first/second/third"  then the folder structure will be created as following
		 *		first
		 *		  |_second
		 *  	    |_third
		 */
		void parseFolderString(std::string path);
		static int progress_func(void* ptr, double TotalToDownload, double NowDownloaded, double TotalToUpload, double NowUploaded);
	public:
		FileDownloader();
		~FileDownloader();

		/**
		 * Send a file download request
		 * @param url - full url to the file
		 * @param folder - where to store retrieved file
		 * @param filename - what should be the name of the saved file
		 */
		void download(std::string url, std::string folder, std::string filename);

		static size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream);
	};
}
