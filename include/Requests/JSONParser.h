#pragma once
#include "rapidjson\document.h"
#include "rapidjson\writer.h"
#include "rapidjson\stringbuffer.h"
#include <iostream>
#include <string>
#include <curl/curl.h>
#include <mutex>
#include "Requests\RequestResults.h"

/* For older cURL versions you will also need
#include <curl/types.h>
#include <curl/easy.h>
*/

namespace ServerRequests {

	class JSONParser
	{
	private:
		/**
		 * Make JSON request to the server
		 */
		void makeRequest();

		/**
		 * Read response json data stream which we later can write to the _buffer element
		 */
		static size_t loadJson(void *ptr, size_t size, size_t nmemb, void *stream);

		/**
		 * This will hold the retrieved response
		 */
		std::string _buffer;

		/**
		 * Url of the request
		 */
		std::string _url;

		/**
		 * JSON object that we will use to send requests
		 */
		rapidjson::Document _outgoingJson;

		/**
		 * Pointer to class which holds the obtained request results
		 */
		RequestResults* _requestResults;

		/**
		 * Check and read file if it exists
		 */
		void checkAndRead(std::string filepath);

	public:
		JSONParser();
		~JSONParser();

		/**
		 * Do specific Json parsing
		 */
		void parse();

		void readPlayerList();

		void readVersion();

		void readVehicles();

		void readTrack();

		/**
		 * Set the url from which we want to get the JSON object
		 */
		void setUrl(std::string url);

		/**
		 * Return the list of obtained vehicles
		 */
		std::list<VehicleInfo> getObtainedVehicles();

		/**
		 * Get pointer to the json object which we will send with the request
		 */
		rapidjson::Document* getJsonObject();

		/**
		 * Set the _requestResults class pointer
		 */
		void setRequestResultsPointer(RequestResults* r);
	};
}
