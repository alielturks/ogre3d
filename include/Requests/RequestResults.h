#pragma once
#include <mutex>

namespace ServerRequests {

	/**
	* This will hold information about one vehicle
	*/
	typedef struct VehicleInfo {
		std::string name;
		float mass;
		float speed;
		float friction;
		float suspensionForce;
		float suspensionTravel;
		float suspensionCompression;
		float suspensionDamping;
		float suspensionStiffness;
		float maxSteerValue;
		float brakeForce;
		bool front_drive;
		bool rear_drive;
	};

	/**
	 * Track grid information
	 */
	typedef struct TrackInfo {
		int width;
		int height;
		float scale;
		std::vector<float> mapPoints;
	};

	/**
	 * This class will be responsible for holding already obtained request results
	 */
	class RequestResults
	{
	private:
		/**
		 * List of obtained vehicles
		 */
		std::list<VehicleInfo> _vehicles;

		/**
		 * Mutex which will guard _vehicles list
		 */
		std::mutex _vehiclesMutex;

		/**
		 * Track data
		 */
		TrackInfo _trackInfo;

		/**
		 * Mutex that will secure the _trackInfo struct
		 */
		std::mutex _trackMutex;
	public:
		RequestResults();
		~RequestResults();

		/**
		 * Add new vehicle to the list
		 */
		void addVehicle(VehicleInfo i);

		/**
		 * Clear the vehicle list
		 */
		void clearVehicles();

		/**
		 * Add map points
		 */
		void addMapPoint(int w, int h, float val);

		/**
		 * Clear the map
		 */
		void clearMap();

		/**
		 * Obtain list of vehicles
		 */
		std::list<VehicleInfo> getVehicles();

		/**
		 * Get pointer to the track info
		 */
		TrackInfo* getTrackInfo();

		/**
		 * Set track dimensions
		 */
		void setTrackSizeAndScale(int width, int height, float scale);
	};
}
