#pragma once
#include <string>
#include "FileDownloader.h"
#include "JSONParser.h"
#include "Requests\RequestResults.h"

namespace ServerRequests {

	enum RequestTypes {
		FILE_DOWNLOAD, //Download specific file
		JSON_FILE_LIST, //Obtain the list of files to download
		JSON_PLAYER_LIST, //Retrieve player list
		JSON_VERSION, //Get the version info for the updates
		JSON_VEHICLES, //Obtain vehicle list
		JSON_TRACK //Obtain track information
	};

	class RequestHandler
	{
	private:
		/**
		 * Class which will handle file downloads
		 */
		FileDownloader _fileDownloader;

		/**
		 * Class which will deal with the JSON object upload/download
		 */
		JSONParser _jsonParser;

		/**
		 * Pointer to the obtained results class
		 */
		RequestResults* _requestResults;

	public:
		RequestHandler();
		~RequestHandler();

		/**
		* Send request and expect JSON in return
		*/
		void retrieveJson(std::string url, RequestTypes type);

		/**
		* Download file
		*/
		void downloadFile(std::string url, std::string folder, std::string filename);

		/**
		 * Retrieve pointer to json parser object
		 */
		JSONParser* getJsonParser();

		/**
		 * Set the _requestResults class pointer
		 */
		void setRequestResultsPointer(RequestResults* r);
	};
}
