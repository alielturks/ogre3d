#pragma once
#include "Requests\RequestHandler.h"
#include "Requests\RequestResults.h"
#include <mutex>

namespace ServerRequests {

	/**
	 * Enumeration for every possible task that RequestThreadController can process
	 */
	enum TaskList {
		GET_VEHICLES,
		GET_TRACK,
		NONE
	};

	/**
	 * This class will basically hold information about the outgoing requests
	 * This class is controled trough another thread, which calls the go() method
	 * We set the needed requests via addTask() method
	 */
	class RequestThreadController
	{
	private:

		/**
		 * Do we have something to do
		 */
		bool _running;

		/**
		 * Do we have shutdown thread
		 */
		bool _shutdown;

		/**
		 * CURL request handler
		 */
		ServerRequests::RequestHandler _requestHandler;

		/**
		* Mutex to avoid reading and writing at the same time to the _taskList
		*/
		std::mutex _taskListMutex;

		/**
		 * List of tasks that should be done
		 */
		std::list<TaskList> _taskList;

		/**
		 * This holds information that can help us understand if specific job was finished or not
		 */
		std::map<TaskList, bool> _jobsDone;

		/**
		 * Which is the current task that we are processing
		 */
		TaskList _currentTask;

		/**
		 * Check _taskList, if there is a job pending, set _running to true, otherwise set it to false
		 */
		void getTask();

		/**
		 * Servers full url
		 */
		std::string _serverAddress;

		/**
		 * Class which will hold all the obtained results from the server
		 */
		RequestResults _requestResults;

	public:
		RequestThreadController();
		~RequestThreadController();

		/**
		 * Add task to the queue
		 */
		void addTask(TaskList t);

		/**
		 * Set this to shutdown mode
		 */
		void shutdown();

		/**
		 * Get current status of the job
		 */
		bool getJobStatus(TaskList t);

		/**
		 * Get the current job that this class is processing or will be processing next
		 */
		TaskList getCurrentJob();

		/**
		 * Basically do a loop and wait for the signal
		 */
		bool go();

		/**
		 * Get the pointer to the request results class
		 */
		RequestResults* getRequestResults();

		/**
		 * Get the current job description
		 */
		std::string getCurrentJobDescription();
	};
}
