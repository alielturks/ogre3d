#pragma once
#include "stdafx.h"

class MeshUtilities
{
public:
	MeshUtilities();
	~MeshUtilities();

	static void getMeshInformation(Ogre::MeshPtr mesh, size_t &vertex_count, Ogre::Vector3* &vertices,
		size_t &index_count, unsigned* &indices);
};

