#include "stdafx.h"

#pragma once
namespace GameEvent {
	
	/**
	 * All global events that we have in the game
	 */
	enum EventTypes {
		PLAYERS_JOINING,
		RACE_STARTED,
		RACE_ENDED,
		CREATE_DROP
	};

	typedef struct Event {
		EventTypes type;
	};

	/**
	 * All classes that want to register for global events should inherit this one
	 */
	class GameEventListener
	{
	public:
		/**
		 * Called when any of the events are triggered
		 */
		virtual void eventTriggered(EventTypes evt) {};

		GameEventListener() {};
		virtual ~GameEventListener() {};
	};

	class GameEventDispatcher
	{
	private:
		/**
		 * List of all the classes that are listening for the events
		 */
		std::list<GameEventListener*> _listeners;
	public:
		GameEventDispatcher();
		~GameEventDispatcher();

		/**
		 * Add listener
		 */
		void addListener(GameEventListener* listener);

		/**
		 * Remove listener
		 */
		void removeListener(GameEventListener* listener);

		/**
		 * Dispatch specific event
		 */
		void dispatchEvent(EventTypes type);
	};
}

