#pragma once
#include "stdafx.h"
#include "Vehicle.h"
#include "GameEventListener.h"
#include "Drop.h"

class Player: public GameEvent::GameEventListener
{
public:

	/**
	 * Data about the player
	 */
	struct PlayerData {
		int id;
		int health;
		int points;
	};

	/**
	 * Visual effects for this player
	 */
	struct PlayerEffects {
	};

	/**
	 * This struct will hold key code values
	 */
	struct PlayerControls {
		bool keyboardEnabled;
		unsigned int accelerate;
		unsigned int brake;
		unsigned int left;
		unsigned int right;
		unsigned int back;
		unsigned int reset;
	};

	Player();
	~Player();

	/**
	 * Set Vehicle for this player
	 */
	void setVehicle(Vehicle* v, Ogre::SceneNode* n);
	
	/**
	 * Init the player object
	 * Create all the GUI elements and add additional information
	 */
	void init(OgreBites::SdkTrayManager* mgr);

	void keyPressed(const OIS::KeyEvent &arg);
	void keyReleased(const OIS::KeyEvent &arg);
	void frameStarted(const Ogre::FrameEvent& evt);

	/**
	 * Set the pointer to the Ogre::Camera, which will follow this player
	 */
	void setCamera(Ogre::Camera* cam);

	virtual void eventTriggered(GameEvent::EventTypes evt);

	/**
	 * Hide this player's GUI elements
	 */
	void hideGui();
	/**
	* Show this player's GUI elements
	*/
	void showGui();

	/**
	 * Get all the information about the player
	 */
	const PlayerData getPlayerData();

	/**
	 * Get the player position
	 */
	Ogre::Vector3 getPlayerPos();

	/**
	 * Adds point to the player
	 */
	void addPoint();

	/**
	* Collect droppable object
	*/
	void collectDrop(Drop* drop);

	/**
	* Set the player controlls
	*/
	void setControls(
		unsigned int accelerate,
		unsigned int backward,
		unsigned int brake,
		unsigned int left,
		unsigned int right,
		unsigned int reset,
		bool keyboardEnabled
		);
	
	/**
	 * Set scene manager
	 */
	void setSceneManager(Ogre::SceneManager* mgr);

	/**
	* Toggle on/off some effects
	*/
	void updateEffects();

private:
	Ogre::SceneManager* _sceneManager;
	/**
	 * Struct that holds together raycast vehicle and car chasis scene node
	 */
	struct VehicleInfo {
		Vehicle* raycastVehicle;
		Ogre::SceneNode* vehicleNode;
	};

	/**
	 * All the GUI elements for the player
	 */
	struct PlayerInfoElements {
		OgreBites::Label* speed;
		OgreBites::Label* points;
		OgreBites::Label* bonus;
	};

	/**
	 * Pointer to the player's vehicle
	 */
	VehicleInfo _vehicleInfo;

	/**
	 * Information about all the GUI elements
	 */
	PlayerInfoElements _gui;

	/**
	 * Player data
	 * id, health, etc.
	 */
	PlayerData _playerData;

	/**
	 * Camera for this player
	 */
	Ogre::Camera* _camera;
	
	/**
	 * Do all the updating that we need
	 */
	void update(const Ogre::FrameEvent& evt);

	/**
	 * Update player's camera
	 */
	void updateCameraPosition(const Ogre::FrameEvent& evt);

	/**
	 * Update all the GUI elements
	 */
	void updateGui();

	/**
	 * Controls for this player
	 */
	PlayerControls _controls;

	/**
	 * Visual effects for this player
	 */
	PlayerEffects _effects;
};

