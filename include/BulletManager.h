#pragma once
#include "stdafx.h"
#include "BulletDebugDrawer.h"
#include "BulletCollision\CollisionShapes\btHeightfieldTerrainShape.h"
#include "DebugDrawer.h"
#include "Vehicle.h"
#include "GUI.h"
#include "Requests\RequestResults.h"

typedef struct RigidBodyObject {
	btRigidBody* object;
	std::string name;
};

class BulletManager
{
private:
	/**
	 * All the moving objects in bulletPhysics world
	 */
	std::vector<RigidBodyObject> rigidBodies;
	/**
	 * All the static objects in bulletPhysics world
	 */
	std::vector<RigidBodyObject> staticObjects;

	/**
	* Bullet physics variables
	*/
	btBroadphaseInterface* broadphase;
	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btDiscreteDynamicsWorld* dynamicsWorld;
	btCollisionShape* groundShape;
	btDefaultMotionState* groundMotionState;
	btRigidBody* groundRigidBody;
	btDefaultMotionState* fallMotionState;
	btRigidBody* fallRigidBody;
	btConvexHullShape* ground;
	OgreDebugDrawer* debugDrawer;

	Ogre::SceneManager* mSceneMgr;
	OgreBites::SdkTrayManager*	mTrayMgr;

	float _gravityTime;

	btVector3 _defaultGravity;

	bool _gravityEffectEnabled;
public:
	BulletManager();
	~BulletManager();

	/**
	 * Set the bullet physics world
	 * @param mgr - pointer to Ogre scene manager 
	 */
	void init(Ogre::SceneManager* mgr, OgreBites::SdkTrayManager*	mTray);

	/**
	* Add new sphere to the bullet physics world and the
	* Ogre engine
	* @param name - name of the entity, should be unique
	* @param size - btVector3 object with the size in x, y, z dimensions
	* @param position - btVector3 object with the position in the world
	* @param restitution - bounciness
	* @param linearDamping - air density (when moving)
	* @param angularDamping - air density (when rotating)
	* @param friction - friction between object and the world
	*/
	Ogre::SceneNode* addCube(	const std::string name, 
					const btVector3 &size, 
					const btVector3 &position, 
					const float mass,
					const float restitution, 
					const float linearDamping, 
					const float angularDamping, 
					const float friction
					);

	/**
	 * Add new sphere to the bullet physics world and the
	 * Ogre engine
	 * @param name - name of the entity, should be unique
	 * @param size - Sphere diameter
	 * @param position - btVector3 object with the position in the world
	 * @param restitution - bounciness
	 * @param linearDamping - air density (when moving)
	 * @param angularDamping - air density (when rotating)
	 * @param friction - friction between object and the world
	 */
	void addSphere(
					const std::string name, 
					const float size, 
					const btVector3 &position, 
					const float restitution, 
					const float linearDamping, 
					const float angularDamping, 
					const float friction
					);

	/**
	 * Delete dynamic object by provided iterator from the
	 * rigidBodies vector
	 * @param it - the actual iterator
	 */
	std::vector<RigidBodyObject>::iterator deleteObject(const std::vector<RigidBodyObject>::iterator it);

	/**
	 * Delete static plane by it's name
	 * Note: Multiple objects can contain the same name so
	 * all the objects with specified name will be deleted
	 * @param name - static plane name which
	 * was used when the object was created
	 */
	void deleteStaticObject(const std::string name);

	/**
	 * Adds static ground mesh
	 * @position - position of the static mesh
	 * @points - vector of btVector3 points for the mesh
	 * @restitution - bounciness
	 * @friction - friction between this plane and dynamic object
	 */
	btRigidBody* addCustomObject(const std::string name,
					const btVector3& position,
					const std::vector<btVector3>* points,
					const float restitution,
					const float friction,
					const float mass
					);

	/**
	 * Create ground
	 */
	void createGround(ServerRequests::TrackInfo* t);

	Vehicle* createCar(
		const int ControllerId,
		const std::string name,
		const btVector3& position,
		const std::vector<btVector3>* points,
		const float restitution,
		const float friction,
		const float mass,
		ServerRequests::VehicleInfo info,
		std::vector<btRigidBody*> wheels
		);

	/**
	 * Return pointer to the std::vector<RigidBodyObject>
	 * which will hold the entity name and the pointer to
	 * actual bullet library object
	 */
	std::vector<RigidBodyObject> getBodies();

	std::vector<RigidBodyObject>* getStaticBodies();

	/**
	 * Return pointer to the bullet physics world object
	 */
	btDiscreteDynamicsWorld* getWorld();

	bool frameStarted(const Ogre::FrameEvent& evt);
	bool frameEnded(const Ogre::FrameEvent& evt);

	virtual bool keyPressed(const OIS::KeyEvent &arg);
	virtual bool keyReleased(const OIS::KeyEvent &arg);

	bool axisMoved(const OIS::JoyStickEvent &e, int axis);
	bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
	bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
	bool povMoved(const OIS::JoyStickEvent &arg, int pov);

	/**
	 * Set the gravity
	 */
	void changeGravity(float g, float t);

	/**
	 * Get remaining time of the effect
	 */
	float getEffectTime();

	/**
	 * Check if no gravity effect is enabled
	 */
	bool getGravityEffect();
};

